﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace WinBridge
{
    public class HeadTracking
    {
        public enum EventType
        {
            // Calibration Events
            CalRunningEvent,

            // Working Events
            WornEvent,
            LeaningEvent,
            WalkingEvent,
            MagNoiseEvent,
            CalRequiredEvent
        };

        public struct Data3D
        {
            public Int16 x;
            public Int16 y;
            public Int16 z;
        }

        public struct IMUData
        {
            public Data3D acc;
            public Data3D gyro;
            public Data3D mag;
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void EventCallback(EventType ev, bool val);

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool setEventsCallback(EventCallback cb);

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool getEvent(EventType type);


        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool initialize();

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool setSingleIMUData(IMUData data);

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool startCalibration();

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool stopCalibration();

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern int getCalibrationSamplesNumber();

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool isCalibrationRunning();

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool getCalibrationResult(double[] a, double[] b);

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern double getRoll();

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern double getPitch();

        [DllImport("VisionARHeadTracking.dll", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern double getYaw();
    }
}
