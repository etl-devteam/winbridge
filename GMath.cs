﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinBridge
{
    public class GMath
    {
        public class GLocation
        {
            public double latitude;
            public double longitude;
            public double elevation;
        }

        public class GPoint
        {
            public double x;
            public double y;
            public double z;
            public double radius;
            public double nx;      // x, y e z normalizzati
            public double ny;
            public double nz;
        }

        public static bool ParseAngle(TextBox tb, double limit, out double angle)
        {
            string s = tb.Text.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            s = s.Replace(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            if (double.TryParse(s, out angle))
            {
                if (double.IsNaN(angle) || (angle < -limit) || (angle > limit))
                {
                    tb.BackColor = Color.Orange;
                    return false;
                }

                tb.BackColor = SystemColors.Window;
                return true;
            }

            tb.BackColor = Color.Orange;
            return false;
        }
        public static bool ParseElevation(TextBox tb, out double angle)
        {
            string s = tb.Text.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            s = s.Replace(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            if (double.TryParse(s, out angle))
            {
                tb.BackColor = SystemColors.Window;
                return true;
            }

            tb.BackColor = Color.Orange;
            return false;
        }

        /// <summary>
        /// Raggio della terra in metri alla latitudine specificata
        /// (http://en.wikipedia.org/wiki/Earth_radius)
        /// La latitudine è geodetica, ad esempio quella restituita dal GPS
        /// </summary>
        public static double EarthRadiusInMeters(double latitudeRadians)
        {
            double a = 6378137.0;  // equatorial radius in meters
            double b = 6356752.3;  // polar radius in meters
            double cos = Math.Cos(latitudeRadians);
            double sin = Math.Sin(latitudeRadians);
            double t1 = a * a * cos;
            double t2 = b * b * sin;
            double t3 = a * cos;
            double t4 = b * sin;
            return Math.Sqrt((t1 * t1 + t2 * t2) / (t3 * t3 + t4 * t4));
        }

        /// <summary>
        /// Converte un valore di latitudine geodetica in geocentrica
        /// (https://en.wikipedia.org/wiki/Latitude#Geocentric_latitude)
        /// </summary>
        public static double GeocentricLatitude(double latitude)
        {
            double e2 = 0.00669437999014;
            double clat = Math.Atan((1.0 - e2) * Math.Tan(latitude));
            return clat;
        }

        public static GPoint LocationToPoint(GLocation location, bool oblate)
        {
            GPoint p = new GPoint();

            // converte latitudine, longitudine e angolo di elevazione in x, y, z
            double lat = location.latitude * Math.PI / 180.0;
            double lon = location.longitude * Math.PI / 180.0;
            p.radius = oblate ? EarthRadiusInMeters(lat) : 6371009;
            double clat = oblate ? GeocentricLatitude(lat) : lat;

            double cosLon = Math.Cos(lon);
            double sinLon = Math.Sin(lon);
            double cosLat = Math.Cos(clat);
            double sinLat = Math.Sin(clat);
            p.x = p.radius * cosLon * cosLat;
            p.y = p.radius * sinLon * cosLat;
            p.z = p.radius * sinLat;

            // Utilizza la latitudine geocentrica per calcolare x, y e z sull'elissoide della terra
            // poi utilizza la latitudine geodetica nel calcolare il vettore normalizzato sulla superficie per corregere l'angolo di elevazione
            double cosGlat = Math.Cos(lat);
            double sinGlat = Math.Sin(lat);

            p.nx = cosGlat * cosLon;
            p.ny = cosGlat * sinLon;
            p.nz = sinGlat;

            p.x += location.elevation * p.nx;
            p.y += location.elevation * p.ny;
            p.z += location.elevation * p.nz;

            return p;
        }

        public static double Distance(GPoint ap, GPoint bp)
        {
            var dx = ap.x - bp.x;
            var dy = ap.y - bp.y;
            var dz = ap.z - bp.z;
            return Math.Sqrt((dx * dx) + (dy * dy) + (dz * dz));
        }

        public static GPoint RotateGlobe(GLocation b, GLocation a, double bradius, double aradius, bool oblate)
        {
            // Get modified coordinates of 'b' by rotating the globe so that 'a' is at lat=0, lon=0.
            GLocation br = new GLocation { latitude = b.latitude, longitude = (b.longitude - a.longitude), elevation = b.elevation };
            GPoint brp = LocationToPoint(br, oblate);

            // Ruota le coordinate cartesiantedi brp intorno all'asse z per i gradi di a.longitude,
            // poi ruota l'asse y per i gradi di a.latitude.
            // Sebbene a.latitude sia stato diminuito, la rotazione è positiva quando b.longitude è ad est di b.longitude.
            // Da questo punto di vista l'asse x punta verso sinistra, quindi bisogna invertire l'asse x e
            // l'asse z e trattare la rotazione come negativa.
            double alat = -a.latitude * Math.PI / 180.0;
            if (oblate)
                alat = GeocentricLatitude(alat);

            double acos = Math.Cos(alat);
            double asin = Math.Sin(alat);

            double bx = (brp.x * acos) - (brp.z * asin);
            double by = brp.y;
            double bz = (brp.x * asin) + (brp.z * acos);

            return new GPoint { x = bx, y = by, z = bz, radius = bradius };
        }

        /// <summary>
        /// Calcola  il vettore tra due punti normalizzando la differenza al valore unitario
        /// </summary>
        public static bool NormalizeVectorDiff(GPoint b, GPoint a, out GPoint n)
        {
            n = new GPoint();

            double dx = b.x - a.x;
            double dy = b.y - a.y;
            double dz = b.z - a.z;
            double dist2 = dx * dx + dy * dy + dz * dz;
            if (dist2 == 0)
                return false;

            double dist = Math.Sqrt(dist2);
            n.x = dx / dist;
            n.y = dy / dist;
            n.z = dz / dist;
            n.radius = 1.0;
            return true;
        }

    }
}