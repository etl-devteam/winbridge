﻿namespace WinBridge
{
    partial class frmCalibration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblStato = new System.Windows.Forms.Label();
            this.btnAvvia = new System.Windows.Forms.Button();
            this.btnInterrompi = new System.Windows.Forms.Button();
            this.lblProgress = new System.Windows.Forms.Label();
            this.pbStatus = new System.Windows.Forms.PictureBox();
            this.lblCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblB0 = new System.Windows.Forms.Label();
            this.lblB1 = new System.Windows.Forms.Label();
            this.lblB2 = new System.Windows.Forms.Label();
            this.lblA0 = new System.Windows.Forms.Label();
            this.lblA1 = new System.Windows.Forms.Label();
            this.lblA2 = new System.Windows.Forms.Label();
            this.lblA3 = new System.Windows.Forms.Label();
            this.lblA4 = new System.Windows.Forms.Label();
            this.lblA5 = new System.Windows.Forms.Label();
            this.lblA6 = new System.Windows.Forms.Label();
            this.lblA7 = new System.Windows.Forms.Label();
            this.lblA8 = new System.Windows.Forms.Label();
            this.lblMagB = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(474, 284);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(93, 30);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Annulla";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Enabled = false;
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(374, 284);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(93, 30);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblStato
            // 
            this.lblStato.AutoSize = true;
            this.lblStato.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblStato.Location = new System.Drawing.Point(12, 9);
            this.lblStato.Name = "lblStato";
            this.lblStato.Size = new System.Drawing.Size(43, 16);
            this.lblStato.TabIndex = 4;
            this.lblStato.Text = "Stato:";
            // 
            // btnAvvia
            // 
            this.btnAvvia.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAvvia.Location = new System.Drawing.Point(15, 51);
            this.btnAvvia.Name = "btnAvvia";
            this.btnAvvia.Size = new System.Drawing.Size(109, 30);
            this.btnAvvia.TabIndex = 3;
            this.btnAvvia.Text = "Avvia";
            this.btnAvvia.UseVisualStyleBackColor = true;
            this.btnAvvia.Click += new System.EventHandler(this.btnAvvia_Click);
            // 
            // btnInterrompi
            // 
            this.btnInterrompi.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInterrompi.Location = new System.Drawing.Point(15, 51);
            this.btnInterrompi.Name = "btnInterrompi";
            this.btnInterrompi.Size = new System.Drawing.Size(109, 30);
            this.btnInterrompi.TabIndex = 3;
            this.btnInterrompi.Text = "Interrompi";
            this.btnInterrompi.UseVisualStyleBackColor = true;
            this.btnInterrompi.Visible = false;
            this.btnInterrompi.Click += new System.EventHandler(this.btnInterrompi_Click);
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblProgress.Location = new System.Drawing.Point(12, 31);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(253, 16);
            this.lblProgress.TabIndex = 4;
            this.lblProgress.Text = "Premere \'Avvia\' per iniziare la raccolta dati";
            // 
            // pbStatus
            // 
            this.pbStatus.Image = Properties.Resources.smallgrayled;
            this.pbStatus.Location = new System.Drawing.Point(62, 10);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(16, 16);
            this.pbStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbStatus.TabIndex = 5;
            this.pbStatus.TabStop = false;
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblCount.Location = new System.Drawing.Point(124, 9);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(124, 16);
            this.lblCount.TabIndex = 4;
            this.lblCount.Text = "Valori collezionati: 0";
            this.lblCount.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Bias combinato (b):";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.05882F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.64706F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.64706F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.64706F));
            this.tableLayoutPanel1.Controls.Add(this.lblB2, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblB1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblB0, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblA0, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblA1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblA2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblA3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblA4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblA5, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblA6, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblA7, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblA8, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblMagB, 1, 6);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(15, 87);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(552, 191);
            this.tableLayoutPanel1.TabIndex = 7;
            this.tableLayoutPanel1.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label3.Location = new System.Drawing.Point(3, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.label3.Name = "label3";
            this.tableLayoutPanel1.SetRowSpan(this.label3, 3);
            this.label3.Size = new System.Drawing.Size(204, 48);
            this.label3.TabIndex = 5;
            this.label3.Text = "Fattore di scala combinato, compensazione disallineamento e distorsione da soft-i" +
    "ron (A):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label2.Location = new System.Drawing.Point(262, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 16);
            this.label2.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label7.Location = new System.Drawing.Point(3, 156);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(227, 16);
            this.label7.TabIndex = 9;
            this.label7.Text = "Magnitudo campo geomagnetico (uT):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label6.Location = new System.Drawing.Point(3, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = " ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.label8.Location = new System.Drawing.Point(3, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 16);
            this.label8.TabIndex = 10;
            this.label8.Text = " ";
            // 
            // lblB0
            // 
            this.lblB0.AutoSize = true;
            this.lblB0.BackColor = System.Drawing.SystemColors.Window;
            this.lblB0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblB0.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblB0.Location = new System.Drawing.Point(262, 4);
            this.lblB0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblB0.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblB0.Name = "lblB0";
            this.lblB0.Padding = new System.Windows.Forms.Padding(2);
            this.lblB0.Size = new System.Drawing.Size(80, 22);
            this.lblB0.TabIndex = 9;
            this.lblB0.Text = "0.0";
            this.lblB0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblB1
            // 
            this.lblB1.AutoSize = true;
            this.lblB1.BackColor = System.Drawing.SystemColors.Window;
            this.lblB1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblB1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblB1.Location = new System.Drawing.Point(359, 4);
            this.lblB1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblB1.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblB1.Name = "lblB1";
            this.lblB1.Padding = new System.Windows.Forms.Padding(2);
            this.lblB1.Size = new System.Drawing.Size(80, 22);
            this.lblB1.TabIndex = 11;
            this.lblB1.Text = "0.0";
            this.lblB1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblB2
            // 
            this.lblB2.AutoSize = true;
            this.lblB2.BackColor = System.Drawing.SystemColors.Window;
            this.lblB2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblB2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblB2.Location = new System.Drawing.Point(456, 4);
            this.lblB2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblB2.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblB2.Name = "lblB2";
            this.lblB2.Padding = new System.Windows.Forms.Padding(2);
            this.lblB2.Size = new System.Drawing.Size(80, 22);
            this.lblB2.TabIndex = 12;
            this.lblB2.Text = "0.0";
            this.lblB2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA0
            // 
            this.lblA0.AutoSize = true;
            this.lblA0.BackColor = System.Drawing.SystemColors.Window;
            this.lblA0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA0.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA0.Location = new System.Drawing.Point(262, 50);
            this.lblA0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA0.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA0.Name = "lblA0";
            this.lblA0.Padding = new System.Windows.Forms.Padding(2);
            this.lblA0.Size = new System.Drawing.Size(80, 22);
            this.lblA0.TabIndex = 13;
            this.lblA0.Text = "0.0";
            this.lblA0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA1
            // 
            this.lblA1.AutoSize = true;
            this.lblA1.BackColor = System.Drawing.SystemColors.Window;
            this.lblA1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA1.Location = new System.Drawing.Point(359, 50);
            this.lblA1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA1.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA1.Name = "lblA1";
            this.lblA1.Padding = new System.Windows.Forms.Padding(2);
            this.lblA1.Size = new System.Drawing.Size(80, 22);
            this.lblA1.TabIndex = 14;
            this.lblA1.Text = "0.0";
            this.lblA1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA2
            // 
            this.lblA2.AutoSize = true;
            this.lblA2.BackColor = System.Drawing.SystemColors.Window;
            this.lblA2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA2.Location = new System.Drawing.Point(456, 50);
            this.lblA2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA2.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA2.Name = "lblA2";
            this.lblA2.Padding = new System.Windows.Forms.Padding(2);
            this.lblA2.Size = new System.Drawing.Size(80, 22);
            this.lblA2.TabIndex = 15;
            this.lblA2.Text = "0.0";
            this.lblA2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA3
            // 
            this.lblA3.AutoSize = true;
            this.lblA3.BackColor = System.Drawing.SystemColors.Window;
            this.lblA3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA3.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA3.Location = new System.Drawing.Point(262, 80);
            this.lblA3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA3.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA3.Name = "lblA3";
            this.lblA3.Padding = new System.Windows.Forms.Padding(2);
            this.lblA3.Size = new System.Drawing.Size(80, 22);
            this.lblA3.TabIndex = 16;
            this.lblA3.Text = "0.0";
            this.lblA3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA4
            // 
            this.lblA4.AutoSize = true;
            this.lblA4.BackColor = System.Drawing.SystemColors.Window;
            this.lblA4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA4.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA4.Location = new System.Drawing.Point(359, 80);
            this.lblA4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA4.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA4.Name = "lblA4";
            this.lblA4.Padding = new System.Windows.Forms.Padding(2);
            this.lblA4.Size = new System.Drawing.Size(80, 22);
            this.lblA4.TabIndex = 17;
            this.lblA4.Text = "0.0";
            this.lblA4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA5
            // 
            this.lblA5.AutoSize = true;
            this.lblA5.BackColor = System.Drawing.SystemColors.Window;
            this.lblA5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA5.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA5.Location = new System.Drawing.Point(456, 80);
            this.lblA5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA5.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA5.Name = "lblA5";
            this.lblA5.Padding = new System.Windows.Forms.Padding(2);
            this.lblA5.Size = new System.Drawing.Size(80, 22);
            this.lblA5.TabIndex = 18;
            this.lblA5.Text = "0.0";
            this.lblA5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA6
            // 
            this.lblA6.AutoSize = true;
            this.lblA6.BackColor = System.Drawing.SystemColors.Window;
            this.lblA6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA6.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA6.Location = new System.Drawing.Point(262, 110);
            this.lblA6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA6.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA6.Name = "lblA6";
            this.lblA6.Padding = new System.Windows.Forms.Padding(2);
            this.lblA6.Size = new System.Drawing.Size(80, 22);
            this.lblA6.TabIndex = 19;
            this.lblA6.Text = "0.0";
            this.lblA6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA7
            // 
            this.lblA7.AutoSize = true;
            this.lblA7.BackColor = System.Drawing.SystemColors.Window;
            this.lblA7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA7.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA7.Location = new System.Drawing.Point(359, 110);
            this.lblA7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA7.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA7.Name = "lblA7";
            this.lblA7.Padding = new System.Windows.Forms.Padding(2);
            this.lblA7.Size = new System.Drawing.Size(80, 22);
            this.lblA7.TabIndex = 20;
            this.lblA7.Text = "0.0";
            this.lblA7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblA8
            // 
            this.lblA8.AutoSize = true;
            this.lblA8.BackColor = System.Drawing.SystemColors.Window;
            this.lblA8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblA8.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblA8.Location = new System.Drawing.Point(456, 110);
            this.lblA8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblA8.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblA8.Name = "lblA8";
            this.lblA8.Padding = new System.Windows.Forms.Padding(2);
            this.lblA8.Size = new System.Drawing.Size(80, 22);
            this.lblA8.TabIndex = 21;
            this.lblA8.Text = "0.0";
            this.lblA8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblMagB
            // 
            this.lblMagB.AutoSize = true;
            this.lblMagB.BackColor = System.Drawing.SystemColors.Window;
            this.lblMagB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMagB.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblMagB.Location = new System.Drawing.Point(262, 156);
            this.lblMagB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblMagB.MinimumSize = new System.Drawing.Size(80, 0);
            this.lblMagB.Name = "lblMagB";
            this.lblMagB.Padding = new System.Windows.Forms.Padding(2);
            this.lblMagB.Size = new System.Drawing.Size(80, 22);
            this.lblMagB.TabIndex = 22;
            this.lblMagB.Text = "0.0";
            this.lblMagB.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // frmCalibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 326);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.lblStato);
            this.Controls.Add(this.btnInterrompi);
            this.Controls.Add(this.btnAvvia);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.MinimumSize = new System.Drawing.Size(595, 365);
            this.Name = "frmCalibration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calibrazione";
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblStato;
        private System.Windows.Forms.Button btnAvvia;
        private System.Windows.Forms.Button btnInterrompi;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.PictureBox pbStatus;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblB2;
        private System.Windows.Forms.Label lblB1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblB0;
        private System.Windows.Forms.Label lblA0;
        private System.Windows.Forms.Label lblA1;
        private System.Windows.Forms.Label lblA2;
        private System.Windows.Forms.Label lblA3;
        private System.Windows.Forms.Label lblA4;
        private System.Windows.Forms.Label lblA5;
        private System.Windows.Forms.Label lblA6;
        private System.Windows.Forms.Label lblA7;
        private System.Windows.Forms.Label lblA8;
        private System.Windows.Forms.Label lblMagB;
    }
}