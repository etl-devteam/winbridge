﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadWizard.WinUSBNet;
using libusb;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.Net;
using System.Xml;
using System.Linq.Expressions;
using System.Net.NetworkInformation;

namespace WinBridge
{
    public partial class frmMain : Form
    {
        private BackgroundWorker worker = new BackgroundWorker();
        private BackgroundWorker xmlTcpSniffer = new BackgroundWorker();
        private BackgroundWorker xmlUdpSniffer = new BackgroundWorker();
        private USBDevice fbUSBdevice = null;
        private USBDevice memsUSBdevice = null;
        private USBInterface fbUSBInterface = null;
        private USBInterface memsUSBInterface = null;
        private bool CalibrationIsRunning = false;
        private Bitmap bitmap = null;
        private GCHandle BitsHandle;
        private int[] GCPixels = new int[VisionAR.PIXEL_WIDTH * VisionAR.PIXEL_HEIGHT];

        public frmMain()
        {
            InitializeComponent();

            BitsHandle = GCHandle.Alloc(GCPixels, GCHandleType.Pinned);
            bitmap = new Bitmap(VisionAR.PIXEL_WIDTH, VisionAR.PIXEL_HEIGHT, VisionAR.PIXEL_WIDTH * 4, PixelFormat.Format32bppPArgb, BitsHandle.AddrOfPinnedObject());
            Application.ApplicationExit += new EventHandler(MemoryCleanup);
            pictureBox1.Image = bitmap;
            tsTcp.Visible = false;
            tsTcp.Tag = -1;
            tsUdp.Visible = false;
            tsUdp.Tag = -1;

            if (Images.MakeDefault() == false)
                Close();

            Images.Refresh();
        }

        private void SaveXmlFile(string xml, string uid)
        {
            int i = 0;
            string unum = "";
            string fname;

            fname = Path.Combine(Properties.Settings.Default.XMLpath, uid + ".xml");
            if (File.Exists(fname) && Properties.Settings.Default.ServerLog)
            {
                while (true)
                {
                    string newfname = Path.Combine(Properties.Settings.Default.XMLpath, uid + " " + DateTime.UtcNow.ToString("yyyy-MM-ddTHHmmss.") + unum + "bak");
                    if (File.Exists(newfname) == false)
                    {
                        try
                        {
                            File.Move(fname, newfname);
                            break;
                        }
                        catch { return; }
                    }
                    unum = (i++).ToString("D4") + ".";
                }
            }

            try { File.WriteAllText(fname, xml); }
            catch { }
        }

        private string SaveFileWithUniqueName(string fileExtension, string data)
        {
            int i = 0;
            string unum = "";
            string fname;

            if (Properties.Settings.Default.ServerLog == false)
                return "";

            while (true)
            {
                fname = Path.Combine(Properties.Settings.Default.XMLpath, DateTime.UtcNow.ToString("yyyy-MM-ddTHHmmss.") + unum + fileExtension);
                if (File.Exists(fname) == false)
                {
                    try
                    {
                        File.WriteAllText(fname, data);
                        break;
                    }
                    catch { return ""; }
                }
                unum = (i++).ToString("D4") + ".";
                if (i == 9999)
                    return "";
            }
            return fname;
        }

        private string ExtractXML(string s)
        {
            if (s.Length < 20)
                return s;
            if (s.Length > 30000)
            {
                string logName = SaveFileWithUniqueName("log", s);
                s = "Il buffer di ricezione è stato saturato in modo imprevisto e verrà ripristinato.";
                if (logName != "")
                    s += " Il contenuto del buffer è stato salvato nel file " + logName;
                MessageBox.Show(this, s, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }

            int len = 0;
            while (true)
            {
                if (len == s.Length)
                    return s;               // qualcosa è andato storto; la lunghezza della stringa è rimasta inviariata e ciò significa che non è stata processata per un caso imprevisto
                len = s.Length;

                // elimina tutti i caratteri prima di '<'
                int pos = s.IndexOf("<");
                if (pos < 0)
                    return "";
                if (pos > 0)
                    s = s.Substring(pos);
                if (s.Length < 6)
                    return s;

                // verifica l'XML-prolog
                if ((s.StartsWith("<?xml") == false) || (char.IsWhiteSpace(s[5]) == false))
                {
                    s = s.Substring(1);
                    continue;
                }

                // XML-prolog trovato, verifica se ce n'è uno successivo
                pos = s.IndexOf("<?xml", 1);
                if (pos > 0)    // se c'è, mi aspetto che tutto ciò che c'è prima sia un XML valido
                {
                    string xml = s.Substring(0, pos);
                    s = s.Remove(0, xml.Length);
                    xml = xml.Substring(0, xml.LastIndexOf('>') + 1);
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(xml);
                        //if (doc.DocumentElement["event/detail/remarks"] != null)
                          //  MessageBox.Show("special handling");
                        SaveXmlFile(xml, doc.DocumentElement.Attributes["uid"].Value);
                    }
                    catch { SaveFileWithUniqueName("malf", xml); }
                }
                else           // se non c'è, verifico se c'è il tag di chiusura
                {
                    pos = s.LastIndexOf("</event");
                    if (pos < 0)
                        return s;

                    string xml = s.Substring(0, pos + 7);
                    s = s.Remove(0, xml.Length);
                    xml += ">";
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(xml);
                        //if (doc["event"]["detail"]["remarks"] != null)
                          //  MessageBox.Show("special handling");
                        SaveXmlFile(xml, doc.DocumentElement.Attributes["uid"].Value);
                    }
                    catch { SaveFileWithUniqueName("malf", xml); }

                }
            }
        }

        private void MemoryCleanup(object sender, EventArgs e)
        {
            if (bitmap != null)
            {
                bitmap.Dispose();
                BitsHandle.Free();
                bitmap = null;
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UpgradeRequired)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpgradeRequired = false;
                Properties.Settings.Default.Save();
            }

            if (string.IsNullOrWhiteSpace(Properties.Settings.Default.XMLpath) || (Directory.Exists(Properties.Settings.Default.XMLpath) == false))
            {
                Properties.Settings.Default.XMLpath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                Properties.Settings.Default.Save();
            }

            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerAsync();

            xmlTcpSniffer.WorkerReportsProgress = true;
            xmlTcpSniffer.WorkerSupportsCancellation = true;
            xmlTcpSniffer.DoWork += XmlTcpSniffer_DoWork;
            xmlTcpSniffer.ProgressChanged += XmlTcpSniffer_ProgressChanged;
            xmlTcpSniffer.RunWorkerAsync();

            xmlUdpSniffer.WorkerReportsProgress = true;
            xmlUdpSniffer.WorkerSupportsCancellation = true;
            xmlUdpSniffer.DoWork += XmlUdpSniffer_DoWork;
            xmlUdpSniffer.ProgressChanged += XmlUdpSniffer_ProgressChanged;
            xmlUdpSniffer.RunWorkerAsync();
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == -1)
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                    g.FillRectangle(Brushes.Black, 0, 0, VisionAR.PIXEL_WIDTH, VisionAR.PIXEL_HEIGHT);

                try
                {
                    for (int y = 0; y < VisionAR.PIXEL_HEIGHT; y++)
                    {
                        for (int x = 0; x < VisionAR.PIXEL_WIDTH; x++)
                        {
                            byte b = ((byte[])e.UserState)[y * VisionAR.PIXEL_WIDTH + x];
                            if (b != 0)
                                GCPixels[y * VisionAR.PIXEL_WIDTH + x] = (b | 0xFF0000) << 8;
                        }
                    }
                }
                catch { }

                pictureBox1.Invalidate();
            }
            else if (e.ProgressPercentage == 0)
            {
                tsStatus.Image = Properties.Resources.smallredled;
                tsStatus.ToolTipText = (string)e.UserState;
            }
            else if (e.ProgressPercentage == 1)
            {
                tsStatus.Image = Properties.Resources.smallgreenled;
                tsStatus.ToolTipText = (string)e.UserState;
            }
            else if (e.ProgressPercentage == -3)
            {
                if (tsFiles.ToolTipText != (string)e.UserState)
                {
                    tsFiles.Image = Properties.Resources.FolderRed;
                    tsFiles.ToolTipText = (string)e.UserState;
                }
            }
            else if (e.ProgressPercentage == -4)
            {
                tsFiles.Image = Properties.Resources.Folder;
                tsFiles.ToolTipText = (string)e.UserState;
            }
        }

        private void Worker_USBClose()
        {
            if (fbUSBdevice != null)
                try { fbUSBdevice.Dispose(); } catch { }

            if (memsUSBdevice != null)
                try { memsUSBdevice.Dispose(); } catch { }

            fbUSBdevice = null;
            fbUSBInterface = null;
        }

        private bool Worker_USBOpen()
        {
            if (fbUSBdevice != null)
                fbUSBdevice.Dispose();

            if (memsUSBdevice != null)
                memsUSBdevice.Dispose();

            try
            {
                USBDeviceInfo[] details = USBDevice.GetDevices(VisionAR.GUIDStringFB);
                if (details.Length == 0)
                    throw new Exception();

                USBDeviceInfo match = details.First(info => (info.VID == VisionAR.VID) && (info.PID == VisionAR.PID));
                fbUSBdevice = new USBDevice(match);
                fbUSBInterface = fbUSBdevice.Interfaces.First(usbIf => (usbIf.BaseClass == USBBaseClass.CDCData));
            }
            catch
            {
                try { if (fbUSBdevice != null) fbUSBdevice.Dispose(); } catch { }
                fbUSBdevice = null;
                fbUSBInterface = null;
                return false;
            }

            try
            {
                USBDeviceInfo[] details = USBDevice.GetDevices(VisionAR.GUIDStringCDC);
                if (details.Length == 0)
                    throw new Exception();

                USBDeviceInfo match = details.First(info => (info.VID == VisionAR.VID) && (info.PID == VisionAR.PID));
                memsUSBdevice = new USBDevice(match);
                memsUSBInterface = memsUSBdevice.Interfaces.First(usbIf => (usbIf.BaseClass == USBBaseClass.VendorSpecific) && (usbIf.Number == 1));
            }
            catch
            {
                try { if (memsUSBdevice != null) memsUSBdevice.Dispose(); } catch { }
                memsUSBdevice = null;
                memsUSBInterface = null;
                Thread.Sleep(100);
                return false;
            }
            return true;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch fb_sw = new Stopwatch();
            Stopwatch imu_sw = new Stopwatch();
            int imu_refresh_interval = 50;

            Program.Settings.Changed = true;
            int err = -1;
            fb_sw.Restart();
            imu_sw.Restart();

            while (true)
            {
                if (worker.CancellationPending)
                {
                    Worker_USBClose();
                    e.Cancel = true;
                    return;
                }

                if (Program.Settings.Changed || (err != 0))
                {
                    Thread.Sleep(100);

                    Worker_USBClose();
                    Program.Settings.Load();

                    imu_refresh_interval = (int)Math.Round(1000 / Program.Settings.IMU_SampleRate_HZ);

                    if (Worker_USBOpen() == false)
                    {
                        if ((err != 1))
                        {
                            worker.ReportProgress(0, "VisionAR non rilevato");
                            err = 1;
                        }
                    }
                    else
                    {
                        HeadTracking.initialize();
                        ViewPort.MakeViewPort();
                        if (err != 0)
                        {
                            worker.ReportProgress(1, "VisionAR collegato");
                            err = 0;
                        }
                    }
                }
                else if (CalibrationIsRunning)
                {
                    if (imu_sw.ElapsedMilliseconds >= imu_refresh_interval)
                    {
                        if (VisionAR.GetStatus(memsUSBInterface, out HeadTracking.IMUData s))
                            worker.ReportProgress(-2, s);
                        else if (err != 1)
                        {
                            worker.ReportProgress(0, "VisionAR non ha risposto al comando");
                            err = 1;
                        }

                        imu_sw.Restart();
                    }
                }
                else
                {
                    if (fb_sw.ElapsedMilliseconds > 16)
                    {
                        ViewPort.UpdateField(HeadTracking.getPitch(), HeadTracking.getRoll(), HeadTracking.getYaw());
                        if (VisionAR.SendPixels(ref ViewPort.pixels, fbUSBInterface) == false)
                        {
                            if (err != 1)
                            {
                                worker.ReportProgress(0, "VisionAR non ha risposto al comando");
                                err = 1;
                            }
                        }
                        else
                            worker.ReportProgress(-1, ViewPort.pixels);
                        fb_sw.Restart();
                    }

                    if (imu_sw.ElapsedMilliseconds >= imu_refresh_interval)
                    {
                        if (VisionAR.GetStatus(memsUSBInterface, out HeadTracking.IMUData s))
                            HeadTracking.setSingleIMUData(s);
                        else if (err != 1)
                        {
                            worker.ReportProgress(0, "VisionAR non ha risposto al comando");
                            err = 1;
                        }

                        imu_sw.Restart();
                    }

                    Markers.ScanForMarkers(Properties.Settings.Default.XMLpath, worker);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void azimuthDistanceCalculatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAziDistCalc frm = new frmAziDistCalc();
            frm.ShowDialog();
        }

        private void calibrazioneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCalibration frm = new frmCalibration();
            CalibrationIsRunning = true;
            worker.ProgressChanged += frm.Worker_CollectData;
            frm.ShowDialog();
            worker.ProgressChanged -= frm.Worker_CollectData;
            frm.Dispose();
            CalibrationIsRunning = false;
            Program.Settings.Changed = true;
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            worker.CancelAsync();
            xmlTcpSniffer.CancelAsync();
            xmlUdpSniffer.CancelAsync();
        }

        private void informazioniSuVisionARMWindowsEngineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAbout().ShowDialog();
        }

        private void impostazioniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettingsFile frm = new frmSettingsFile();
            if (frm.ShowDialog() == DialogResult.OK)
                Program.Settings.Changed = true;
            frm.Dispose();
        }

        private void immaginiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImages frm = new frmImages();
            if (frm.ShowDialog() == DialogResult.OK)
                Images.Refresh();
            frm.Dispose();
        }

        private void connessioneProviderXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettingsServer frm = new frmSettingsServer();
            DialogResult ret = frm.ShowDialog();
            frm.Dispose();
        }

        private void XmlTcpSniffer_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            bool visible = (Properties.Settings.Default.ServerConnectionActive && (Properties.Settings.Default.ServerProtocolType == 1));

            if (e.ProgressPercentage == 0)
            {
                if ((int)tsTcp.Tag != 0)
                    tsTcp.Image = Properties.Resources.ConnectionOff;
                tsTcp.Tag = 0;
            }
            else if ((int)tsTcp.Tag != 1)
            {
                tsTcp.Image = Properties.Resources.ConnectionOn;
                tsTcp.Tag = 1;
            }

            if (tsTcp.Visible != visible)
                tsTcp.Visible = visible;            
        }

        private void XmlTcpSniffer_DoWork(object sender, DoWorkEventArgs e)
        {
            IPGlobalProperties ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] connectionsInfo = null;
            TcpClient client = null;
            NetworkStream networkStream = null;
            byte[] bytes = new byte[1000];
            string s = "";
            uint currentIP = 0;
            int currentPort = -1;
            int status = -1;
            bool active;
            bool changed;

            while (true)
            {
                active = (Properties.Settings.Default.ServerConnectionActive && (Properties.Settings.Default.ServerProtocolType == 1));
                changed = ((currentIP != Properties.Settings.Default.ServerIPAddr) || (currentPort != Properties.Settings.Default.ServerPort));

                if ((active == false) || xmlTcpSniffer.CancellationPending)
                {
                    if (status != 0)
                        xmlTcpSniffer.ReportProgress(0);
                    status = 0;
                    s = "";
                    if (networkStream != null)
                    {
                        try { networkStream.Close(); } catch { }
                        networkStream = null;
                    }
                    if (client != null)
                    {
                        try { client.Close(); } catch { }
                        client = null;
                    }
                    if (xmlTcpSniffer.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    Thread.Sleep(100);
                }
                else if (changed)
                {
                    if (status != 1)
                        xmlTcpSniffer.ReportProgress(0);
                    status = 1;
                    currentIP = Properties.Settings.Default.ServerIPAddr;
                    currentPort = Properties.Settings.Default.ServerPort;
                    s = "";
                    if (networkStream != null)
                    {
                        try { networkStream.Close(); } catch { }
                        networkStream = null;
                    }
                    if (client != null)
                    {
                        try { client.Close(); } catch { }
                        client = null;
                    }
                }
                else if (client == null)
                {
                    s = "";
                    try
                    {
                        IPAddress ipa = new IPAddress(currentIP);
                        IPEndPoint ipe = new IPEndPoint(ipa, currentPort);
                        client = new TcpClient();
                        client.Connect(ipe);
                        networkStream = client.GetStream();
                        networkStream.ReadTimeout = 100;

                        connectionsInfo = ipProperties.GetActiveTcpConnections().Where(x => x.LocalEndPoint.Equals(client.Client.LocalEndPoint) && x.RemoteEndPoint.Equals(client.Client.RemoteEndPoint)).ToArray();
                        if (status != 2)
                            xmlTcpSniffer.ReportProgress(1);
                        status = 2;
                    }
                    catch
                    {
                        if (status != 3)
                            xmlTcpSniffer.ReportProgress(0);
                        status = 3;
                        if (networkStream != null)
                        {
                            try { networkStream.Close(); } catch { }
                            networkStream = null;
                        }
                        if (client != null)
                        {
                            try { client.Close(); } catch { }
                            client = null;
                        }
                        Thread.Sleep(100);
                    }
                }
                else
                {
                    try
                    {
                        if (networkStream.DataAvailable)
                        {
                            int bytesRead = networkStream.Read(bytes, 0, 1000);
                            s += Encoding.UTF8.GetString(bytes, 0, bytesRead);
                        }
                        else
                        {
                            bool disconnected = false;
                            connectionsInfo = ipProperties.GetActiveTcpConnections().Where(x => x.LocalEndPoint.Equals(client.Client.LocalEndPoint) && x.RemoteEndPoint.Equals(client.Client.RemoteEndPoint)).ToArray();
                            if ((connectionsInfo != null) && (connectionsInfo.Length > 0))
                            {
                                TcpState stateOfConnection = connectionsInfo.First().State;
                                if (stateOfConnection != TcpState.Established)
                                    disconnected = true;
                            }
                            else
                                disconnected = true;

                            if (disconnected)
                            {
                                if (status != 4)
                                    xmlTcpSniffer.ReportProgress(0);
                                status = 4;

                                if (networkStream != null)
                                {
                                    try { networkStream.Close(); } catch { }
                                    networkStream = null;
                                }
                                if (client != null)
                                {
                                    try { client.Close(); } catch { }
                                    client = null;
                                }
                            }
                            s = ExtractXML(s);
                        }
                    }
                    catch { }
                }
            }
        }

        private void XmlUdpSniffer_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            bool visible = (Properties.Settings.Default.ServerConnectionActive && (Properties.Settings.Default.ServerProtocolType == 0));

            if (e.ProgressPercentage == 0)
            {
                if ((int)tsUdp.Tag != 0)
                    tsUdp.Image = Properties.Resources.ConnectionOff;
                tsUdp.Tag = 0;
            }
            else if ((int)tsUdp.Tag != 1)
            {
                tsUdp.Image = Properties.Resources.ConnectionOn;
                tsUdp.Tag = 1;
            }

            if (tsUdp.Visible != visible)
                tsUdp.Visible = visible;
        }

        private void XmlUdpSniffer_DoWork(object sender, DoWorkEventArgs e)
        {
            UdpClient client = null;
            IPEndPoint ipe = null;
            string s = "";
            uint currentIP = 0;
            int currentPort = -1;
            int status = -1;
            bool active;
            bool changed;

            while (true)
            {
                active = (Properties.Settings.Default.ServerConnectionActive && (Properties.Settings.Default.ServerProtocolType == 0));
                changed = ((currentIP != Properties.Settings.Default.ServerIPAddr) || (currentPort != Properties.Settings.Default.ServerPort));

                if ((active == false) || xmlUdpSniffer.CancellationPending)
                {
                    if (status != 0)
                        xmlUdpSniffer.ReportProgress(0);
                    status = 0;
                    s = "";
                    if (client != null)
                    {
                        try { client.Close(); } catch { }
                        client = null;
                    }
                    if (xmlUdpSniffer.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    Thread.Sleep(100);
                }
                else if (changed)
                {
                    currentIP = Properties.Settings.Default.ServerIPAddr;
                    currentPort = Properties.Settings.Default.ServerPort;
                    s = "";
                    if (client != null)
                    {
                        try { client.Close(); } catch { }
                        client = null;
                    }
                }
                else if (client == null)
                {
                    s = "";
                    try
                    {
                        ///IPAddress ipa = new IPAddress(currentIP);
                        IPAddress ipa = IPAddress.Any;
                        ipe = new IPEndPoint(ipa, currentPort);
                        client = new UdpClient();
                        client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                        client.ExclusiveAddressUse = false;
                        client.Client.Bind(ipe);
                        if (status != 1)
                            xmlUdpSniffer.ReportProgress(1);
                        status = 1;
                    }
                    catch
                    {
                        if (client != null)
                        {
                            try { client.Close(); } catch { }
                            client = null;
                        }
                        Thread.Sleep(100);
                    }
                }
                else
                {
                    try
                    {
                        if (client.Available > 0)
                        {
                            byte[] buf = client.Receive(ref ipe);
                            s += Encoding.UTF8.GetString(buf, 0, buf.Length);
                        }
                        s = ExtractXML(s);
                    }
                    catch { }
                }
            }
        }

    }
}
