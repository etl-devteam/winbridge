﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WinBridge
{
    delegate void xml_parse_map_delegate(string stringValue, ref byte flags, string dst);
    class XML_PARSE_MAP
    {
        public string NodePath;            // full node path where the value is intended to be written to
        public byte flags;                 // XML.MAP_FLAG_x
        public xml_parse_map_delegate callback;  // callback for value conversion and assignment
        public string dst;
    };

    class XML
    {
        public const int MAP_FLAG_OPTIONAL = 0x00;
        public const int MAP_FLAG_MANDATORY = 0x01;
        public const int MAP_FLAG_FOUND = 0x02;
        public const int MAP_FLAG_DUPLICATED = 0x04;
        public const int MAP_FLAG_ERROR = 0x08;
        public const int STACK_SIZE = 1024;

        public enum XRet
        {
            EEOF = -5,         // Unexpected EOF
            EREF = -4,         // Invalid character or entity reference (&whatever;)
            ECLOSE = -3,       // Close tag does not match open tag (<Tag> .. </OtherTag>)
            ESTACK = -2,       // Stack overflow (too deeply nested tags or too long element/attribute name)
            ESYN = -1,         // Syntax error (unexpected byte)
            OK = 0,            // Character consumed, no new token present
            ELEMSTART = 1,     // Start of an element: '<Tag ..'
            CONTENT = 2,       // Element content
            ELEMEND = 3,       // End of an element: '.. />' or '</Tag>'
            ATTRSTART = 4,     // Attribute: 'Name=..'
            ATTRVAL = 5,       // Attribute value
            ATTREND = 6,       // End of attribute '.."'
            PISTART = 7,       // Start of a processing instruction
            PICONTENT = 8,     // Content of a PI
            PIEND = 9          // End of a processing instruction
        }

        public enum XState
        {
            xmlS_string,
            xmlS_attr0,
            xmlS_attr1,
            xmlS_attr2,
            xmlS_attr3,
            xmlS_attr4,
            xmlS_cd0,
            xmlS_cd1,
            xmlS_cd2,
            xmlS_comment0,
            xmlS_comment1,
            xmlS_comment2,
            xmlS_comment3,
            xmlS_comment4,
            xmlS_dt0,
            xmlS_dt1,
            xmlS_dt2,
            xmlS_dt3,
            xmlS_dt4,
            xmlS_elem0,
            xmlS_elem1,
            xmlS_elem2,
            xmlS_elem3,
            xmlS_enc0,
            xmlS_enc1,
            xmlS_enc2,
            xmlS_enc3,
            xmlS_etag0,
            xmlS_etag1,
            xmlS_etag2,
            xmlS_init,
            xmlS_le0,
            xmlS_le1,
            xmlS_le2,
            xmlS_le3,
            xmlS_lee1,
            xmlS_lee2,
            xmlS_leq0,
            xmlS_misc0,
            xmlS_misc1,
            xmlS_misc2,
            xmlS_misc2a,
            xmlS_misc3,
            xmlS_pi0,
            xmlS_pi1,
            xmlS_pi2,
            xmlS_pi3,
            xmlS_pi4,
            xmlS_std0,
            xmlS_std1,
            xmlS_std2,
            xmlS_std3,
            xmlS_ver0,
            xmlS_ver1,
            xmlS_ver2,
            xmlS_ver3,
            xmlS_xmldecl0,
            xmlS_xmldecl1,
            xmlS_xmldecl2,
            xmlS_xmldecl3,
            xmlS_xmldecl4,
            xmlS_xmldecl5,
            xmlS_xmldecl6,
            xmlS_xmldecl7,
            xmlS_xmldecl8,
            xmlS_xmldecl9
        }

        public class SXML
        {
            public int elem;
            public byte[] data;
            public int attr;
            public int pi;
            public byte _byte;
            public int total;
            public int line;

            public XState state;
            public XState nextstate;        /* Used for '@' state remembering and for the "string" consuming state */
            public byte[] stack;            /* Stack of element names + attribute/PI name, separated by \0. Also starts with a \0. */
            public int stacksize;
            public int stacklen;
            public int reflen;
            public int quote;
            public byte ignore;
            public string xstring;
        }

        public static void xml_init(ref SXML x)
        {
            x.data = new byte[8];
            x._byte = 0;
            x.total = 0;
            x.line = 1;
            x.state = XState.xmlS_init;
            x.nextstate = 0;                 /* Used for '@' state remembering and for the "string" consuming state */
            x.stacksize = 4096;
            x.stacklen = 0;
            x.reflen = 0;
            x.quote = 0;
            x.ignore = 0;
            x.xstring = "";

            x.stack = new byte[4096];        /* Stack of element names + attribute/PI name, separated by \0. Also starts with a \0. */
            x.attr = 0;
            x.elem = 0;
            x.pi = 0;
        }

        private static bool xml_isSP(byte c)
        {
            return (c == 0x20 || c == 0x09 || c == 0x0A);
            /* 0x0D should be part of SP, too, but xml_parse() already normalizes that into 0x0A */
        }

        private static bool xml_isAlpha(byte c)
        {
            c |= 32;
            c -= (byte)'a';
            return (c < 26);
        }

        private static bool xml_isNum(byte c)
        {
            c -= (byte)'0';
            return (c < 10);
        }

        private static bool xml_isHex(byte c)
        {
            byte n = (byte)(c | 32);
            n -= (byte)'a';
            return (xml_isNum(c) || (n < 6));
        }

        private static bool xml_isEncName(byte c)
        {
            return (xml_isAlpha(c) || xml_isNum(c) || (c == '.') || (c == '_') || (c == '-'));
        }

        private static bool xml_isNameStart(byte c)
        {
            return (xml_isAlpha(c) || (c == ':') || (c == '_') || (c >= 128));
        }

        private static bool xml_isName(byte c)
        {
            return (xml_isNameStart(c) || xml_isNum(c) || (c == '-') || (c == '.'));
        }

        /* XXX: The valid characters are dependent on the quote char, hence the access to x.quote */
        private static bool xml_isAttValue(ref SXML x, byte c)
        {
            return (c != x.quote && c != '<' && c != '&');
            /* Anything between '&' and ';', the xml_ref* functions will do further
             * validation. Strictly speaking, this is "xml_isName(c) || c == '#'", but
             * this parser doesn't understand entities with '.', ':', etc, anwyay.  */
        }

        private static bool xml_isRef(byte c)
        {
            return (xml_isNum(c) || xml_isAlpha(c) || (c == '#'));
        }

        private static ulong INTFROM5CHARS(int a, int b, int c, int d, int e)
        {
            return ((((ulong)((byte)a)) << 32) | (((ulong)((byte)b)) << 24) | (((ulong)((byte)c)) << 16) | (((ulong)((byte)d)) << 8) | (byte)e);
        }

        /* Similar to xml_setchar(), but will convert ch (any valid unicode point) to
         * UTF-8 and appends a '\0'. dest must have room for at least 5 bytes. */
/*        static void xml_setUTF8(char* dest, int ch)
        {
            if (ch <= 0x007F)
                xml_setchar(dest++, ch);
            else if (ch <= 0x07FF)
            {
                xml_setchar(dest++, 0xC0 | (ch >> 6));
                xml_setchar(dest++, 0x80 | (ch & 0x3F));
            }
            else if (ch <= 0xFFFF)
            {
                xml_setchar(dest++, 0xE0 | (ch >> 12));
                xml_setchar(dest++, 0x80 | ((ch >> 6) & 0x3F));
                xml_setchar(dest++, 0x80 | (ch & 0x3F));
            }
            else
            {
                xml_setchar(dest++, 0xF0 | (ch >> 18));
                xml_setchar(dest++, 0x80 | ((ch >> 12) & 0x3F));
                xml_setchar(dest++, 0x80 | ((ch >> 6) & 0x3F));
                xml_setchar(dest++, 0x80 | (ch & 0x3F));
            }
            *dest = 0;
        }*/

        private static XRet xml_datacontent(ref SXML x, byte ch)
        {
            x.data[0] = ch;
            x.data[1] = 0;
            return XRet.CONTENT;
        }

        private static XRet xml_datapi1(ref SXML x, byte ch)
        {
            x.data[0] = ch;
            x.data[1] = 0;
            return XRet.PICONTENT;
        }

        private static XRet xml_datapi2(ref SXML x, byte ch)
        {
            x.data[0] = 63; //'?'
            x.data[1] =  ch;
            x.data[2] = 0;
            return XRet.PICONTENT;
        }

        private static XRet xml_datacd1(ref SXML x, byte ch)
        {
            x.data[0] = (byte)']';
            x.data[1] = ch;
            x.data[2] = 0;
            return XRet.CONTENT;
        }


        private static XRet xml_datacd2(ref SXML x, byte ch)
        {
            x.data[0] = (byte)']';
            x.data[1] = (byte)']';
            x.data[2] = ch;
            x.data[3] = 0;
            return XRet.CONTENT;
        }


        private static XRet xml_dataattr(ref SXML x, byte ch)
        {
            /* Normalize attribute values according to the XML spec section 3.3.3. */
            x.data[0] = ((ch == 0x09) || (ch == 0x0A)) ? (byte)0x20 : ch;
            x.data[1] = 0;
            return XRet.ATTRVAL;
        }


        private static XRet xml_pushstack(ref SXML x, ref int res, byte ch)
        {
            if (x.stacklen + 2 >= x.stacksize)
                return XRet.ESTACK;
            x.stacklen++;
            res = x.stacklen;
            x.stack[x.stacklen] = ch;
            x.stacklen++;
            x.stack[x.stacklen] = 0;
            return XRet.OK;
        }


        private static XRet xml_pushstackc(ref SXML x, byte ch)
        {
            if (x.stacklen + 1 >= x.stacksize)
                return XRet.ESTACK;
            x.stack[x.stacklen] = ch;
            x.stacklen++;
            x.stack[x.stacklen] = 0;
            return XRet.OK;
        }


        private static void xml_popstack(ref SXML x)
        {
            do
                x.stacklen--;
            while (x.stack[x.stacklen] != 0);
        }

        private static XRet xml_elemstart(ref SXML x, byte ch) { return xml_pushstack(ref x, ref x.elem, ch); }
        private static XRet xml_elemname(ref SXML x, byte ch) { return xml_pushstackc(ref x, ch); }
        private static XRet xml_elemnameend(ref SXML x, byte ch) { return XRet.ELEMSTART; }


        /* Also used in xml_elemcloseend(), since this function just removes the last
         * element from the stack and returns ELEMEND. */
        private static XRet xml_selfclose(ref SXML x, byte ch)
        {
            xml_popstack(ref x);
            if (x.stacklen > 0)
            {
                x.elem = x.stacklen - 1;
                while (x.stack[x.elem - 1] != 0)
                    x.elem--;
                return XRet.ELEMEND;
            }
            x.elem = 0;
            x.state = XState.xmlS_misc3;
            return XRet.ELEMEND;
        }


        private static XRet xml_elemclose(ref SXML x, byte ch)
        {
            if (x.stack[x.elem] != ch)
		        return XRet.ECLOSE;
            x.elem++;
            return XRet.OK;
        }


        private static XRet xml_elemcloseend(ref SXML x, byte ch)
        {
            if (x.stack[x.elem] != 0)
                return XRet.ECLOSE;
            return xml_selfclose(ref x, ch);
        }


        private static XRet xml_attrstart(ref SXML x, byte ch) { return xml_pushstack(ref x,ref x.attr, ch); }
        private static XRet xml_attrname(ref SXML x, byte ch) { return xml_pushstackc(ref x, ch); }
        private static XRet xml_attrnameend(ref SXML x, byte ch) { return XRet.ATTRSTART; }
        private static XRet xml_attrvalend(ref SXML x, byte ch) { xml_popstack(ref x); return XRet.ATTREND; }


        private static XRet xml_pistart(ref SXML x, byte ch) { return xml_pushstack(ref x, ref x.pi, ch); }
        private static XRet xml_piname(ref SXML x, byte ch) { return xml_pushstackc(ref x, ch); }
        private static XRet xml_piabort(ref SXML x, byte ch) { xml_popstack(ref x); return XRet.OK; }
        private static XRet xml_pinameend(ref SXML x, byte ch)
        {
            return (x.stack[x.pi + 0] | 32) == 'x' && (x.stack[x.pi + 1] | 32) == 'm' && (x.stack[x.pi + 2] | 32) == 'l' && (x.stack[x.pi + 3] != 0) ? XRet.ESYN : XRet.PISTART;
        }

        private static XRet xml_pivalend(ref SXML x, byte ch)
        {
            xml_popstack(ref x);
            x.pi = 0;
            return XRet.PIEND;
        }


        private static XRet xml_refstart(ref SXML x, byte ch)
        {
            for (int i = 0; i < 8; i++)
                x.data[i] = 0;
            x.reflen = 0;
            return XRet.OK;
        }


        private static XRet xml_ref(ref SXML x, byte ch)
        {
            if (x.reflen < 7)
            { 
                x.data[x.reflen] = ch;
                x.reflen++;
                return XRet.OK;
            }
            return XRet.EREF;
        }

        private static XRet xml_refend(ref SXML x, XRet ret)
        {
            int ch = 0;
            if (x.data[0] == '#')
            {
                int r;
                if (x.data[1] == 'x')
                {
                    for (r = 2; xml_isHex(x.data[r]); r++)
                        ch = (ch << 4) + (x.data[r] <= '9' ? x.data[r] - '0' : (x.data[r] | 32) - 'a' + 10);
                }
                else
                {
                    for (r = 1; xml_isNum(x.data[r]); r++)
                        ch = (ch * 10) + (x.data[r] - '0');
                }

                if (x.data[r] != 0)
                    ch = 0;
            }
            else
            {
                ulong i = INTFROM5CHARS(x.data[0], x.data[1], x.data[2], x.data[3], x.data[4]);
                ch =
                    i == INTFROM5CHARS('l', 't', 0, 0, 0) ? '<' :
                    i == INTFROM5CHARS('g', 't', 0, 0, 0) ? '>' :
                    i == INTFROM5CHARS('a', 'm', 'p', 0, 0) ? '&' :
                    i == INTFROM5CHARS('a', 'p', 'o', 's', 0) ? '\'' :
                    i == INTFROM5CHARS('q', 'u', 'o', 't', 0) ? '"' : 0;
            }

            /* Codepoints not allowed in the XML 1.1 definition of a Char */
            //if (ch == 0 || ch > 0x10FFFF || ch == 0xFFFE || ch == 0xFFFF || (ch - 0xDFFF) < 0x7FF)
            return XRet.EREF;
            //xml_setutf8(x.data, ch);
            //return ret;
        }

        private static XRet xml_refcontent(ref SXML x, byte ch) { return xml_refend(ref x, XRet.CONTENT); }
        private static XRet xml_refattrval(ref SXML x, byte ch) { return xml_refend(ref x, XRet.ATTRVAL); }

        public static XRet xml_parse(ref SXML x, byte ch)
        {
            if (ch == 0)
                return XRet.ESYN;

            x.total++;

            /* End-of-Line normalization, "\rX", "\r\n" and "\n" are recognized and
             * normalized to a single '\n' as per XML 1.0 section 2.11. XML 1.1 adds
             * some non-ASCII character sequences to this list, but we can only handle
             * ASCII here without making assumptions about the input encoding. */
            if (x.ignore == ch)
            {
                x.ignore = 0x00;
                return XRet.OK;
            }

            x.ignore = (ch == 0x0D) ? (byte)0xA : (byte)0x00;
            if (ch == 0xa || ch == 0xd)
            {
                ch = 0xA;
                x.line++;
                x._byte = 0;
            }
            x._byte++;

            switch (x.state)
            {
                case XState.xmlS_string:
                    if (x.xstring.Length == 0)
                        break;

                    if (ch == (byte)x.xstring[0])
                    {
                        x.xstring = x.xstring.Substring(1);
                        if (x.xstring.Length == 0)
				            x.state = x.nextstate;
                        return XRet.OK;
                    }
                    break;

                case XState.xmlS_attr0:
                    if (xml_isName(ch))
                        return xml_attrname(ref x, ch);
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_attr1;
                        return xml_attrnameend(ref x, ch);
                    }
                    if (ch == '=') {
                        x.state = XState.xmlS_attr2;
                        return xml_attrnameend(ref x, ch);
                    }
                    break;
                case XState.xmlS_attr1:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '=') {
                        x.state = XState.xmlS_attr2;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_attr2:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '\'' || ch == '"') {
                        x.state = XState.xmlS_attr3;
                        x.quote = ch;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_attr3:
                    if (xml_isAttValue(ref x, ch))
                        return xml_dataattr(ref x, ch);
                    if (ch == '&') {
                        x.state = XState.xmlS_attr4;
                        return xml_refstart(ref x, ch);
                    }
                    if (x.quote == ch)
                    {
                        x.state = XState.xmlS_elem2;
                        return xml_attrvalend(ref x, ch);
                    }
                    break;
                case XState.xmlS_attr4:
                    if (xml_isRef(ch))
                        return xml_ref(ref x, ch);
                    if (ch == '\x3b') {
                        x.state = XState.xmlS_attr3;
                        return xml_refattrval(ref x, ch);
                    }
                    break;
                case XState.xmlS_cd0:
                    if (ch == ']') {
                        x.state = XState.xmlS_cd1;
                        return XRet.OK;
                    }
                    return xml_datacontent(ref x, ch);

                case XState.xmlS_cd1:
                    if (ch == ']') {
                        x.state = XState.xmlS_cd2;
                        return XRet.OK;
                    }
                    x.state = XState.xmlS_cd0;
                    return xml_datacd1(ref x, ch);

                case XState.xmlS_cd2:
                    if (ch == ']')
                        return xml_datacontent(ref x, ch);
                    if (ch == '>')
                    {
                        x.state = XState.xmlS_misc2;
                        return XRet.OK;
                    }

                    x.state = XState.xmlS_cd0;
                    return xml_datacd2(ref x, ch);

                case XState.xmlS_comment0:
                    if (ch == '-') {
                        x.state = XState.xmlS_comment1;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_comment1:
                    if (ch == '-') {
                        x.state = XState.xmlS_comment2;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_comment2:
                    if (ch == '-') {
                        x.state = XState.xmlS_comment3;
                        return XRet.OK;
                    }
                    return XRet.OK;

                case XState.xmlS_comment3:
                    if (ch == '-') {
                        x.state = XState.xmlS_comment4;
                        return XRet.OK;
                    }
                    x.state = XState.xmlS_comment2;
                    return XRet.OK;

                case XState.xmlS_comment4:
                    if (ch == '>') {
                        x.state = x.nextstate;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_dt0:
                    if (ch == '>') {
                        x.state = XState.xmlS_misc1;
                        return XRet.OK;
                    }
                    if (ch == '\'' || ch == '"') {
                        x.state = XState.xmlS_dt1;
                        x.quote = ch;
                        x.nextstate = XState.xmlS_dt0;
                        return XRet.OK;
                    }
                    if (ch == '<') {
                        x.state = XState.xmlS_dt2;
                        return XRet.OK;
                    }
                    return XRet.OK;

                case XState.xmlS_dt1:
                    if (x.quote == ch)
                    {
                        x.state = x.nextstate;
                        return XRet.OK;
                    }
                    return XRet.OK;

                case XState.xmlS_dt2:
                    if (ch == '?') {
                        x.state = XState.xmlS_pi0;
                        x.nextstate = XState.xmlS_dt0;
                        return XRet.OK;
                    }
                    if (ch == '!') {
                        x.state = XState.xmlS_dt3;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_dt3:
                    if (ch == '-') {
                        x.state = XState.xmlS_comment1;
                        x.nextstate = XState.xmlS_dt0;
                        return XRet.OK;
                    }
                    x.state = XState.xmlS_dt4;
                    return XRet.OK;

                case XState.xmlS_dt4:
                    if (ch == '\'' || ch == '"')
                    {
                        x.state = XState.xmlS_dt1;
                        x.quote = ch;
                        x.nextstate = XState.xmlS_dt4;
                        return XRet.OK;
                    }
                    if (ch == '>') {
                        x.state = XState.xmlS_dt0;
                        return XRet.OK;
                    }
                    return XRet.OK;

                case XState.xmlS_elem0:
                    if (xml_isName(ch))
                        return xml_elemname(ref x, ch);
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_elem1;
                        return xml_elemnameend(ref x, ch);
                    }
                    if (ch == '/') {
                        x.state = XState.xmlS_elem3;
                        return xml_elemnameend(ref x, ch);
                    }
                    if (ch == '>') {
                        x.state = XState.xmlS_misc2;
                        return xml_elemnameend(ref x, ch);
                    }
                    break;
                case XState.xmlS_elem1:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '/') {
                        x.state = XState.xmlS_elem3;
                        return XRet.OK;
                    }
                    if (ch == '>') {
                        x.state = XState.xmlS_misc2;
                        return XRet.OK;
                    }
                    if (xml_isNameStart(ch))
                    {
                        x.state = XState.xmlS_attr0;
                        return xml_attrstart(ref x, ch);
                    }
                    break;
                case XState.xmlS_elem2:
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_elem1;
                        return XRet.OK;
                    }
                    if (ch == '/') {
                        x.state = XState.xmlS_elem3;
                        return XRet.OK;
                    }
                    if (ch == '>') {
                        x.state = XState.xmlS_misc2;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_elem3:
                    if (ch == '>') {
                        x.state = XState.xmlS_misc2;
                        return xml_selfclose(ref x, ch);
                    }
                    break;
                case XState.xmlS_enc0:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '=') {
                        x.state = XState.xmlS_enc1;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_enc1:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '\'' || ch == '"') {
                        x.state = XState.xmlS_enc2;
                        x.quote = ch;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_enc2:
                    if (xml_isAlpha(ch))
                    {
                        x.state = XState.xmlS_enc3;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_enc3:
                    if (xml_isEncName(ch))
                        return XRet.OK;
                    if (x.quote == ch)
                    {
                        x.state = XState.xmlS_xmldecl6;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_etag0:
                    if (xml_isNameStart(ch))
                    {
                        x.state = XState.xmlS_etag1;
                        return xml_elemclose(ref x, ch);
                    }
                    break;
                case XState.xmlS_etag1:
                    if (xml_isName(ch))
                        return xml_elemclose(ref x, ch);
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_etag2;
                        return xml_elemcloseend(ref x, ch);
                    }
                    if (ch == '>') {
                        x.state = XState.xmlS_misc2;
                        return xml_elemcloseend(ref x, ch);
                    }
                    break;
                case XState.xmlS_etag2:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '>') {
                        x.state = XState.xmlS_misc2;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_init:
                    if (ch == '\xef') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_misc0;
                        x.xstring = "\xbb\xbf";
                        return XRet.OK;
                    }
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_misc0;
                        return XRet.OK;
                    }
                    if (ch == '<') {
                        x.state = XState.xmlS_le0;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_le0:
                    if (ch == '!') {
                        x.state = XState.xmlS_lee1;
                        return XRet.OK;
                    }
                    if (ch == '?') {
                        x.state = XState.xmlS_leq0;
                        return XRet.OK;
                    }
                    if (xml_isNameStart(ch))
                    {
                        x.state = XState.xmlS_elem0;
                        return xml_elemstart(ref x, ch);
                    }
                    break;
                case XState.xmlS_le1:
                    if (ch == '!') {
                        x.state = XState.xmlS_lee1;
                        return XRet.OK;
                    }
                    if (ch == '?') {
                        x.state = XState.xmlS_pi0;
                        x.nextstate = XState.xmlS_misc1;
                        return XRet.OK;
                    }
                    if (xml_isNameStart(ch))
                    {
                        x.state = XState.xmlS_elem0;
                        return xml_elemstart(ref x, ch);
                    }
                    break;
                case XState.xmlS_le2:
                    if (ch == '!') {
                        x.state = XState.xmlS_lee2;
                        return XRet.OK;
                    }
                    if (ch == '?') {
                        x.state = XState.xmlS_pi0;
                        x.nextstate = XState.xmlS_misc2;
                        return XRet.OK;
                    }
                    if (ch == '/') {
                        x.state = XState.xmlS_etag0;
                        return XRet.OK;
                    }
                    if (xml_isNameStart(ch))
                    {
                        x.state = XState.xmlS_elem0;
                        return xml_elemstart(ref x, ch);
                    }
                    break;
                case XState.xmlS_le3:
                    if (ch == '!') {
                        x.state = XState.xmlS_comment0;
                        x.nextstate = XState.xmlS_misc3;
                        return XRet.OK;
                    }
                    if (ch == '?') {
                        x.state = XState.xmlS_pi0;
                        x.nextstate = XState.xmlS_misc3;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_lee1:
                    if (ch == '-') {
                        x.state = XState.xmlS_comment1;
                        x.nextstate = XState.xmlS_misc1;
                        return XRet.OK;
                    }
                    if (ch == 'D') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_dt0;
                        x.xstring = "OCTYPE";
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_lee2:
                    if (ch == '-') {
                        x.state = XState.xmlS_comment1;
                        x.nextstate = XState.xmlS_misc2;
                        return XRet.OK;
                    }
                    if (ch == '[') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_cd0;
                        x.xstring = "CDATA[";
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_leq0:
                    if (ch == 'x') {
                        x.state = XState.xmlS_xmldecl0;
                        x.nextstate = XState.xmlS_misc1;
                        return xml_pistart(ref x, ch);
                    }
                    if (xml_isNameStart(ch))
                    {
                        x.state = XState.xmlS_pi1;
                        x.nextstate = XState.xmlS_misc1;
                        return xml_pistart(ref x, ch);
                    }
                    break;
                case XState.xmlS_misc0:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '<') {
                        x.state = XState.xmlS_le0;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_misc1:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '<') {
                        x.state = XState.xmlS_le1;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_misc2:
                    if (ch == '<') {
                        x.state = XState.xmlS_le2;
                        return XRet.OK;
                    }
                    if (ch == '&') {
                        x.state = XState.xmlS_misc2a;
                        return xml_refstart(ref x, ch);
                    }
                    return xml_datacontent(ref x, ch);

                case XState.xmlS_misc2a:
                    if (xml_isRef(ch))
                        return xml_ref(ref x, ch);
                    if (ch == '\x3b') {
                        x.state = XState.xmlS_misc2;
                        return xml_refcontent(ref x, ch);
                    }
                    break;
                case XState.xmlS_misc3:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '<') {
                        x.state = XState.xmlS_le3;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_pi0:
                    if (xml_isNameStart(ch))
                    {
                        x.state = XState.xmlS_pi1;
                        return xml_pistart(ref x, ch);
                    }
                    break;
                case XState.xmlS_pi1:
                    if (xml_isName(ch))
                        return xml_piname(ref x, ch);
                    if (ch == '?') {
                        x.state = XState.xmlS_pi4;
                        return xml_pinameend(ref x, ch);
                    }
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_pi2;
                        return xml_pinameend(ref x, ch);
                    }
                    break;
                case XState.xmlS_pi2:
                    if (ch == '?') {
                        x.state = XState.xmlS_pi3;
                        return XRet.OK;
                    }
                    return xml_datapi1(ref x, ch);

                case XState.xmlS_pi3:
                    if (ch == '>') {
                        x.state = x.nextstate;
                        return xml_pivalend(ref x, ch);
                    }
                    x.state = XState.xmlS_pi2;
                    return xml_datapi2(ref x, ch);

                case XState.xmlS_pi4:
                    if (ch == '>') {
                        x.state = x.nextstate;
                        return xml_pivalend(ref x, ch);
                    }
                    break;
                case XState.xmlS_std0:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '=') {
                        x.state = XState.xmlS_std1;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_std1:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '\'' || ch == '"') {
                        x.state = XState.xmlS_std2;
                        x.quote = ch;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_std2:
                    if (ch == 'y') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_std3;
                        x.xstring = "es";
                        return XRet.OK;
                    }
                    if (ch == 'n') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_std3;
                        x.xstring = "o";
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_std3:
                    if (x.quote == ch)
                    {
                        x.state = XState.xmlS_xmldecl8;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_ver0:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '=') {
                        x.state = XState.xmlS_ver1;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_ver1:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '\'' || ch == '"') {
                        x.state = XState.xmlS_string;
                        x.quote = ch;
                        x.nextstate = XState.xmlS_ver2;
                        x.xstring = "1.";
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_ver2:
                    if (xml_isNum(ch))
                    {
                        x.state = XState.xmlS_ver3;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_ver3:
                    if (xml_isNum(ch))
                        return XRet.OK;
                    if (x.quote == ch)
                    {
                        x.state = XState.xmlS_xmldecl4;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_xmldecl0:
                    if (ch == 'm') {
                        x.state = XState.xmlS_xmldecl1;
                        return xml_piname(ref x, ch);
                    }
                    if (xml_isName(ch))
                    {
                        x.state = XState.xmlS_pi1;
                        return xml_piname(ref x, ch);
                    }
                    if (ch == '?') {
                        x.state = XState.xmlS_pi4;
                        return xml_pinameend(ref x, ch);
                    }
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_pi2;
                        return xml_pinameend(ref x, ch);
                    }
                    break;
                case XState.xmlS_xmldecl1:
                    if (ch == 'l') {
                        x.state = XState.xmlS_xmldecl2;
                        return xml_piname(ref x, ch);
                    }
                    if (xml_isName(ch))
                    {
                        x.state = XState.xmlS_pi1;
                        return xml_piname(ref x, ch);
                    }
                    if (ch == '?') {
                        x.state = XState.xmlS_pi4;
                        return xml_pinameend(ref x, ch);
                    }
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_pi2;
                        return xml_pinameend(ref x, ch);
                    }
                    break;
                case XState.xmlS_xmldecl2:
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_xmldecl3;
                        return xml_piabort(ref x, ch);
                    }
                    if (xml_isName(ch))
                    {
                        x.state = XState.xmlS_pi1;
                        return xml_piname(ref x, ch);
                    }
                    break;
                case XState.xmlS_xmldecl3:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == 'v') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_ver0;
                        x.xstring = "ersion";
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_xmldecl4:
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_xmldecl5;
                        return XRet.OK;
                    }
                    if (ch == '?') {
                        x.state = XState.xmlS_xmldecl9;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_xmldecl5:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '?') {
                        x.state = XState.xmlS_xmldecl9;
                        return XRet.OK;
                    }
                    if (ch == 'e') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_enc0;
                        x.xstring = "ncoding";
                        return XRet.OK;
                    }
                    if (ch == 's') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_std0;
                        x.xstring = "tandalone";
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_xmldecl6:
                    if (xml_isSP(ch))
                    {
                        x.state = XState.xmlS_xmldecl7;
                        return XRet.OK;
                    }
                    if (ch == '?') {
                        x.state = XState.xmlS_xmldecl9;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_xmldecl7:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '?') {
                        x.state = XState.xmlS_xmldecl9;
                        return XRet.OK;
                    }
                    if (ch == 's') {
                        x.state = XState.xmlS_string;
                        x.nextstate = XState.xmlS_std0;
                        x.xstring = "tandalone";
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_xmldecl8:
                    if (xml_isSP(ch))
                        return XRet.OK;
                    if (ch == '?') {
                        x.state = XState.xmlS_xmldecl9;
                        return XRet.OK;
                    }
                    break;
                case XState.xmlS_xmldecl9:
                    if (ch == '>') {
                        x.state = XState.xmlS_misc1;
                        return XRet.OK;
                    }
                    break;
            }
            return XRet.ESYN;
        }

        public static bool parse_file(string fname, ref XML_PARSE_MAP[] map)
        {
	        string attrPath = "";
            string attrValue = "";
            SXML xml = new SXML();
            string f = null;
            XRet r;

            Stopwatch sw = new Stopwatch();
            sw.Restart();

            while (true)
            {
                try { f = File.ReadAllText(fname); }
                catch { f = null; }
                if (f != null)
                    break;
                if (sw.ElapsedMilliseconds > 2000)
                    return false;
            }
	        xml_init(ref xml);
	        foreach (char ch in f)
	        {
        		r = xml_parse(ref xml, Convert.ToByte(ch));
		        if (r < XRet.OK)
    			    return false;
	
                if ((r == XRet.ATTRSTART) || (r == XRet.ELEMSTART))
		        {
			        attrPath = "";
			        attrValue = "";
			        for (int i = 0; i < xml.stacklen; i++)
				        attrPath += (xml.stack[i] == 0) ? '/' : (char)xml.stack[i];
		        }
		        else if ((r == XRet.ATTRVAL) || (r == XRet.CONTENT))
			        attrValue += (char) xml.data[0];
		        else if ((r == XRet.ATTREND) || (r == XRet.ELEMEND))
		        {
			        if ((string.IsNullOrWhiteSpace(attrPath) == false) && (string.IsNullOrWhiteSpace(attrValue) == false))
			        {
				        for (int i = 0; i < map.Length; i++)
				        {
					        if (map[i].NodePath.ToLowerInvariant().Trim() == attrPath.ToLowerInvariant().Trim())
					        {
						        map[i].callback(attrValue, ref map[i].flags, map[i].dst);
						        break;
					        }
				        }
			        }

			        attrPath = "";
			        attrValue = "";			
		        }
	        }

        	// file parsing complete, now scan for errors and check if all mandatory nodes was found
	        foreach (XML_PARSE_MAP m in map)
	        {
		        byte flg = m.flags;
		        if ((flg != XML.MAP_FLAG_OPTIONAL) && (flg != XML.MAP_FLAG_FOUND) && (flg != (XML.MAP_FLAG_FOUND | XML.MAP_FLAG_MANDATORY)))
			        return false;
	        }

        	return true;
        }

    }
}