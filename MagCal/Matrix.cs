﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WinBridge
{
	partial class MagCal
	{
		const double CORRUPTMATRIX = 0.001;         // column vector modulus limit for rotation matrix

		// vector components
		const int X = 0;
		const int Y = 1;
		const int Z = 2;

		// function sets the 3x3 matrix A to the identity matrix
		void f3x3matrixAeqI(ref double[,] A)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
					A[i, j] = 0.0;
				A[i, i] = 1.0;
			}
		}

		// function sets the matrix A to the identity matrix
		void fmatrixAeqI(ref double[,] A, int rc)
		{
			// rc = rows and columns in A
			for (int i = 0; i < rc; i++)
			{
				for (int j = 0; j < rc; j++)
					A[i, j] = 0.0;
				A[i, i] = 1.0;
			}
		}

		// function sets every entry in the 3x3 matrix A to a constant scalar
		void f3x3matrixAeqScalar(ref double[,] A, double Scalar)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
					A[i, j] = Scalar;
			}
		}

		// function multiplies all elements of 3x3 matrix A by the specified scalar
		void f3x3matrixAeqAxScalar(ref double[,] A, double Scalar)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
					A[i, j] *= Scalar;
			}
		}

		// function negates all elements of 3x3 matrix A
		void f3x3matrixAeqMinusA(ref double[,] A)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
					A[i, j] = -A[i, j];
			}
		}

		// function directly calculates the symmetric inverse of a symmetric 3x3 matrix
		// only the on and above diagonal terms in B are used and need to be specified
		void f3x3matrixAeqInvSymB(ref double[,] A, ref double[,] B)
		{
			double fB11B22mB12B12;   // B[1,1] * B[2,2] - B[1,2] * B[1,2]
			double fB12B02mB01B22;   // B[1,2] * B[0,2] - B[0,1] * B[2,2]
			double fB01B12mB11B02;   // B[0,1] * B[1,2] - B[1,1] * B[0,2]
			double ftmp;             // determinant and then reciprocal

			// calculate useful products
			fB11B22mB12B12 = B[1, 1] * B[2, 2] - B[1, 2] * B[1, 2];
			fB12B02mB01B22 = B[1, 2] * B[0, 2] - B[0, 1] * B[2, 2];
			fB01B12mB11B02 = B[0, 1] * B[1, 2] - B[1, 1] * B[0, 2];

			// set ftmp to the determinant of the input matrix B
			ftmp = B[0, 0] * fB11B22mB12B12 + B[0, 1] * fB12B02mB01B22 + B[0, 2] * fB01B12mB11B02;

			// set A to the inverse of B for any determinant except zero
			if (ftmp != 0.0F)
			{
				ftmp = 1.0F / ftmp;
				A[0, 0] = fB11B22mB12B12 * ftmp;
				A[1, 0] = A[0, 1] = fB12B02mB01B22 * ftmp;
				A[2, 0] = A[0, 2] = fB01B12mB11B02 * ftmp;
				A[1, 1] = (B[0, 0] * B[2, 2] - B[0, 2] * B[0, 2]) * ftmp;
				A[2, 1] = A[1, 2] = (B[0, 2] * B[0, 1] - B[0, 0] * B[1, 2]) * ftmp;
				A[2, 2] = (B[0, 0] * B[1, 1] - B[0, 1] * B[0, 1]) * ftmp;
			}
			else
			{
				// provide the identity matrix if the determinant is zero
				f3x3matrixAeqI(ref A);
			}
		}

		// function calculates the determinant of a 3x3 matrix
		double f3x3matrixDetA(ref double[,] A)
		{
			return (A[X, X] * (A[Y, Y] * A[Z, Z] - A[Y, Z] * A[Z, Y]) +
					A[X, Y] * (A[Y, Z] * A[Z, X] - A[Y, X] * A[Z, Z]) +
					A[X, Z] * (A[Y, X] * A[Z, Y] - A[Y, Y] * A[Z, X]));
		}

		// function computes all eigenvalues and eigenvectors of a real symmetric matrix A[0..n-1,0..n-1]
		// stored in the top left of a 10x10 array A[10,10]
		// A[,] is changed on output.
		// eigval[0..n-1] returns the eigenvalues of A[,].
		// eigvec[0..n-1,0..n-1] returns the normalized eigenvectors of A[,]
		// the eigenvectors are not sorted by value
		void eigencompute(ref double[,] A, ref double[] eigval, ref double[,] eigvec, byte n)
		{
			// maximum number of iterations to achieve convergence: in practice 6 is typical
			const double NITERATIONS = 15;

			// various trig functions of the jacobi rotation angle phi
			double cot2phi, tanhalfphi, tanphi, sinphi, cosphi;
			// scratch variable to prevent over-writing during rotations
			double ftmp;
			// residue from remaining non-zero above diagonal terms
			double residue;
			// matrix row and column indices
			int ir, ic;
			// general loop counter
			int j;
			// timeout ctr for number of passes of the algorithm
			byte ctr;

			// initialize eigenvectors matrix and eigenvalues array
			for (ir = 0; ir < n; ir++)
			{
				// loop over all columns
				for (ic = 0; ic < n; ic++)
				{
					// set on diagonal and off-diagonal elements to zero
					eigvec[ir, ic] = 0.0;
				}

				// correct the diagonal elements to 1.0
				eigvec[ir, ir] = 1.0;

				// initialize the array of eigenvalues to the diagonal elements of m
				eigval[ir] = A[ir, ir];
			}

			// initialize the counter and loop until converged or NITERATIONS reached
			ctr = 0;
			do
			{
				// compute the absolute value of the above diagonal elements as exit criterion
				residue = 0.0;
				// loop over rows excluding last row
				for (ir = 0; ir < n - 1; ir++)
				{
					// loop over above diagonal columns
					for (ic = ir + 1; ic < n; ic++)
					{
						// accumulate the residual off diagonal terms which are being driven to zero
						residue += Math.Abs(A[ir, ic]);
					}
				}

				// check if we still have work to do
				if (residue > 0.0F)
				{
					// loop over all rows with the exception of the last row (since only rotating above diagonal elements)
					for (ir = 0; ir < n - 1; ir++)
					{
						// loop over columns ic (where ic is always greater than ir since above diagonal)
						for (ic = ir + 1; ic < n; ic++)
						{
							// only continue with this element if the element is non-zero
							if (Math.Abs(A[ir, ic]) > 0.0F)
							{
								// calculate cot(2*phi) where phi is the Jacobi rotation angle
								cot2phi = 0.5F * (eigval[ic] - eigval[ir]) / (A[ir, ic]);

								// calculate tan(phi) correcting sign to ensure the smaller solution is used
								tanphi = 1.0F / (Math.Abs(cot2phi) + Math.Sqrt(1.0F + cot2phi * cot2phi));
								if (cot2phi < 0.0F)
								{
									tanphi = -tanphi;
								}

								// calculate the sine and cosine of the Jacobi rotation angle phi
								cosphi = 1.0F / Math.Sqrt(1.0F + tanphi * tanphi);
								sinphi = tanphi * cosphi;

								// calculate tan(phi/2)
								tanhalfphi = sinphi / (1.0F + cosphi);

								// set tmp = tan(phi) times current matrix element used in update of leading diagonal elements
								ftmp = tanphi * A[ir, ic];

								// apply the jacobi rotation to diagonal elements [ir,ir] and [ic,ic] stored in the eigenvalue array
								// eigval[ir] = eigval[ir] - tan(phi) *  A[ir,ic]
								eigval[ir] -= ftmp;
								// eigval[ic] = eigval[ic] + tan(phi) * A[ir,ic]
								eigval[ic] += ftmp;

								// by definition, applying the jacobi rotation on element ir, ic results in 0.0
								A[ir, ic] = 0.0;

								// apply the jacobi rotation to all elements of the eigenvector matrix
								for (j = 0; j < n; j++)
								{
									// store eigvec[j,ir]
									ftmp = eigvec[j, ir];
									// eigvec[j,ir] = eigvec[j,ir] - sin(phi) * (eigvec[j,ic] + tan(phi/2) * eigvec[j,ir])
									eigvec[j, ir] = ftmp - sinphi * (eigvec[j, ic] + tanhalfphi * ftmp);
									// eigvec[j,ic] = eigvec[j,ic] + sin(phi) * (eigvec[j,ir] - tan(phi/2) * eigvec[j,ic])
									eigvec[j, ic] = eigvec[j, ic] + sinphi * (ftmp - tanhalfphi * eigvec[j, ic]);
								}

								// apply the jacobi rotation only to those elements of matrix m that can change
								for (j = 0; j <= ir - 1; j++)
								{
									// store A[j,ir]
									ftmp = A[j, ir];
									// A[j,ir] = A[j,ir] - sin(phi) * (A[j,ic] + tan(phi/2) * A[j,ir])
									A[j, ir] = ftmp - sinphi * (A[j, ic] + tanhalfphi * ftmp);
									// A[j,ic] = A[j,ic] + sin(phi) * (A[j,ir] - tan(phi/2) * A[j,ic])
									A[j, ic] = A[j, ic] + sinphi * (ftmp - tanhalfphi * A[j, ic]);
								}
								for (j = ir + 1; j <= ic - 1; j++)
								{
									// store A[ir,j]
									ftmp = A[ir, j];
									// A[ir,j] = A[ir,j] - sin(phi) * (A[j,ic] + tan(phi/2) * A[ir,j])
									A[ir, j] = ftmp - sinphi * (A[j, ic] + tanhalfphi * ftmp);
									// A[j,ic] = A[j,ic] + sin(phi) * (A[ir,j] - tan(phi/2) * A[j,ic])
									A[j, ic] = A[j, ic] + sinphi * (ftmp - tanhalfphi * A[j, ic]);
								}
								for (j = ic + 1; j < n; j++)
								{
									// store A[ir,j]
									ftmp = A[ir, j];
									// A[ir,j] = A[ir,j] - sin(phi) * (A[ic,j] + tan(phi/2) * A[ir,j])
									A[ir, j] = ftmp - sinphi * (A[ic, j] + tanhalfphi * ftmp);
									// A[ic,j] = A[ic,j] + sin(phi) * (A[ir,j] - tan(phi/2) * A[ic,j])
									A[ic, j] = A[ic, j] + sinphi * (ftmp - tanhalfphi * A[ic, j]);
								}
							}   // end of test for matrix element already zero
						}   // end of loop over columns
					}   // end of loop over rows
				}  // end of test for non-zero residue
			} while ((residue > 0.0F) && (ctr++ < NITERATIONS)); // end of main loop
		}

		// function uses Gauss-Jordan elimination to compute the inverse of matrix A in situ
		// on exit, A is replaced with its inverse
		void fmatrixAeqInvA(ref double[,] A, byte isize)
		{
			byte[] iColInd = new byte[4];
			byte[] iRowInd = new byte[4];
			byte[] iPivot = new byte[4];

			double largest;                  // largest element used for pivoting
			double scaling;                  // scaling factor in pivoting
			double recippiv;                 // reciprocal of pivot element
			double ftmp;                     // temporary variable used in swaps
			int i, j, k, l, m;           // index counters
			byte iPivotRow, iPivotCol;    // row and column of pivot element

			// to avoid compiler warnings
			iPivotRow = iPivotCol = 0;

			// initialize the pivot array to 0
			for (j = 0; j < isize; j++)
			{
				iPivot[j] = 0;
			}

			// main loop i over the dimensions of the square matrix A
			for (i = 0; i < isize; i++)
			{
				// zero the largest element found for pivoting
				largest = 0.0;
				// loop over candidate rows j
				for (j = 0; j < isize; j++)
				{
					// check if row j has been previously pivoted
					if (iPivot[j] != 1)
					{
						// loop over candidate columns k
						for (k = 0; k < isize; k++)
						{
							// check if column k has previously been pivoted
							if (iPivot[k] == 0)
							{
								// check if the pivot element is the largest found so far
								if (Math.Abs(A[j, k]) >= largest)
								{
									// and store this location as the current best candidate for pivoting
									iPivotRow = (byte)j;
									iPivotCol = (byte)k;
									largest = (double)Math.Abs(A[iPivotRow, iPivotCol]);
								}
							}
							else if (iPivot[k] > 1)
							{
								// zero determinant situation: exit with identity matrix
								fmatrixAeqI(ref A, isize);
								return;
							}
						}
					}
				}
				// increment the entry in iPivot to denote it has been selected for pivoting
				iPivot[iPivotCol]++;

				// check the pivot rows iPivotRow and iPivotCol are not the same before swapping
				if (iPivotRow != iPivotCol)
				{
					// loop over columns l
					for (l = 0; l < isize; l++)
					{
						// and swap all elements of rows iPivotRow and iPivotCol
						ftmp = A[iPivotRow, l];
						A[iPivotRow, l] = A[iPivotCol, l];
						A[iPivotCol, l] = ftmp;
					}
				}

				// record that on the i-th iteration rows iPivotRow and iPivotCol were swapped
				iRowInd[i] = iPivotRow;
				iColInd[i] = iPivotCol;

				// check for zero on-diagonal element (singular matrix) and return with identity matrix if detected
				if (A[iPivotCol, iPivotCol] == 0.0F)
				{
					// zero determinant situation: exit with identity matrix
					fmatrixAeqI(ref A, isize);
					return;
				}

				// calculate the reciprocal of the pivot element knowing it's non-zero
				recippiv = 1.0F / A[iPivotCol, iPivotCol];
				// by definition, the diagonal element normalizes to 1
				A[iPivotCol, iPivotCol] = 1.0;
				// multiply all of row iPivotCol by the reciprocal of the pivot element including the diagonal element
				// the diagonal element A[iPivotCol,iPivotCol] now has value equal to the reciprocal of its previous value
				for (l = 0; l < isize; l++)
				{
					A[iPivotCol, l] *= recippiv;
				}
				// loop over all rows m of A
				for (m = 0; m < isize; m++)
				{
					if (m != iPivotCol)
					{
						// scaling factor for this row m is in column iPivotCol
						scaling = A[m, iPivotCol];
						// zero this element
						A[m, iPivotCol] = 0.0;
						// loop over all columns l of A and perform elimination
						for (l = 0; l < isize; l++)
						{
							A[m, l] -= A[iPivotCol, l] * scaling;
						}
					}
				}
			} // end of loop i over the matrix dimensions

			// finally, loop in inverse order to apply the missing column swaps
			for (l = isize - 1; l >= 0; l--)
			{
				// set i and j to the two columns to be swapped
				i = iRowInd[l];
				j = iColInd[l];

				// check that the two columns i and j to be swapped are not the same
				if (i != j)
				{
					// loop over all rows k to swap columns i and j of A
					for (k = 0; k < isize; k++)
					{
						ftmp = A[k, i];
						A[k, i] = A[k, j];
						A[k, j] = ftmp;
					}
				}
			}
		}

		// function re-orthonormalizes a 3x3 rotation matrix
		void fmatrixAeqRenormRotA(double[,] A)
		{
			double ftmp;                 // scratch variable

			// normalize the X column of the low pass filtered orientation matrix
			ftmp = Math.Sqrt(A[X, X] * A[X, X] + A[Y, X] * A[Y, X] + A[Z, X] * A[Z, X]);
			if (ftmp > CORRUPTMATRIX)
			{
				// normalize the x column vector
				ftmp = 1.0F / ftmp;
				A[X, X] *= ftmp;
				A[Y, X] *= ftmp;
				A[Z, X] *= ftmp;
			}
			else
			{
				// set x column vector to {1, 0, 0}
				A[X, X] = 1.0;
				A[Y, X] = A[Z, X] = 0.0;
			}

			// force the y column vector to be orthogonal to x using y = y-(x.y)x
			ftmp = A[X, X] * A[X, Y] + A[Y, X] * A[Y, Y] + A[Z, X] * A[Z, Y];
			A[X, Y] -= ftmp * A[X, X];
			A[Y, Y] -= ftmp * A[Y, X];
			A[Z, Y] -= ftmp * A[Z, X];

			// normalize the y column vector
			ftmp = Math.Sqrt(A[X, Y] * A[X, Y] + A[Y, Y] * A[Y, Y] + A[Z, Y] * A[Z, Y]);
			if (ftmp > CORRUPTMATRIX)
			{
				// normalize the y column vector
				ftmp = 1.0F / ftmp;
				A[X, Y] *= ftmp;
				A[Y, Y] *= ftmp;
				A[Z, Y] *= ftmp;
			}
			else
			{
				// set y column vector to {0, 1, 0}
				A[Y, Y] = 1.0;
				A[X, Y] = A[Z, Y] = 0.0;
			}

			// finally set the z column vector to x vector cross y vector (automatically normalized)
			A[X, Z] = A[Y, X] * A[Z, Y] - A[Z, X] * A[Y, Y];
			A[Y, Z] = A[Z, X] * A[X, Y] - A[X, X] * A[Z, Y];
			A[Z, Z] = A[X, X] * A[Y, Y] - A[Y, X] * A[X, Y];
		}

	}
}