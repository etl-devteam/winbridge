﻿using System;
using System.Collections.Generic;

namespace WinBridge
{
	///--------------------------------------------------------------------------------------------
	/// Calcola i coefficienti necessari per correggere i dati acquisiti dal magnetometro
	/// dell'occhiale VisionAR. Viene utilizzata l'equazione C = (D -b) * A da cui si ottengono i
	/// dati calibrati C che si trovano sulla sfera di raggio expmfs.
	/// Il calcolo è equivalente alla funzione MagCal di MatLab e salvo salvo piccole differenze
	/// del tutto trascurabili produce lo stesso risultato
	///--------------------------------------------------------------------------------------------

	partial class MagCal
	{
		private const double FondoScalaGauss = 49.152;						// valore massimo positivo gestibile dal sensore in gauss
		private const double uT_per_lsb = FondoScalaGauss / 32768 * 100;    // 1 gauss = 100 microtesla, 32768 è per i 15 bit + segno del registro del sensore (si ottiene il peso del least-significant-bit in microtesla)
		private const double DEFAULTB = 50.0;                               // valore in uT del campo geomagnetico predefinito
		private const double ONETHIRD = 0.33333333;                         // un terzo
		private const double ONESIXTH = 0.166666667;                        // un sesto
		private const int MINMEASUREMENTS4CAL = 40;                         // numero minimo di campioni necessare per la calibrazione a 4 elementi
		private const int MINMEASUREMENTS7CAL = 100;                        // numero minimo di campioni necessare per la calibrazione a 7 elementi
		private const int MINMEASUREMENTS10CAL = 150;                       // numero minimo di campioni necessare per la calibrazione a 10 elementi
		private const double MINBFITUT = 22.0;                              // minimo valore di campo geomagnetico B (uT) per considerare valida la calibratione
		private const double MAXBFITUT = 67.0;                              // massimo valore di campo geomagnetico B (uT) per considerare valida la calibratione

		private List<short> xl = new List<short>();
		private List<short> yl = new List<short>();
		private List<short> zl = new List<short>();

		public void AddRawData(short x, short y, short z)
		{
			xl.Add(x);
			yl.Add(y);
			zl.Add(z);
		}

		public int SamplesCount
		{
			get { return xl.Count; }
		}

		// magnetic calibration & buffer structure
		private class TmpCalcVars
		{
			//public double[] V = new double[3];                      // current hard iron offset x, y, z, (uT)
			//public double[,] invW = new double[3, 3];               // current inverse soft iron matrix
			public double[] trV = new double[3];                    // trial value of hard iron offset z, y, z (uT)
			public double[,] trinvW = new double[3, 3];             // trial inverse soft iron matrix size
			public double trB;                                      // trial value of geomagnetic field magnitude in uT
			public double trFitErrorpc;                             // trial value of fit error %
			public double[,] A = new double[3, 3];                  // ellipsoid matrix A
			public double[,] invA = new double[3, 3];               // inverse of ellipsoid matrix A
			public double[,] matA = new double[10, 10];             // scratch 10x10 matrix used by calibration algorithms
			public double[,] matB = new double[10, 10];             // scratch 10x10 matrix used by calibration algorithms
			public double[] vecA = new double[10];                  // scratch 10x1 vector used by calibration algorithms
			public double[] vecB = new double[4];                   // scratch 4x1 vector used by calibration algorithms
			public short[,] RawData;								// uncalibrated magnetometer readings
		};

		public struct MagCalResult
		{
			public double[,] A;
			public double[] b;
			public double magB;

			// valori statistici non restituiti da MagCal di MatLab
			public double FitError;                                 // current fit error %
			public double FourBsq;                                  // current 4*B*B (uT^2)

		}

		public bool ComputeMagnetometerCalibrationCoefficients(out MagCalResult result)
		{
			TmpCalcVars tmp = new TmpCalcVars();
			result = new MagCalResult();

			if (xl.Count < MINMEASUREMENTS4CAL)
				return false;

			tmp.RawData = new short[xl.Count, 3];
			for (int n = 0; n < xl.Count; n++)
			{
				tmp.RawData[n, X] = xl[n];
				tmp.RawData[n, Y] = yl[n];
				tmp.RawData[n, Z] = zl[n];
			}

			// is enough data collected
			if (xl.Count < MINMEASUREMENTS7CAL)
			{
				fUpdateCalibration4INV(tmp);		// 4 element matrix inversion calibration
				if (tmp.trFitErrorpc < 12.0) tmp.trFitErrorpc = 12.0;
			}
			else if (xl.Count < MINMEASUREMENTS10CAL)
			{
				fUpdateCalibration7EIG(tmp);		// 7 element eigenpair calibration
				if (tmp.trFitErrorpc < 7.5) tmp.trFitErrorpc = 7.5;
			}
			else
				fUpdateCalibration10EIG(tmp);		// 10 element eigenpair calibration

			// the trial geomagnetic field must be in range (earth is 22uT to 67uT)
			if ((tmp.trB >= MINBFITUT) && (tmp.trB <= MAXBFITUT))
			{
				result.A = tmp.trinvW;              // accept the new calibration
				result.b = tmp.trV;
				result.magB = tmp.trB;

				result.FitError = tmp.trFitErrorpc;
				result.FourBsq = 4.0 * tmp.trB* tmp.trB;
				return true;						// calibration output is valid
			}
			return false;
		}

		// 4 element calibration using 4x4 matrix inverse
		void fUpdateCalibration4INV(TmpCalcVars tmp)
		{
			double fBp2;                 // fBp[X]^2+fBp[Y]^2+fBp[Z]^2
			double fSumBp4;              // sum of fBp2
			double fscaling;             // set to FUTPERCOUNT * FMATRIXSCALING
			double fE;                   // error function = r^T.r
			short[] iOffset = new short[3];         // offset to remove large DC hard iron bias in matrix
			int i, j, k;                // loop counters

			// compute fscaling to reduce multiplications later
			fscaling = uT_per_lsb / DEFAULTB;

			// the trial inverse soft iron matrix invW always equals
			// the identity matrix for 4 element calibration
			f3x3matrixAeqI(ref tmp.trinvW);

			// zero fSumBp4=Y^T.Y, vecB=X^T.Y (4x1) and on and above
			// diagonal elements of matA=X^T*X (4x4)
			fSumBp4 = 0.0;
			for (i = 0; i < 4; i++)
			{
				tmp.vecB[i] = 0.0;
				for (j = i; j < 4; j++)
					tmp.matA[i, j] = 0.0;
			}

			// the offsets are guaranteed to be set from the first element but to avoid compiler error
			iOffset[X] = iOffset[Y] = iOffset[Z] = 0;

			// use from MINEQUATIONS up to MAXEQUATIONS entries from magnetic buffer to compute matrices
			for (j = 0; j < xl.Count; j++)
			{
				// use first valid magnetic buffer entry as estimate (in counts) for offset
				if (j == 0)
				{
					for (k = 0; k < 3; k++)
						iOffset[k] = tmp.RawData[j, k];
				}

				// store scaled and offset fBp[XYZ] in vecA[0-2] and fBp[XYZ]^2 in vecA[3-5]
				for (k = 0; k < 3; k++)
				{
					tmp.vecA[k] = (tmp.RawData[j, k] - iOffset[k]) * fscaling;
					tmp.vecA[k + 3] = tmp.vecA[k] * tmp.vecA[k];
				}

				// calculate fBp2 = Bp[X]^2 + Bp[Y]^2 + Bp[Z]^2 (scaled uT^2)
				fBp2 = tmp.vecA[3] + tmp.vecA[4] + tmp.vecA[5];

				// accumulate fBp^4 over all measurements into fSumBp4=Y^T.Y
				fSumBp4 += fBp2 * fBp2;

				// now we have fBp2, accumulate vecB[0-2] = X^T.Y =sum(Bp2.Bp[XYZ])
				for (k = X; k <= Z; k++)
					tmp.vecB[k] += tmp.vecA[k] * fBp2;

				//accumulate vecB[3] = X^T.Y =sum(fBp2)
				tmp.vecB[3] += fBp2;

				// accumulate on and above-diagonal terms of matA = X^T.X ignoring matA[3,3]
				tmp.matA[0, 0] += tmp.vecA[X + 3];
				tmp.matA[0, 1] += tmp.vecA[X] * tmp.vecA[Y];
				tmp.matA[0, 2] += tmp.vecA[X] * tmp.vecA[Z];
				tmp.matA[0, 3] += tmp.vecA[X];
				tmp.matA[1, 1] += tmp.vecA[Y + 3];
				tmp.matA[1, 2] += tmp.vecA[Y] * tmp.vecA[Z];
				tmp.matA[1, 3] += tmp.vecA[Y];
				tmp.matA[2, 2] += tmp.vecA[Z + 3];
				tmp.matA[2, 3] += tmp.vecA[Z];
			}

			// set the last element of the measurement matrix to the number of buffer elements used
			tmp.matA[3, 3] = xl.Count;

			// use above diagonal elements of symmetric matA to set both matB and matA to X^T.X
			for (i = 0; i < 4; i++)
			{
				for (j = i; j < 4; j++)
					tmp.matB[i, j] = tmp.matB[j, i] = tmp.matA[j, i] = tmp.matA[i, j];
			}

			// working arrays for 4x4 matrix inversion
			/*double* pfRows[4];

			// calculate in situ inverse of matB = inv(X^T.X) (4x4) while matA still holds X^T.X
			for (i = 0; i < 4; i++)
			{
				pfRows[i] = tmp.matB[i];
			}*/
			fmatrixAeqInvA(ref tmp.matB, 4);

			// calculate vecA = solution beta (4x1) = inv(X^T.X).X^T.Y = matB * vecB
			for (i = 0; i < 4; i++)
			{
				tmp.vecA[i] = 0.0;
				for (k = 0; k < 4; k++)
					tmp.vecA[i] += tmp.matB[i, k] * tmp.vecB[k];
			}

			// calculate P = r^T.r = Y^T.Y - 2 * beta^T.(X^T.Y) + beta^T.(X^T.X).beta
			// = fSumBp4 - 2 * vecA^T.vecB + vecA^T.matA.vecA
			// first set P = Y^T.Y - 2 * beta^T.(X^T.Y) = SumBp4 - 2 * vecA^T.vecB
			fE = 0.0;
			for (i = 0; i < 4; i++)
			{
				fE += tmp.vecA[i] * tmp.vecB[i];
			}
			fE = fSumBp4 - 2.0 * fE;

			// set vecB = (X^T.X).beta = matA.vecA
			for (i = 0; i < 4; i++)
			{
				tmp.vecB[i] = 0.0;
				for (k = 0; k < 4; k++)
					tmp.vecB[i] += tmp.matA[i, k] * tmp.vecA[k];
			}

			// complete calculation of P by adding beta^T.(X^T.X).beta = vecA^T * vecB
			for (i = 0; i < 4; i++)
				fE += tmp.vecB[i] * tmp.vecA[i];

			// compute the hard iron vector (in uT but offset and scaled by FMATRIXSCALING)
			for (k = X; k <= Z; k++)
				tmp.trV[k] = 0.5 * tmp.vecA[k];

			// compute the scaled geomagnetic field strength B (in uT but scaled by FMATRIXSCALING)
			tmp.trB = Math.Sqrt(tmp.vecA[3] + tmp.trV[X] * tmp.trV[X] + tmp.trV[Y] * tmp.trV[Y] + tmp.trV[Z] * tmp.trV[Z]);

			// calculate the trial fit error (percent) normalized to number of measurements
			// and scaled geomagnetic field strength
			tmp.trFitErrorpc = Math.Sqrt(fE / xl.Count) * 100.0 / (2.0 * tmp.trB * tmp.trB);

			// correct the hard iron estimate for FMATRIXSCALING and the offsets applied (result in uT)
			for (k = X; k <= Z; k++)
				tmp.trV[k] = tmp.trV[k] * DEFAULTB + iOffset[k] * uT_per_lsb;

			// correct the geomagnetic field strength B to correct scaling (result in uT)
			tmp.trB *= DEFAULTB;
		}

		// 7 element calibration using direct eigen-decomposition
		void fUpdateCalibration7EIG(TmpCalcVars tmp)
		{
			double det;							// matrix determinant
			double fscaling;					// set to FUTPERCOUNT * FMATRIXSCALING
			double ftmp;						// scratch variable
			short[] iOffset = new short[3];     // offset to remove large DC hard iron bias
			int i, j, k, m, n;					// loop counters

			// compute fscaling to reduce multiplications later
			fscaling = uT_per_lsb / DEFAULTB;

			// the offsets are guaranteed to be set from the first element but to avoid compiler error
			iOffset[X] = iOffset[Y] = iOffset[Z] = 0;

			// zero the on and above diagonal elements of the 7x7 symmetric measurement matrix matA
			for (m = 0; m < 7; m++)
			{
				for (n = m; n < 7; n++)
					tmp.matA[m, n] = 0.0;
			}

			// place from MINEQUATIONS to MAXEQUATIONS entries into product matrix matA
			for (j = 0; j < xl.Count; j++)
			{
				// use first valid magnetic buffer entry as offset estimate (bit counts)
				if (j == 0)
				{
					for (k = X; k <= Z; k++)
						iOffset[k] = tmp.RawData[j, k];
				}

				// apply the offset and scaling and store in vecA
				for (k = X; k <= Z; k++)
				{
					tmp.vecA[k + 3] = (tmp.RawData[j, k] - iOffset[k]) * fscaling;
					tmp.vecA[k] = tmp.vecA[k + 3] * tmp.vecA[k + 3];
				}

				// accumulate the on-and above-diagonal terms of
				// tmp.matA=Sigma{vecA^T * vecA}
				// with the exception of matA[6,6] which will sum to the number
				// of measurements and remembering that vecA[6] equals 1.0
				// update the right hand column [6] of matA except for matA[6,6]
				for (m = 0; m < 6; m++)
					tmp.matA[m, 6] += tmp.vecA[m];

				// update the on and above diagonal terms except for right hand column 6
				for (m = 0; m < 6; m++)
				{
					for (n = m; n < 6; n++)
						tmp.matA[m, n] += tmp.vecA[m] * tmp.vecA[n];
				}
			}

			// finally set the last element matA[6,6] to the number of measurements
			tmp.matA[6, 6] = xl.Count;

			// copy the above diagonal elements of matA to below the diagonal
			for (m = 1; m < 7; m++)
			{
				for (n = 0; n < m; n++)
					tmp.matA[m, n] = tmp.matA[n, m];
			}

			// set tmpA7x1 to the unsorted eigenvalues and matB to the unsorted eigenvectors of matA
			eigencompute(ref tmp.matA, ref tmp.vecA, ref tmp.matB, 7);

			// find the smallest eigenvalue
			j = 0;
			for (i = 1; i < 7; i++)
			{
				if (tmp.vecA[i] < tmp.vecA[j])
					j = i;
			}

			// set ellipsoid matrix A to the solution vector with smallest eigenvalue,
			// compute its determinant and the hard iron offset (scaled and offset)
			f3x3matrixAeqScalar(ref tmp.A, 0.0);
			det = 1.0;
			for (k = X; k <= Z; k++)
			{
				tmp.A[k, k] = tmp.matB[k, j];
				det *= tmp.A[k, k];
				tmp.trV[k] = -0.5 * tmp.matB[k + 3, j] / tmp.A[k, k];
			}

			// negate A if it has negative determinant
			if (det < 0.0)
			{
				f3x3matrixAeqMinusA(ref tmp.A);
				tmp.matB[6, j] = -tmp.matB[6, j];
				det = -det;
			}

			// set ftmp to the square of the trial geomagnetic field strength B
			// (counts times FMATRIXSCALING)
			ftmp = -tmp.matB[6, j];
			for (k = X; k <= Z; k++)
				ftmp += tmp.A[k, k] * tmp.trV[k] * tmp.trV[k];

			// calculate the trial normalized fit error as a percentage
			tmp.trFitErrorpc = 50.0 * Math.Sqrt(Math.Abs(tmp.vecA[j]) / xl.Count) / Math.Abs(ftmp);

			// normalize the ellipsoid matrix A to unit determinant
			f3x3matrixAeqAxScalar(ref tmp.A, Math.Pow(det, -(ONETHIRD)));

			// convert the geomagnetic field strength B into uT for normalized
			// soft iron matrix A and normalize
			tmp.trB = Math.Sqrt(Math.Abs(ftmp)) * DEFAULTB * Math.Pow(det, -(ONESIXTH));

			// compute trial invW from the square root of A also with normalized
			// determinant and hard iron offset in uT
			f3x3matrixAeqI(ref tmp.trinvW);
			for (k = X; k <= Z; k++)
			{
				tmp.trinvW[k, k] = Math.Sqrt(Math.Abs(tmp.A[k, k]));
				tmp.trV[k] = tmp.trV[k] * DEFAULTB + iOffset[k] * uT_per_lsb;
			}
		}

		// 10 element calibration using direct eigen-decomposition
		void fUpdateCalibration10EIG(TmpCalcVars tmp)
		{
			double det;							// matrix determinant
			double fscaling;					// set to FUTPERCOUNT * FMATRIXSCALING
			double ftmp;						// scratch variable
			short[] iOffset = new short[3];     // offset to remove large DC hard iron bias in matrix
			int i, j, k, m, n;					// loop counters

			// compute fscaling to reduce multiplications later
			fscaling = uT_per_lsb / DEFAULTB;

			// the offsets are guaranteed to be set from the first element but to avoid compiler error
			iOffset[X] = iOffset[Y] = iOffset[Z] = 0;

			// zero the on and above diagonal elements of the 10x10 symmetric measurement matrix matA
			for (m = 0; m < 10; m++)
			{
				for (n = m; n < 10; n++)
					tmp.matA[m, n] = 0.0;
			}

			// sum between MINEQUATIONS to MAXEQUATIONS entries into the 10x10 product matrix matA
			for (j = 0; j < xl.Count; j++)
			{

				// use first valid magnetic buffer entry as estimate for offset
				// to help solution (bit counts)
				if (j == 0)
				{
					for (k = X; k <= Z; k++)
						iOffset[k] = tmp.RawData[j, k];
				}

				// apply the fixed offset and scaling and enter into vecA[6-8]
				for (k = X; k <= Z; k++)
					tmp.vecA[k + 6] = (tmp.RawData[j, k] - iOffset[k]) * fscaling;

				// compute measurement vector elements vecA[0-5] from vecA[6-8]
				tmp.vecA[0] = tmp.vecA[6] * tmp.vecA[6];
				tmp.vecA[1] = 2.0 * tmp.vecA[6] * tmp.vecA[7];
				tmp.vecA[2] = 2.0 * tmp.vecA[6] * tmp.vecA[8];
				tmp.vecA[3] = tmp.vecA[7] * tmp.vecA[7];
				tmp.vecA[4] = 2.0 * tmp.vecA[7] * tmp.vecA[8];
				tmp.vecA[5] = tmp.vecA[8] * tmp.vecA[8];

				// accumulate the on-and above-diagonal terms of matA=Sigma{vecA^T * vecA}
				// with the exception of matA[9,9] which equals the number of measurements
				// update the right hand column [9] of matA[0-8,9] ignoring matA[9,9]
				for (m = 0; m < 9; m++)
					tmp.matA[m, 9] += tmp.vecA[m];

				// update the on and above diagonal terms of matA ignoring right hand column 9
				for (m = 0; m < 9; m++)
				{
					for (n = m; n < 9; n++)
						tmp.matA[m, n] += tmp.vecA[m] * tmp.vecA[n];
				}
			}

			// set the last element matA[9,9] to the number of measurements
			tmp.matA[9, 9] = xl.Count;

			// copy the above diagonal elements of symmetric product matrix matA to below the diagonal
			for (m = 1; m < 10; m++)
			{
				for (n = 0; n < m; n++)
					tmp.matA[m, n] = tmp.matA[n, m];
			}

			// set tmp.vecA to the unsorted eigenvalues and matB to the unsorted
			// normalized eigenvectors of matA
			eigencompute(ref tmp.matA, ref tmp.vecA, ref tmp.matB, 10);

			// set ellipsoid matrix A from elements of the solution vector column j with
			// smallest eigenvalue
			j = 0;
			for (i = 1; i < 10; i++)
			{
				if (tmp.vecA[i] < tmp.vecA[j])
					j = i;
			}
			tmp.A[0, 0] = tmp.matB[0, j];
			tmp.A[0, 1] = tmp.A[1, 0] = tmp.matB[1, j];
			tmp.A[0, 2] = tmp.A[2, 0] = tmp.matB[2, j];
			tmp.A[1, 1] = tmp.matB[3, j];
			tmp.A[1, 2] = tmp.A[2, 1] = tmp.matB[4, j];
			tmp.A[2, 2] = tmp.matB[5, j];

			// negate entire solution if A has negative determinant
			det = f3x3matrixDetA(ref tmp.A);
			if (det < 0.0)
			{
				f3x3matrixAeqMinusA(ref tmp.A);
				tmp.matB[6, j] = -tmp.matB[6, j];
				tmp.matB[7, j] = -tmp.matB[7, j];
				tmp.matB[8, j] = -tmp.matB[8, j];
				tmp.matB[9, j] = -tmp.matB[9, j];
				det = -det;
			}

			// compute the inverse of the ellipsoid matrix
			f3x3matrixAeqInvSymB(ref tmp.invA, ref tmp.A);

			// compute the trial hard iron vector in offset bit counts times FMATRIXSCALING
			for (k = X; k <= Z; k++)
			{
				tmp.trV[k] = 0.0;
				for (m = X; m <= Z; m++)
					tmp.trV[k] += tmp.invA[k, m] * tmp.matB[m + 6, j];
				tmp.trV[k] *= -0.5;
			}

			// compute the trial geomagnetic field strength B in bit counts times FMATRIXSCALING
			tmp.trB = Math.Sqrt(Math.Abs(tmp.A[0, 0] * tmp.trV[X] * tmp.trV[X] +
					2.0 * tmp.A[0, 1] * tmp.trV[X] * tmp.trV[Y] +
					2.0 * tmp.A[0, 2] * tmp.trV[X] * tmp.trV[Z] +
					tmp.A[1, 1] * tmp.trV[Y] * tmp.trV[Y] +
					2.0 * tmp.A[1, 2] * tmp.trV[Y] * tmp.trV[Z] +
					tmp.A[2, 2] * tmp.trV[Z] * tmp.trV[Z] - tmp.matB[9, j]));

			// calculate the trial normalized fit error as a percentage
			tmp.trFitErrorpc = 50.0 * Math.Sqrt(Math.Abs(tmp.vecA[j]) / xl.Count) / (tmp.trB * tmp.trB);

			// correct for the measurement matrix offset and scaling and
			// get the computed hard iron offset in uT
			for (k = X; k <= Z; k++)
				tmp.trV[k] = tmp.trV[k] * DEFAULTB + iOffset[k] * uT_per_lsb;

			// convert the trial geomagnetic field strength B into uT for
			// un-normalized soft iron matrix A
			tmp.trB *= DEFAULTB;

			// normalize the ellipsoid matrix A to unit determinant and
			// correct B by root of this multiplicative factor
			f3x3matrixAeqAxScalar(ref tmp.A, Math.Pow(det, -(ONETHIRD)));
			tmp.trB *= Math.Pow(det, -(ONESIXTH));

			// compute trial invW from the square root of fA (both with normalized determinant)
			// set vecA to the unsorted eigenvalues and matB to the unsorted eigenvectors of matA
			// where matA holds the 3x3 matrix fA in its top left elements
			for (i = 0; i < 3; i++)
			{
				for (j = 0; j < 3; j++)
					tmp.matA[i, j] = tmp.A[i, j];
			}
			eigencompute(ref tmp.matA, ref tmp.vecA, ref tmp.matB, 3);

			// set tmp.matB to be eigenvectors . diag(sqrt(sqrt(eigenvalues))) =
			//   matB . diag(sqrt(sqrt(vecA))
			for (j = 0; j < 3; j++)
			{ // loop over columns j
				ftmp = Math.Sqrt(Math.Sqrt(Math.Abs(tmp.vecA[j])));
				for (i = 0; i < 3; i++)
					tmp.matB[i, j] *= ftmp;      // loop over rows i
			}

			// set trinvW to eigenvectors * diag(sqrt(eigenvalues)) * eigenvectors^T =
			//   matB * matB^T = sqrt(fA) (guaranteed symmetric)
			// loop over rows
			for (i = 0; i < 3; i++)
			{
				// loop over on and above diagonal columns
				for (j = i; j < 3; j++)
				{
					tmp.trinvW[i, j] = 0.0;
					// accumulate the matrix product
					for (k = 0; k < 3; k++)
						tmp.trinvW[i, j] += tmp.matB[i, k] * tmp.matB[j, k];

					// copy to below diagonal element
					tmp.trinvW[j, i] = tmp.trinvW[i, j];
				}
			}

		}

	}
}