﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinBridge
{
    public partial class frmImages : Form
    {
        public frmImages()
        {
            InitializeComponent();
        }

        private void frmImages_Load(object sender, EventArgs e)
        {
            string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string s = Path.Combine(path, "Immagini", "Mappatura.xml");
            if (File.Exists(s) == false)
                return;
            try
            {
                DataSet ds = new DataSet();
                ds.ReadXml(s);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    DataGridViewRow dgvr = new DataGridViewRow();

                    DataGridViewTextBoxCell tCell = new DataGridViewTextBoxCell();
                    tCell.Value = dr.ItemArray[0];
                    dgvr.Cells.Add(tCell);
                    DataGridViewCheckBoxCell cbCell = new DataGridViewCheckBoxCell();
                    cbCell.Value = dr.ItemArray[1];
                    dgvr.Cells.Add(cbCell);
                    tCell = new DataGridViewTextBoxCell();
                    tCell.Value = dr.ItemArray[2];
                    dgvr.Cells.Add(tCell);
                    dataGrid.Rows.Add(dgvr);
                }
                ds.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Si è verificato un errore nella lettura del file Mappatura.xml\r\n" + ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            if (Directory.Exists(Path.Combine(path, "Immagini")) == false)
            {
                try
                {
                    Directory.CreateDirectory(Path.Combine(path, "Immagini"));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Impossibile creare la cartella delle immagini\r\n" + ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            string s = Path.Combine(path, "Immagini", "Mappatura.xml");
            try
            {
                DataTable dt = new DataTable();
                dt.TableName = "Mappatura";
                foreach (DataGridViewColumn column in dataGrid.Columns)
                    dt.Columns.Add();
                object[] cellValues = new object[dataGrid.Columns.Count];
                foreach (DataGridViewRow row in dataGrid.Rows)
                {
                    if ((string.IsNullOrEmpty(Convert.ToString(row.Cells[0].Value)) == false) || (string.IsNullOrEmpty(Convert.ToString(row.Cells[2].Value)) == false))
                    {
                        for (int i = 0; i < row.Cells.Count; i++)
                            cellValues[i] = row.Cells[i].Value;
                        dt.Rows.Add(cellValues);
                    }
                }
                dt.WriteXml(s);
                dt.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossibile salvare il file " + s + "\r\n" + ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Close();
        }

    }
}