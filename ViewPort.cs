﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinBridge
{
	class ViewPort
	{
		public const double VIEWPORT_WIDTH = 21.7;
		public const double VIEWPORT_HEIGHT = 10.9;
		public const int VISIBLE_LEFT = 0;          // effective visible area is smaller than expected, the first visible pixel is located at x = 0 and y = 1
		public const int VISIBLE_TOP = 1;
		public const int VISIBLE_WIDTH = (VisionAR.PIXEL_WIDTH - 6);
		public const int VISIBLE_HEIGHT = (VisionAR.PIXEL_HEIGHT - 4);
		public const int FB_SIZE_BYTES = (VisionAR.PIXEL_WIDTH * VisionAR.PIXEL_HEIGHT);
		public static byte[] pixels = new byte[VisionAR.PIXEL_HEIGHT * VisionAR.PIXEL_WIDTH];
		private static POINT3D[] ViewportQ = new POINT3D[4] { new POINT3D(), new POINT3D(), new POINT3D(), new POINT3D() };
		private static int image_index;

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void MakeViewPort()
		{
			double hw = VIEWPORT_WIDTH / 2 / 1000;      // half viewport width in meters
			double hh = VIEWPORT_HEIGHT / 2 / 1000;     // half viewport height in meters

			// The viewport is a origin-centered quadrilateral 'q' with vertices sorted in CCW order from from A to D, as follows:
			//
			//   D --------- C
			//   |           |
			//   |           |
			//   A --------- B
			//

			ViewportQ[0].x = -hw;
			ViewportQ[0].y = -hh;
			ViewportQ[0].z = 0;

			ViewportQ[1].x = hw;
			ViewportQ[1].y = -hh;
			ViewportQ[1].z = 0;

			ViewportQ[2].x = hw;
			ViewportQ[2].y = hh;
			ViewportQ[2].z = 0;

			ViewportQ[3].x = -hw;
			ViewportQ[3].y = hh;
			ViewportQ[3].z = 0;

			// Convert degrees in radiands and adjust signs of the rotations
			double yaw = Program.Settings.ViewportYaw * Math.PI / 180.0;
			double pitch = Program.Settings.ViewportPitch * Math.PI / 180.0;
			double roll = -Program.Settings.ViewportRoll * Math.PI / 180.0;
			ROTATION_MATRIX matrix = Math3D.InitRotationMatrix(roll, pitch, yaw);

			for (int i = 0; i < 4; i++)
			{
				Math3D.MatrixRotate(ref ViewportQ[i], ref matrix);

				// Offset along the XYZ axes
				// The previous rotation was performed around the perpendicular axes that are coincident with the origin O of the Euclidean space.
				// Now, the center of the area is moved to the required position. Also the origin is moved if required, as it is the focal point, but only on the XY axes.
				// All XYZ settings are in millimeters and are converted in meters dividing by 1000; 
				ViewportQ[i].x += (Program.Settings.ViewportCenterX + Program.Settings.ViewportOffsetX) / 1000;
				ViewportQ[i].y += (Program.Settings.ViewportCenterY + Program.Settings.ViewportOffsetY) / 1000;
				ViewportQ[i].z += (Program.Settings.ViewportCenterZ) / 1000;
			}
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void SetPixel(int x, int y, byte color)
		{
			if ((x >= 0) && (x < VisionAR.PIXEL_WIDTH) && (y >= 0) && (y < VisionAR.PIXEL_HEIGHT))
				pixels[y * VisionAR.PIXEL_WIDTH + x] = color;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static byte GetPixel(int x, int y)
		{
			if ((x >= 0) && (x < VisionAR.PIXEL_WIDTH) && (y >= 0) && (y < VisionAR.PIXEL_HEIGHT))
				return pixels[y * VisionAR.PIXEL_WIDTH + x];
			return 0;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void DrawString(int x, int y, string txt, FontBase fnt)
		{
			foreach (char c in txt)
			{
				byte ch = (byte)c;
				if (ch < 32)
					ch = 0;
				else
					ch -= 32;

				int p = fnt.ofts[ch];
				int width = fnt.data[p++];

				for (int v = 0; v < fnt.height; v++)
				{
					for (int h = 0; h < width; h++)
						SetPixel(x + h, y + v, fnt.data[p++]);
				}
				x += width;
			}
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void DrawStringInverse(int x, int y, string txt, FontBase fnt)
		{
			foreach (char c in txt)
			{
				byte ch = Convert.ToByte(c);
				if (ch < 32)
					ch = 0;
				else
					ch -= 32;

				int p = fnt.ofts[ch];
				int width = fnt.data[p++];

				for (byte v = 0; v < fnt.height; v++)
				{
					for (byte h = 0; h < width; h++)
						SetPixel(x + h, y + v, (byte)(fnt.data[p++] ^ 0xFF));
				}
				x += width;
			}
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void DrawImage(int x, int y, byte[] img)
		{
			int width = img[0];
			int height = img[1];
			int p = 2;

			x -= width / 2;     // center image
			y -= height / 2;

			for (byte v = 0; v < height; v++)
			{
				for (byte h = 0; h < width; h++)
				{
					// Each pixel of the images are represented by alpha channel only and it simple to alpha-blend 
					SetPixel(x + h, y + v, (byte)(((255 - img[p]) * GetPixel(x + h, y + v) / 255) + img[p]));
					p++;
				}
			}
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void DrawEllipse(int w, int h, int ofts, byte color)
		{
			int centerX = VisionAR.PIXEL_WIDTH / 2;
			int centerY = VisionAR.PIXEL_HEIGHT - ofts - (h / 2);

			int radiusX = w / 2;
			int radiusY = h / 2;
			double rel = Math.Sqrt(radiusX * radiusX + radiusY * radiusY) / radiusY;
			double distance;

			int xs;
			double yt;
			byte green;
			byte bgColor;

			for (xs = -radiusX; xs <= radiusX; xs++)
			{
				yt = radiusY * Math.Sqrt(1 - xs * xs / (double)(radiusX * radiusX));
				distance = yt - (int)yt;

				bgColor = GetPixel(centerX + xs, centerY + (int)yt);
				green = (byte)((byte)(distance * bgColor) + (byte)((1.0 - distance) * color));
				SetPixel(centerX + xs, centerY + (int)yt, green);

				bgColor = GetPixel(centerX + xs, centerY + (int)yt + 1);
				green = (byte)((byte)((1.0 - distance) * bgColor) + (byte)(distance * color));
				SetPixel(centerX + xs, centerY + (int)yt + 1, green);

				bgColor = GetPixel(centerX - xs, centerY - (int)yt);
				green = (byte)((byte)(distance * bgColor) + (byte)((1.0 - distance) * color));
				SetPixel(centerX - xs, centerY - (int)yt, green);

				bgColor = GetPixel(centerX - xs, centerY - (int)yt - 1);
				green = (byte)((byte)((1.0 - distance) * bgColor) + (byte)(distance * color));
				SetPixel(centerX - xs, centerY - (int)yt - 1, green);
			}

			int ys;
			double xt;
			for (ys = -(int)((float)radiusY / rel); ys <= (int)((double)radiusY / rel); ys++)
			{
				xt = (float)(radiusX * Math.Sqrt(1 - ys * ys / (double)(radiusY * radiusY)));
				distance = (double)(xt - (int)xt);

				bgColor = GetPixel(centerX + (int)xt, centerY + ys);
				green = (byte)((byte)(distance * bgColor) + (byte)((1.0 - distance) * color));
				SetPixel(centerX + (int)xt, centerY + ys, green);

				bgColor = GetPixel(centerX + (int)xt + 1, centerY + ys);
				green = (byte)((byte)((1.0 - distance) * bgColor) + (byte)(distance * color));
				SetPixel(centerX + (int)xt + 1, centerY + ys, green);

				bgColor = GetPixel(centerX - (int)xt, centerY - ys);
				green = (byte)((byte)(distance * bgColor) + (byte)((1.0 - distance) * color));
				SetPixel(centerX - (int)xt, centerY - ys, green);

				bgColor = GetPixel(centerX - (int)xt - 1, centerY - ys);
				green = (byte)((byte)((1.0 - distance) * bgColor) + (byte)(distance * color));
				SetPixel(centerX - (int)xt - 1, centerY - ys, green);
			}
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void DrawLine(int X0, int Y0, int X1, int Y1, byte color)
		{
			if (Y0 > Y1)            /// make sure the line runs top to bottom
			{
				int Temp = Y0;
				Y0 = Y1;
				Y1 = Temp;
				Temp = X0;
				X0 = X1;
				X1 = Temp;
			}

			// Draw the initial pixel, which is always exactly intersected by the line and so needs no weighting
			SetPixel(X0, Y0, color);

			int XDir = 1;
			int DeltaX = X1 - X0;
			if (DeltaX < 0)
			{
				XDir = -1;
				DeltaX = 0 - DeltaX;        // make DeltaX positive
			}

			// Special-case horizontal, vertical, and 45-degree diagonal lines, which require no weighting because they go right through the center of every pixel
			int DeltaY = Y1 - Y0;
			if (DeltaY == 0)        // horizontal line
			{
				while (DeltaX-- > 0)
				{
					X0 += XDir;
					SetPixel(X0, Y0, color);
				}
				return;
			}

			if (DeltaX == 0)        // vertical line
			{
				do
				{
					SetPixel(X0, ++Y0, color);
				} while (--DeltaY > 0);
				return;
			}

			if (DeltaX == DeltaY)
			{
				do                  // 45-degree diagonal line
				{
					X0 += XDir;
					Y0++;
					SetPixel(X0, Y0, color);
				} while (--DeltaY != 0);
				return;
			}

			// Line is not horizontal, vertical, or 45-degree diagonal line
			ushort ErrorAdj;
			ushort ErrorAccTemp;
			ushort weighting;
			ushort ErrorAcc = 0;        // initialize the line error accumulator to 0

			// is this an X-major or Y-major line?
			if (DeltaY > DeltaX)
			{
				// Y-major line
				// Calculate 16-bit fixed-point fractional part of a pixel that X advances each time Y advances 1 pixel
				// Result is truncated so that we won't overrun the endpoint along the X axis
				ErrorAdj = (ushort)(((uint)DeltaX << 16) / DeltaY);

				// draw all pixels other than the first and last
				while (--DeltaY > 0)
				{
					ErrorAccTemp = ErrorAcc;        // remember currrent accumulated error
					ErrorAcc += ErrorAdj;           // calculate error for next pixel
					if (ErrorAcc <= ErrorAccTemp)
						X0 += XDir;                 // The error accumulator turned over, so advance the X coord
					Y0++;                           // Y-major, so always advance Y

					// the IntensityBits most significant bits of ErrorAcc give us the intensity weighting for this pixel, and the complement of the weighting for the paired pixel
					weighting = (ushort)(ErrorAcc >> 8);

					byte bground = GetPixel(X0, Y0);
					byte gr;
					if (bground > color)
						gr = ((byte)((ushort)weighting * (bground - color) / 255 + color));
					else
						gr = ((byte)((ushort)(weighting ^ 0xFF) * (color - bground) / 255 + bground));
					SetPixel(X0, Y0, gr);

					bground = GetPixel(X0 + XDir, Y0);
					if (bground > color)
						gr = ((byte)((ushort)weighting * (bground - color) / 255 + color));
					else
						gr = ((byte)((ushort)weighting * (color - bground) / 255 + bground));
					SetPixel(X0 + XDir, Y0, gr);
				}

				// Draw the final pixel, which is always exactly intersected by the line and so needs no weighting
				SetPixel(X1, Y1, color);
				return;
			}

			// -major line
			// Calculate 16-bit fixed-point fractional part of a pixel that Y advances each time X advances 1 pixel
			// Result is truncated so that we won't overrun the endpoint along the X axis
			ErrorAdj = (ushort)(((uint)DeltaY << 16) / DeltaX);
			/* Draw all pixels other than the first and last */
			while (--DeltaX > 0)
			{
				ErrorAccTemp = ErrorAcc;        // remember currrent accumulated error
				ErrorAcc += ErrorAdj;           // calculate error for next pixel
				if (ErrorAcc <= ErrorAccTemp)
					Y0++;                       // the error accumulator turned over, so advance the Y coord
				X0 += XDir;                     // X-major, so always advance X

				// the IntensityBits most significant bits of ErrorAcc give us the intensity weighting for this pixel, and the complement of the weighting for the paired pixel
				weighting = (ushort)(ErrorAcc >> 8);

				byte bground = GetPixel(X0, Y0);
				byte gr;
				if (bground > color)
					gr = ((byte)((ushort)weighting * (bground - color) / 255 + color));
				else
					gr = ((byte)((ushort)(weighting ^ 0xFF) * (color - bground) / 255 + bground));
				SetPixel(X0, Y0, gr);

				bground = GetPixel(X0, Y0 + 1);
				if (bground > color)
					gr = ((byte)((ushort)(weighting ^ 0xFF) * (bground - color) / 255 + color));
				else
					gr = ((byte)((ushort)weighting * (color - bground) / 255 + bground));
				SetPixel(X0, Y0 + 1, gr);
			}

			// Draw the final pixel, which is always exactly intersected by the line and so needs no weighting
			SetPixel(X1, Y1, color);
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static POINT2D NormalizedPointOnEllipse(int leftCorner, int topCorner, int angle)
		{
			POINT2D p = new POINT2D();

			double eHalfW = (1 + Program.Settings.EllipseWidth) / 2.0;
			double eHalfH = (1 + Program.Settings.EllipseHeight) / 2.0;

			p.x = (int)(leftCorner + eHalfW + eHalfW * Math.Cos((angle - 90) * Math.PI / 180));
			p.y = (int)(topCorner + eHalfH + eHalfH * Math.Sin((angle - 90) * Math.PI / 180));
			return p;
		}

		//--------------------------------------------------------------------------------------
		// Test only, not used
		//--------------------------------------------------------------------------------------
		public static void DrawRays()
		{
			short x, y, x2, y2;

			for (float theta = 0; theta < 360; theta += 10)
			{
				x = (short)(100.0 * Math.Cos(theta * 3.14 / 180.0) + VisionAR.PIXEL_WIDTH / 2);
				y = (short)(-100.0 * Math.Sin(theta * 3.14 / 180.0) + VisionAR.PIXEL_HEIGHT / 2);

				x2 = (short)(20.0 * Math.Cos(theta * 3.14 / 180.0) + VisionAR.PIXEL_WIDTH / 2);
				y2 = (short)(-20.0 * Math.Sin(theta * 3.14 / 180.0) + VisionAR.PIXEL_HEIGHT / 2);

				DrawLine(x, y, x2, y2, 255);
			}
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void DrawViewfinder()
		{
			int cx = VisionAR.PIXEL_WIDTH / 2;
			int cy = VisionAR.PIXEL_HEIGHT / 2;
			const int sz = 12;
			const int sp = 3;

			DrawLine(cx - sz, cy - sz, cx - sp, cy - sz, 255);  // top-left --
			DrawLine(cx - sz, cy - sz, cx - sz, cy - sp, 255);  // top-left |

			DrawLine(cx + sp, cy - sz, cx + sz, cy - sz, 255);  // top right --
			DrawLine(cx + sz, cy - sz, cx + sz, cy - sp, 255);  // top right |

			DrawLine(cx + sp, cy + sz, cx + sz, cy + sz, 255);  // bottom right --
			DrawLine(cx + sz, cy + sp, cx + sz, cy + sz, 255);  // bottom right |

			DrawLine(cx - sp, cy + sz, cx - sz, cy + sz, 255);  // bottom left --
			DrawLine(cx - sz, cy + sz, cx - sz, cy + sp, 255);  // bottom left |
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void DrawTime(int x, int y)
		{
			DrawString(x, y, DateTime.UtcNow.ToString("HH:mm:ss"), Fonts.Arial16);
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static int strWidth(string txt, FontBase fnt)
		{
			int width = 0;
			foreach (char c in txt)
			{
				byte ch = Convert.ToByte(c);
				if (ch < 32)
					ch = 0;
				else
					ch -= 32;

				int p = fnt.ofts[ch];
				width += fnt.data[p++];
			}

			return width;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void draw_event(bool worn, bool lean, bool walk, bool noise, bool cal)
		{
			string s = "";

			s = "worn  " + ((worn) ? "[X]" : "[ ]");
			DrawString(0, 1, s, Fonts.Arial16);

			s = "lean  " + ((lean) ? "[X]" : "[ ]");
			DrawString(0, 1 + 16, s, Fonts.Arial16);

			s = "walk  " + ((walk) ? "[X]" : "[ ]");
			DrawString(0, 1 + 16 + 16, s, Fonts.Arial16);

			s = "noise " + ((noise) ? "[X]" : "[ ]");
			DrawString(0, 1 + 16 + 16 + 16, s, Fonts.Arial16);

			s = "cal   " + ((cal) ? "[X]" : "[ ]");
			DrawString(0, 1 + 16 + 16 + 16 + 16, s, Fonts.Arial16);
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void draw_info(ref POINT wp, string rmks)
		{
			string s = "";

			if ((wp.ce < 9999999.0) && (wp.hae < 9999999.0) && (wp.lat < 9999999.0) && (wp.le < 9999999.0) && (wp.lon < 9999999.0))
			{
				s = string.Format("{0:N5}", wp.lat).Replace(',', '.');
				DrawString(0, 125 - 13 - 13 - 13 - 13, s, Fonts.Arial16);

				s = string.Format("{0:N5}", wp.lon).Replace(',', '.');
				DrawString(0, 125 - 13 - 13 - 13, s, Fonts.Arial16);

				s = string.Format("CE {0:N0}m", wp.ce).Replace(',', '.');
				DrawString(0, 125 - 13 - 13, s, Fonts.Arial16);

				s = string.Format("{0:N5}m", wp.hae).Replace(',', '.');
				DrawString(0, 125 - 13, s, Fonts.Arial16);

				s = string.Format("LE {0:N0}m", wp.le).Replace(',', '.');
				DrawString(0, 125, s, Fonts.Arial16);
			}

			if (string.IsNullOrWhiteSpace(rmks) == false)
			{
				rmks = " " + rmks + " ";
				int w = strWidth(rmks, Fonts.Arial20);
				int x = (VisionAR.PIXEL_WIDTH - (w + 10)) / 2;
				DrawStringInverse(x, 1, rmks, Fonts.Arial20);
			}
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static POINT2D GetPixelXY(ref POINT3D rayPoint)
		{
			POINT2D pixel = new POINT2D();

			double dX = Math3D.ShortestDistance(ref ViewportQ[3], ref ViewportQ[0], ref rayPoint) * 1000;
			double dY = Math3D.ShortestDistance(ref ViewportQ[1], ref ViewportQ[0], ref rayPoint) * 1000;

			pixel.x = (int)(VisionAR.PIXEL_WIDTH * dX / VIEWPORT_WIDTH);
			pixel.y = (int)(VisionAR.PIXEL_HEIGHT * dY / VIEWPORT_HEIGHT);

			return pixel;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void DrawDistance(int x, int y, int angle, int meters)
		{

			string s = meters.ToString();
			int sz = s.Length * 7;
			DrawString(x - 9 - sz, y - 9 - 12, s, Fonts.Arial16);
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void UpdateField(double eahrs_x, double eahrs_y, double eahrs_z)
		{
			POINT3D origin = new POINT3D();
			POINT3D target = new POINT3D();
			POINT2D pixel;
			POINT wp;
			string rmks;
			string s;

			int Azimuth = (int)Math.Round(eahrs_z);
			double eahrs_x_rad = eahrs_x * Math.PI / 180.0;
			double eahrs_y_rad = eahrs_y * Math.PI / 180.0;
			double eahrs_z_rad = eahrs_z * Math.PI / 180.0;

			for (int i = 0; i < (VisionAR.PIXEL_HEIGHT * VisionAR.PIXEL_WIDTH); i++)
				pixels[i] = 0x00;

			// draw watcher point info
			wp = Markers.GetWatcherPoint();
			rmks = Markers.GetRemarks();
			draw_event(HeadTracking.getEvent(HeadTracking.EventType.WornEvent),
						HeadTracking.getEvent(HeadTracking.EventType.LeaningEvent),
						HeadTracking.getEvent(HeadTracking.EventType.WalkingEvent),
						HeadTracking.getEvent(HeadTracking.EventType.MagNoiseEvent),
						HeadTracking.getEvent(HeadTracking.EventType.CalRequiredEvent));

			origin.x = Program.Settings.ViewportOffsetX / 1000;     // observer focal point in meters (settings are in mm)
			origin.y = Program.Settings.ViewportOffsetY / 1000;
			origin.z = 0;

			int ellipse_left = (int)((VisionAR.PIXEL_WIDTH - Program.Settings.EllipseWidth) / 2);
			int ellipse_top = (int)(VisionAR.PIXEL_HEIGHT - Program.Settings.EllipseOffset - Program.Settings.EllipseHeight);

			ENTITY[] entities = Markers.GetEntities(wp, eahrs_y_rad, eahrs_x_rad, eahrs_z_rad);
			image_index = 0;
			foreach (ENTITY e in entities)
			{
				POINT3D p3d = e.point;
				if (Math3D.IntersectLineQuad(ref origin, ref p3d, ref ViewportQ[0], ref ViewportQ[1], ref ViewportQ[2], ref ViewportQ[3], ref target))
				{
					pixel = GetPixelXY(ref target);
					DrawImage(pixel.x, pixel.y, Images.Get(e.type));
				}
				pixel = NormalizedPointOnEllipse(ellipse_left, ellipse_top, e.azimuth + Azimuth);
				DrawImage(pixel.x, pixel.y, Images.Get(e.type));

				DrawDistance(pixel.x, pixel.y, e.azimuth + Azimuth, e.distance);

				image_index++;
				if (image_index > 4)
					image_index = 0;
			}

			// draw compass
			DrawEllipse((int)Program.Settings.EllipseWidth, (int)Program.Settings.EllipseHeight, (int)Program.Settings.EllipseOffset, 255);

			DrawLine(VisionAR.PIXEL_WIDTH / 2, ellipse_top - 4, VisionAR.PIXEL_WIDTH / 2, ellipse_top + 9, 255);            // draw the centered vertical line

			POINT2D pt = NormalizedPointOnEllipse(ellipse_left, ellipse_top, 344);
			DrawLine(pt.x, pt.y, pt.x - 3, pt.y - 5, 255);                              // draw the left FoV limit line
			pt = NormalizedPointOnEllipse(ellipse_left, ellipse_top, 19);
			DrawLine(pt.x, pt.y, pt.x + 3, pt.y - 5, 255);                              // draw the right FoV limit line

			s = string.Format("{0}\xB0", Azimuth);
			DrawString(370, 118, s, Fonts.Arial16);

			DrawViewfinder();
			DrawTime(VisionAR.PIXEL_WIDTH - 55, 1);

			//DrawRays();
			//pt = NormalizedPointOnEllipse(ellipse_left, ellipse_top, Azimuth);
			//DrawImage(pt.x, pt.y, images[image_index]);
			//pt = NormalizedPointOnEllipse(left, top, angle3);
			//DrawImage(pt.x, pt.y, Properties.Resources.TagSQ);
			//pt = NormalizedPointOnEllipse(left, top, angle2);
			//DrawImage(pt.x, pt.y - 3, Properties.Resources.Tag1);
		}
	}
}