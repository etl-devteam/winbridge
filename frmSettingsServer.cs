﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinBridge
{
    public partial class frmSettingsServer : Form
    {

        public frmSettingsServer()
        {
            InitializeComponent();
            IPAddress ipa = new IPAddress(Properties.Settings.Default.ServerIPAddr);
            nudIP0.Value = ipa.GetAddressBytes()[0];
            nudIP1.Value = ipa.GetAddressBytes()[1];
            nudIP2.Value = ipa.GetAddressBytes()[2];
            nudIP3.Value = ipa.GetAddressBytes()[3];

            nudPort.Value = Properties.Settings.Default.ServerPort;
            rbUDP.Checked = Properties.Settings.Default.ServerProtocolType != 1;
            rbTCP.Checked = Properties.Settings.Default.ServerProtocolType == 1;
            cbConnectionActive.Checked = Properties.Settings.Default.ServerConnectionActive;
            cbLog.Checked = Properties.Settings.Default.ServerLog;
        }

        private void frmSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.ServerIPAddr = (uint)nudIP3.Value << 24 | (uint)nudIP2.Value << 16 | (uint)nudIP1.Value << 8 | (uint)nudIP0.Value;
            Properties.Settings.Default.ServerPort = (int)nudPort.Value;
            Properties.Settings.Default.ServerProtocolType = (rbTCP.Checked == true) ? 1 : 0;
            Properties.Settings.Default.ServerConnectionActive = cbConnectionActive.Checked;
            Properties.Settings.Default.ServerLog = cbLog.Checked;
            Properties.Settings.Default.Save();
        }

    }
}
