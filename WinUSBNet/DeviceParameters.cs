﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace libusb
{
    /*public class DeviceParameters
    {
        public int Vid;
        public int Pid;
        public int MI;
        public int PipeId;
        public int AltInterfaceId;

        public DeviceParameters(int vid, int pid, int altInterfaceId, int pipeId)
        {
            MI = -1;
            Vid = vid;
            Pid = pid;
            PipeId = pipeId;
            AltInterfaceId = altInterfaceId;
        }

        public bool ConfigureDevice(out WINUSB_PIPE_INFORMATION pipeInfo, out UsbK usb, out USB_INTERFACE_DESCRIPTOR interfaceDescriptor)
        {
            bool success;
            //Console.WriteLine("Finding usb device by VID/PID.. ({0:X4}h / {1:X4}h)", Vid, Pid);

            // Use a pattern match to include only matching devices.
            // NOTE: You can use the '*' and '?' chars as wildcards for all chars or a single char (respectively). 
            KLST_PATTERN_MATCH patternMatch = new KLST_PATTERN_MATCH();
            if (MI != -1)
                patternMatch.DeviceID = String.Format("USB\\VID_{0:X4}&PID_{1:X4}&MI_{2:X2}*", Vid, Pid, MI);
            else
                patternMatch.DeviceID = String.Format("USB\\VID_{0:X4}&PID_{1:X4}*", Vid, Pid);

            LstK deviceList = new LstK(KLST_FLAG.NONE, ref patternMatch);
            KLST_DEVINFO_HANDLE deviceInfo;
            interfaceDescriptor = new USB_INTERFACE_DESCRIPTOR();
            pipeInfo = new WINUSB_PIPE_INFORMATION();
            usb = null;

            // Iterate the devices looking for a matching alt-interface and endpoint id.
            while (deviceList.MoveNext(out deviceInfo))
            {
                // libusbK class contructors can throw exceptions; For instance, if the device is
                // using the WinUsb driver and already in-use by another application.
                //Console.WriteLine("Opening usb device..");
                usb = new UsbK(deviceInfo);

                //Console.WriteLine("Finding interface and endpoint by PipeId..");
                success = FindPipeAndInterface(usb, out interfaceDescriptor, out pipeInfo);
                if (success)
                    break;

                //Console.WriteLine("PipeId {0:X2}h not found on this device.", PipeId);
                usb.Free();
                usb = null;
            }

            if (ReferenceEquals(usb, null))
            {
                //Console.WriteLine("Usb device not found: {0}", patternMatch.DeviceID);
                success = false;
            }
            else
            {
                MI = interfaceDescriptor.bInterfaceNumber;
                AltInterfaceId = interfaceDescriptor.bAlternateSetting;
                PipeId = pipeInfo.PipeId;

                // Set interface alt setting.
                //Console.WriteLine("Setting interface #{0} to bAlternateSetting #{1}..", interfaceDescriptor.bInterfaceNumber, interfaceDescriptor.bAlternateSetting);
                //success = usb.SetAltInterface(interfaceDescriptor.bInterfaceNumber, false, interfaceDescriptor.bAlternateSetting);
                success = true;
                if (!success)
                {
                    //Console.WriteLine("SetAltInterface failed. ErrorCode={0:X8}h", Marshal.GetLastWin32Error());
                    //Console.WriteLine(interfaceDescriptor.ToString());
                }
            }

            deviceList.Free();
            return success;
        }

        public bool FindPipeAndInterface(UsbK usb, out USB_INTERFACE_DESCRIPTOR interfaceDescriptor, out WINUSB_PIPE_INFORMATION pipeInfo)
        {
            if (FindPipeAndInterface(usb, out interfaceDescriptor, out pipeInfo, AltInterfaceId, PipeId))
            {
                AltInterfaceId = interfaceDescriptor.bAlternateSetting;
                PipeId = pipeInfo.PipeId;

                return true;
            }
            return false;
        }

        public static bool FindPipeAndInterface(UsbK usb, out USB_INTERFACE_DESCRIPTOR interfaceDescriptor, out WINUSB_PIPE_INFORMATION pipeInfo, int altInterfaceId, int pipeId)
        {
            byte interfaceIndex = 0;
            interfaceDescriptor = new USB_INTERFACE_DESCRIPTOR();
            pipeInfo = new WINUSB_PIPE_INFORMATION();
            while (usb.SelectInterface(interfaceIndex, true))
            {
                byte altSettingNumber = 0;
                while (usb.QueryInterfaceSettings(altSettingNumber, out interfaceDescriptor))
                {
                    if (altInterfaceId == -1 || altInterfaceId == altSettingNumber)
                    {
                        byte pipeIndex = 0;
                        while (usb.QueryPipe(altSettingNumber, pipeIndex++, out pipeInfo))
                        {
                            if ((pipeInfo.MaximumPacketSize > 0) && pipeId == -1 || pipeInfo.PipeId == pipeId || ((pipeId & 0xF) == 0 && ((pipeId & 0x80) == (pipeInfo.PipeId & 0x80))))
                                goto FindInterfaceDone;
                            pipeInfo.PipeId = 0;
                        }
                    }
                    altSettingNumber++;
                }
                interfaceIndex++;
            }

            FindInterfaceDone:
            return pipeInfo.PipeId != 0;
        }

        public override string ToString()
        {
            return string.Format("Vid: {0:X4}h\nPid: {1:X4}h\nMI: {2:X2}h\nPipeId: {3:X2}h\n\n", Vid, Pid, MI, PipeId);
        }

    }*/
}