﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinBridge
{
    public partial class frmAziDistCalc : Form
    {
        public frmAziDistCalc()
        {
            InitializeComponent();
        }

        private void FrmAziDistCalc_Load(object sender, EventArgs e)
        {
            value_TextChanged(sender, e);
        }

        private void InvalidateCalc()
        {
            lblDist.Text = "- - -";
            lblAltitude.Text = "- - -";
            lblAzimuth.Text = "- - -";
        }

        private void value_TextChanged(object sender, EventArgs e)
        {
            GMath.GLocation a = new GMath.GLocation();
            GMath.GLocation b = new GMath.GLocation();

            if (GMath.ParseAngle(tbLatA, 90.0, out a.latitude) == false)
            {
                InvalidateCalc();
                return;
            }
            if (GMath.ParseAngle(tbLatB, 90.0, out b.latitude) == false)
            {
                InvalidateCalc();
                return;
            }

            if (GMath.ParseAngle(tbLongA, 180.0, out a.longitude) == false)
            {
                InvalidateCalc();
                return;
            }
            if (GMath.ParseAngle(tbLongB, 180.0, out b.longitude) == false)
            {
                InvalidateCalc();
                return;
            }
            if (GMath.ParseElevation(tbElevA, out a.elevation) == false)
            {
                InvalidateCalc();
                return;
            }
            if (GMath.ParseElevation(tbElevB, out b.elevation) == false)
            {
                InvalidateCalc();
                return;
            }

            GMath.GPoint ap = GMath.LocationToPoint(a, true);
            GMath.GPoint bp = GMath.LocationToPoint(b, true);
            double distKm = GMath.Distance(ap, bp) * 0.001;
            lblDist.Text = distKm.ToString("0.#####", CultureInfo.InvariantCulture);

            // per semplificare, ruota il globo in modo che il punto A si trovi a latitudine e longitudine 0
            // mantenendo il raggio calcolato sul geoide oblato ma usando l'angolo ottenuto dalla sottrazione.
            // Il punto A sarà quindi equivalente a X=raggio, Y=0, Z=0
            // La differenza vettoriale B-A avrà dZ = component N/S e dY componente E/W.           
            var br = GMath.RotateGlobe(b, a, bp.radius, ap.radius, true);
            if (br.z * br.z + br.y * br.y > 1.0e-6)
            {
                var theta = Math.Atan2(br.z, br.y) * 180.0 / Math.PI;
                var azimuth = 90.0 - theta;
                if (azimuth < 0.0)
                    azimuth += 360.0;
                if (azimuth > 360.0)
                    azimuth -= 360.0;
                lblAzimuth.Text = azimuth.ToString("0.#####", CultureInfo.InvariantCulture);
            }
            else
                lblAzimuth.Text = "?";

            GMath.GPoint bma;
            if (GMath.NormalizeVectorDiff(bp, ap, out bma))
            {
                // Calcola il valore di elevazione espresso come angolo rispetto l'orizzonte di B visto dal punto A
                double altitude = 90.0 - (180.0 / Math.PI) * Math.Acos(bma.x * ap.nx + bma.y * ap.ny + bma.z * ap.nz);
                lblAltitude.Text = altitude.ToString("0.#####", CultureInfo.InvariantCulture);
            }
        }

    }
}
