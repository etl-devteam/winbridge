﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinBridge
{
    public partial class frmCalibration : Form
    {
        MagCal.MagCalResult? MagCalData = null;

        public frmCalibration()
        {
            InitializeComponent();
        }


        public void Worker_CollectData(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == -2)
            {
                HeadTracking.IMUData data = (HeadTracking.IMUData)e.UserState;

                HeadTracking.setSingleIMUData(data);
                if (lblCount.Visible == false)
                    lblCount.Visible = true;
                lblCount.Text = "Valori collezionati: " + HeadTracking.getCalibrationSamplesNumber();

                if (!HeadTracking.isCalibrationRunning())
                {
                    endCalibration();
                }
                pbStatus.Image = Properties.Resources.smallgreenled;
            }
            else
                pbStatus.Image = Properties.Resources.smallredled;
        }

        private void setResult(double[] a, double[] b)
        {
            lblB0.Text = b[0].ToString("F5", CultureInfo.InvariantCulture);
            lblB1.Text = b[1].ToString("F5", CultureInfo.InvariantCulture);
            lblB2.Text = b[2].ToString("F5", CultureInfo.InvariantCulture);

            lblA0.Text = a[0].ToString("F5", CultureInfo.InvariantCulture);
            lblA1.Text = a[1].ToString("F5", CultureInfo.InvariantCulture);
            lblA2.Text = a[2].ToString("F5", CultureInfo.InvariantCulture);
            lblA3.Text = a[3].ToString("F5", CultureInfo.InvariantCulture);
            lblA4.Text = a[4].ToString("F5", CultureInfo.InvariantCulture);
            lblA5.Text = a[5].ToString("F5", CultureInfo.InvariantCulture);
            lblA6.Text = a[6].ToString("F5", CultureInfo.InvariantCulture);
            lblA7.Text = a[7].ToString("F5", CultureInfo.InvariantCulture);
            lblA8.Text = a[8].ToString("F5", CultureInfo.InvariantCulture);

            lblMagB.Text = "0.0";
            tableLayoutPanel1.Visible = true;
        }

        private void endCalibration()
        {
            double[] calibrationMatrixA = new double[9];
            double[] calibrationMatrixB = new double[3];

            lblProgress.Text = "Premere 'Avvia' per iniziare la raccolta dati";
            btnInterrompi.Visible = false;
            btnAvvia.Visible = true;
            lblCount.Visible = false;
            if (HeadTracking.getCalibrationResult(calibrationMatrixA, calibrationMatrixB))
            {
                setResult(calibrationMatrixA, calibrationMatrixB);
                btnOK.Enabled = true;
            }
            else
                MessageBox.Show("I dati raccolti non hanno prodotto coefficienti validi,\r\nè necessario riavviare la procedura.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnInterrompi_Click(object sender, EventArgs e)
        {
            lblProgress.Text = "Premere 'Avvia' per iniziare la raccolta dati";
            btnInterrompi.Visible = false;
            btnAvvia.Visible = true;
            lblCount.Visible = false;
        }

        private void btnAvvia_Click(object sender, EventArgs e)
        {
            lblProgress.Text = "Premere 'Interrompi' per terminare la raccolta dati";
            btnInterrompi.Visible = true;
            btnAvvia.Visible = false;
            tableLayoutPanel1.Visible = false;
            btnOK.Enabled = false;
            HeadTracking.startCalibration();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (MagCalData.HasValue)
            {
                
                Properties.Settings.Default.CalMagACalX = MagCalData.Value.A[0,0].ToString().Replace(',', '.');
                Properties.Settings.Default.CalMagACalY = MagCalData.Value.A[1, 1].ToString().Replace(',', '.');
                Properties.Settings.Default.CalMagACalZ = MagCalData.Value.A[2, 2].ToString().Replace(',', '.');
                Properties.Settings.Default.CalMagBCalX = MagCalData.Value.b[0].ToString().Replace(',', '.');
                Properties.Settings.Default.CalMagBCalY = MagCalData.Value.b[1].ToString().Replace(',', '.');
                Properties.Settings.Default.CalMagBCalZ = MagCalData.Value.b[2].ToString().Replace(',', '.');
                Properties.Settings.Default.Save();
            }
        }
    }
}
