﻿using MadWizard.WinUSBNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinBridge
{
    class VisionAR
    {
        public const int PIXEL_WIDTH = 419;
        public const int PIXEL_HEIGHT = 138;

        public static byte[] fbBuf = new byte[PIXEL_WIDTH * PIXEL_HEIGHT + 7];

        // questo valore si trova nel file INF del driver ErGlassDriverFb.inf:
        public const string GUIDStringFB = "{B45F4147-19A2-44F8-A165-290A8BFCB08B}";

        // questo valore si trova nel file INF del driver ErGlassDriverCDC.inf:
        public const string GUIDStringCDC = "{F491F0FD-BECD-4199-8582-2C8AF59D03AC}";

        public const int VID = 0x0483;
        public const int PID = 0xA306;

        public const int FB_OUT_ENDPOINT = 0x01;
        public const int CDC_OUT_ENDPOINT = 0x03;
        public const int CDC_IN_ENDPOINT = 0x83;

        private static byte CalcCsum(byte[] buf, int index, int len)
        {
            byte csum = 0;

            for (int i = 0; i < len; i++)
                csum ^= buf[index + i];
            return csum;
        }

        public static bool SendPixels(ref byte[] pixels, USBInterface usbif)
        {
            try
            {
                fbBuf[0] = 0x01;        // SOH
                fbBuf[1] = 0x01;        // CTRL
                fbBuf[2] = 0xE0;        // PAYLOAD LEN LSB
                fbBuf[3] = 0xE1;        // PAYLOAD LEN MSB
                fbBuf[4] = 0x01;        // HDR CSUM
                fbBuf[5] = 0x01;        // CMD

                byte crc = 0x01;
                for (int i = 0; i < (PIXEL_HEIGHT * PIXEL_WIDTH); i++)
                {
                    byte b = pixels[i];
                    fbBuf[6 + i] = b;
                    crc ^= b;
                }
                fbBuf[57828] = crc;
                usbif.OutPipe.Write(fbBuf);
                return true;
            }
            catch { return false; }
        }

        public static bool GetStatus(USBInterface usbif, out HeadTracking.IMUData imu)
        {
            imu = new HeadTracking.IMUData();

            byte[] buf = new byte[7];
            buf[0] = 0x01;      // SOH
            buf[1] = 0x01;      // CTRL
            buf[2] = 0x02;      // PAYLOAD LEN LSB
            buf[3] = 0x00;      // PAYLOAD LEN MSB
            buf[4] = 0x02;      // HDR CSUM
            buf[5] = 0x90;      // CMD
            buf[6] = 0x90;      // PAYLOAD CSUM

            try
            {
                usbif.OutPipe.Write(buf);

                buf = new byte[64];
                usbif.InPipe.Read(buf);

                if (buf[1] == 0x03)
                {
                    if (CalcCsum(buf, 0, 4) == buf[4])
                    {
                        int len = buf[2] + buf[3] * 256;
                        if ((len >= 31) && (CalcCsum(buf, 5, len - 1) == buf[5 + len - 1]))
                        {
                            imu.acc.x = BitConverter.ToInt16(buf, 10);
                            imu.acc.y = BitConverter.ToInt16(buf, 12);
                            imu.acc.z = BitConverter.ToInt16(buf, 14);
                            imu.gyro.x = BitConverter.ToInt16(buf, 16);
                            imu.gyro.y = BitConverter.ToInt16(buf, 18);
                            imu.gyro.z = BitConverter.ToInt16(buf, 20);
                            imu.mag.x = BitConverter.ToInt16(buf, 22);
                            imu.mag.y = BitConverter.ToInt16(buf, 24);
                            imu.mag.z = BitConverter.ToInt16(buf, 26);
                            return true;
                        }
                    }
                }
            }
            catch { }
            return false;
        }


    }
}
