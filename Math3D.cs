﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinBridge
{
	public class RPOINT
	{
		public double x;
		public double y;
		public double z;
		public double radius;
	};

	public class LPOINT
	{
		public double x;
		public double y;
		public double z;
		public double radius;
		public double nx;
		public double ny;
		public double nz;
	};

	public class ROTATION_MATRIX
	{
		public double xx;
		public double xy;
		public double xz;

		public double yx;
		public double yy;
		public double yz;

		public double zx;
		public double zy;
		public double zz;
	};

	public class POINT2D
	{
		public int x;
		public int y;
	};

	public class POINT3D
	{
		public double x;
		public double y;
		public double z;
	};

	public class LOCATION
	{
		public double elev;        // height above sea-level/ellipsoid
		public double lat;         // latitude
		public double lon;         // longitude
	};

	public class SPHERICAL_COORDINATE
	{
		public double azimuth;
		public double altitude;
		public double distance;
	};

	public class VECTOR
	{
		public double x;
		public double y;
		public double z;
		public double distance;
	};

	class Math3D
	{
		public const double WGS84_SEMI_MAJOR_AXIS = 6378137.00000;          // equatorial radius in meters
		public const double WGS84_SEMI_MINOR_AXIS = 6356752.314245;         // polar radius in meters		(https://en.wikipedia.org/wiki/World_Geodetic_System)
		public const double WGS84_ECCENTRICITY_SQUARED = 0.00669437999014;  // first eccentricity squared	(https://en.wikipedia.org/wiki/Geodetic_datum)

		//--------------------------------------------------------------------------------------
		// Init the rotation matrix for rotating a point arount it's origin
		// The rotation of a vector can be described as the product between a rotation matrix
		// and the vector itself. See https://de.wikipedia.org/wiki/Roll-Nick-Gier-Winkel where
		// it is shown the rotation matrix for certain Euler angles.
		// Roll pitch and yaw are expressed in radians.
		//--------------------------------------------------------------------------------------
		public static ROTATION_MATRIX InitRotationMatrix(double roll, double pitch, double yaw)
		{
			ROTATION_MATRIX matrix = new ROTATION_MATRIX();

			double cosa = Math.Cos(roll);     // rotation around observer distance axis Z
			double sina = Math.Sin(roll);

			double cosb = Math.Cos(yaw);      // rotation around observer vertical axis Y
			double sinb = Math.Sin(yaw);

			double cosc = Math.Cos(pitch);    // rotation around observer horizontal axis X
			double sinc = Math.Sin(pitch);

			matrix.xx = cosa * cosb;
			matrix.xy = cosa * sinb * sinc - sina * cosc;
			matrix.xz = cosa * sinb * cosc + sina * sinc;

			matrix.yx = sina * cosb;
			matrix.yy = sina * sinb * sinc + cosa * cosc;
			matrix.yz = sina * sinb * cosc - cosa * sinc;

			matrix.zx = -sinb;
			matrix.zy = cosb * sinc;
			matrix.zz = cosb * cosc;

			return matrix;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void MatrixRotate(ref POINT3D point, ref ROTATION_MATRIX matrix)
		{
			double px = point.x;
			double py = point.y;
			double pz = point.z;
			point.x = matrix.xx * px + matrix.xy * py + matrix.xz * pz;
			point.y = matrix.yx * px + matrix.yy * py + matrix.yz * pz;
			point.z = matrix.zx * px + matrix.zy * py + matrix.zz * pz;
		}

		//--------------------------------------------------------------------------------------
		// Calculates the Earth radius in meters at the given latitude.
		// Latitude is geodetic, i.e. that reported by a GPS
		// http://en.wikipedia.org/wiki/Earth_radius
		//--------------------------------------------------------------------------------------
		public static double EarthRadiusInMeters(double latitude_radians)
		{
			double cosLat = Math.Cos(latitude_radians);
			double sinLat = Math.Sin(latitude_radians);
			double t1 = WGS84_SEMI_MAJOR_AXIS * WGS84_SEMI_MAJOR_AXIS * cosLat;
			double t2 = WGS84_SEMI_MINOR_AXIS * WGS84_SEMI_MINOR_AXIS * sinLat;
			double t3 = WGS84_SEMI_MAJOR_AXIS * cosLat;
			double t4 = WGS84_SEMI_MINOR_AXIS * sinLat;
			return Math.Sqrt((t1 * t1 + t2 * t2) / (t3 * t3 + t4 * t4));
		}

		//--------------------------------------------------------------------------------------
		// Convert geodetic latitude 'lat' to a geocentric latitude 'clat'.
		// Geodetic latitude is the latitude as given by a GPS.
		// Geocentric latitude is the angle measured from center of Earth between a point and the equator.
		// https://en.wikipedia.org/wiki/Latitude#Geocentric_latitude
		//--------------------------------------------------------------------------------------
		public static double GeocentricLatitude(double lat)
		{
			return Math.Atan((1.0 - WGS84_ECCENTRICITY_SQUARED) * Math.Tan(lat));
		}

		//--------------------------------------------------------------------------------------
		// Convert (lat, lon, hae) to (x, y, z)
		//--------------------------------------------------------------------------------------
		public static LPOINT LocationToPoint(ref LOCATION loc)
		{
			LPOINT nv = new LPOINT();

			double lat = loc.lat * Math.PI / 180.0;
			double lon = loc.lon * Math.PI / 180.0;
			nv.radius = EarthRadiusInMeters(lat);
			double clat = GeocentricLatitude(lat);

			double cosLon = Math.Cos(lon);
			double sinLon = Math.Sin(lon);
			double cosLat = Math.Cos(clat);
			double sinLat = Math.Sin(clat);
			nv.x = nv.radius * cosLon * cosLat;
			nv.y = nv.radius * sinLon * cosLat;
			nv.z = nv.radius * sinLat;

			// I used geocentric latitude to calculate (x, y, z) on the Earth's ellipsoid
			// Now I use geodetic latitude to calculate normal vector from the surface, to correct for elevation
			double cosGlat = Math.Cos(lat);
			double sinGlat = Math.Sin(lat);

			nv.nx = cosGlat * cosLon;
			nv.ny = cosGlat * sinLon;
			nv.nz = sinGlat;

			nv.x += loc.elev * nv.nx;
			nv.y += loc.elev * nv.ny;
			nv.z += loc.elev * nv.nz;

			return nv;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static double Distance(ref LPOINT ap, ref LPOINT bp)
		{
			double dx = ap.x - bp.x;
			double dy = ap.y - bp.y;
			double dz = ap.z - bp.z;
			return Math.Sqrt(dx * dx + dy * dy + dz * dz);
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static RPOINT RotateGlobe(ref LOCATION b, ref LOCATION a, double b_radius)
		{
			RPOINT rp = new RPOINT();

			// Get modified coordinates of 'b' by rotating the globe so that 'a' is at lat = 0, lon = 0
			LOCATION br = new LOCATION();
			br.lat = b.lat;
			br.lon = b.lon - a.lon;
			br.elev = b.elev;
			LPOINT brp = LocationToPoint(ref br);

			// Rotate brp cartesian coordinates around the z-axis by a.lon degrees,
			// then around the y-axis by a.lat degrees.
			// Though we are decreasing by a.lat degrees, as seen above the y-axis,
			// this is a positive (counterclockwise) rotation (if B's longitude is east of A's).
			// However, from this point of view the x-axis is pointing left.
			// So we will look the other way making the x-axis pointing right, the z-axis
			// pointing up, and the rotation treated as negative.

			double alat = GeocentricLatitude(-a.lat * Math.PI / 180.0);
			double cosAlat = Math.Cos(alat);
			double sinAlat = Math.Sin(alat);

			rp.x = (brp.x * cosAlat) - (brp.z * sinAlat);
			rp.y = brp.y;
			rp.z = (brp.x * sinAlat) + (brp.z * cosAlat);
			rp.radius = b_radius;

			return rp;
		}

		//--------------------------------------------------------------------------------------
		// Calculate norm(b - a), where norm divides a vector by its length to produce a unit vector
		//--------------------------------------------------------------------------------------
		public static RPOINT NormalizeVectorDiff(ref LPOINT b, ref LPOINT a)
		{
			RPOINT p = new RPOINT();

			double dx = b.x - a.x;
			double dy = b.y - a.y;
			double dz = b.z - a.z;
			double dist2 = dx * dx + dy * dy + dz * dz;
			if (dist2 == 0)
			{
				p.radius = 0;
				return p;
			}

			double dist = Math.Sqrt(dist2);
			p.x = dx / dist;
			p.y = dy / dist;
			p.z = dz / dist;
			p.radius = 1.0;
			return p;
		}

		//--------------------------------------------------------------------------------------
		// Given the latitude, longitude, and elevation of two points on the Earth, this function
		// determines the azimuth (compass direction) and distance of the second point (B) as seen
		// from the first point (A).
		//
		// NOTES:
		//
		// If a point is North of the equator, make it's latitude positive, or negative if South.
		//
		// If a point is East of the prime meridian(Greenwich, England), its longitude is
		// positive. For example, the longitude is a negative number anywhere in the United States, 
		// because all areas are at the west of England.
		//
		// Elevation (hae) is how many meters above sea level (or ellipsoid) a point is. If elevation
		// in unknown, we can still get a pretty accurate answer in most cases by leaving it at 0 (zero).
		// Use a negative value for points beneath sea level.
		//
		// WARNING!!! Here, the distance is defined as the length of the straight line between the points,
		// ignoring the Earth's curvature. If the points are far enough apart, chances are this straight
		// line goes right through the Earth. In a future Bridge evolution, a more complex and precise
		// calculation may be required.
		//
		// The azimuth is how many degrees clockwise from North you have to rotate in order to face
		// Point B when standing at Point A.
		//
		// The altitude is how many degrees above(if positive) or below(if negative) the horizon Point B
		// is when seen from Point A. For example, if A and B are both at sea level, as B gets farther away,
		// it gradually dips below the horizon because of the Earth's curvature.
		// 
		// This function corrects for oblateness of the Earth according to World Geodetic System (WGS84)
		// reference coordinate system (if I didn't make mistakes). As you get closer to the equator, the
		// distance from the center of the Earth is greater than it is near the poles.
		//--------------------------------------------------------------------------------------
		public static SPHERICAL_COORDINATE CalculateSphericalCoordinate(ref LOCATION a, ref LOCATION b)
		{
			SPHERICAL_COORDINATE c = new SPHERICAL_COORDINATE();

			LPOINT ap = LocationToPoint(ref a);
			LPOINT bp = LocationToPoint(ref b);
			c.distance = Distance(ref ap, ref bp);          // distance in meters between the two points

			// To calculate azimuth:
			// Rotate the globe so that point A is translated to latitude 0 and longitude 0
			// Keep the actual radii calculated based on the oblate geoid, but use angles based on subtraction
			// Point A will be at x = radius, y = 0, z = 0
			// Vector difference B - A will have dz = N/S component and dy = E/W component
			RPOINT br = RotateGlobe(ref b, ref a, bp.radius);

			if (((br.z * br.z) + (br.y * br.y)) > 1.0e-6)
			{
				double theta = Math.Atan2(br.z, br.y) * 180.0 / Math.PI;
				c.azimuth = 90.0 - theta;
				if (c.azimuth < 0.0)
					c.azimuth += 360.0;

				if (c.azimuth > 360.0)
					c.azimuth -= 360.0;
			}

			RPOINT bma = NormalizeVectorDiff(ref bp, ref ap);
			if (bma.radius != 0.0)
			{
				// Calculate altitude, which is the angle above the horizon of B as seen from A.
				// Almost always, B will actually be below the horizon, so the altitude will be negative.
				// The dot product of bma and norm = cos(zenith_angle), and zenith_angle = (90 deg) - altitude.
				// So altitude = 90 - acos(dotprod).
				c.altitude = 90.0 - (180.0 / Math.PI) * Math.Acos(bma.x * ap.nx + bma.y * ap.ny + bma.z * ap.nz);
			}
			return c;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static double Magnitude(double x, double y, double z)
		{
			return Math.Sqrt(x * x + y * y + z * z);
		}

		//--------------------------------------------------------------------------------------
		// https://it.mathworks.com/help/matlab/ref/sph2cart.html
		//--------------------------------------------------------------------------------------
		public static POINT3D SphericalToCartesian(ref POINT3D origin, ref SPHERICAL_COORDINATE sc)
		{
			POINT3D p = new POINT3D();

			double azimuth = sc.azimuth * Math.PI / 180;     // converte da gradi a radianti
			double altitude = sc.altitude * Math.PI / 180;

			double z = sc.distance * Math.Cos(altitude) * Math.Cos(azimuth);
			double x = sc.distance * Math.Cos(altitude) * Math.Sin(azimuth);
			double y = sc.distance * Math.Sin(altitude);

			p.x = origin.x + x;
			p.y = origin.y + y;
			p.z = origin.z + z;
			return p;
		}

		//--------------------------------------------------------------------------------------
		// https://it.mathworks.com/help/matlab/ref/cart2sph.html
		//--------------------------------------------------------------------------------------
		public static SPHERICAL_COORDINATE CartesianToSpherical(ref POINT3D origin, ref POINT3D v)
		{
			SPHERICAL_COORDINATE sc = new SPHERICAL_COORDINATE();

			double x = (v.x - origin.x);
			double y = (v.y - origin.y);
			double z = (v.z - origin.z);

			sc.distance = Magnitude(x, y, z);
			sc.azimuth = Math.Atan2(x, z);
			sc.altitude = Math.Atan2(y, Math.Sqrt(z * z + x * x));

			// converte da radianti a gradi
			sc.azimuth = sc.azimuth / Math.PI * 180;
			sc.altitude = sc.altitude / Math.PI * 180;
			return sc;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static POINT3D Cross(ref POINT3D v1, ref POINT3D v2)
		{
			POINT3D cr = new POINT3D();

			cr.x = (v1.y * v2.z) - (v1.z * v2.y);
			cr.y = (v1.z * v2.x) - (v1.x * v2.z);
			cr.z = (v1.x * v2.y) - (v1.y * v2.x);
			return cr;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static double DotProduct(ref POINT3D v1, ref POINT3D v2)
		{
			return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static double ScalarTriple(ref POINT3D a, ref POINT3D b, ref POINT3D c)
		{
			POINT3D x = Cross(ref a, ref b);
			return DotProduct(ref x, ref c);
		}

		//--------------------------------------------------------------------------------------
		// Given line pq and ccw quadrilateral abcd, return whether the line pierce the triangle.
		// If so, also return the point r of intersection.
		// (Christer Ericson, CRC Press, 22 dic 2004, Real-Time Collision Detection)
		//--------------------------------------------------------------------------------------
		public static bool IntersectLineQuad(ref POINT3D p, ref POINT3D q, ref POINT3D a, ref POINT3D b, ref POINT3D c, ref POINT3D d, ref POINT3D r)
		{
			POINT3D pq = new POINT3D();
			POINT3D pa = new POINT3D();
			POINT3D pb = new POINT3D();
			POINT3D pc = new POINT3D();
			POINT3D pd = new POINT3D();

			pq.x = p.x - q.x;       // segno invertito rispetto all'originale
			pq.y = p.y - q.y;
			pq.z = p.z - q.z;

			pa.x = a.x - p.x;
			pa.y = a.y - p.y;
			pa.z = a.z - p.z;

			pb.x = b.x - p.x;
			pb.y = b.y - p.y;
			pb.z = b.z - p.z;

			pc.x = c.x - p.x;
			pc.y = c.y - p.y;
			pc.z = c.z - p.z;

			// Determine which triangle to test against by testing against diagonal first
			POINT3D m = Cross(ref pc, ref pq);
			double v = DotProduct(ref pa, ref m);          // ScalarTriple(pq, pa, pc);
			if (v >= 0)
			{
				// Test intersection against triangle abc
				double u = -DotProduct(ref pb, ref m);    // ScalarTriple(pq, pc, pb);
				if (u < 0)
					return false;
				double w = ScalarTriple(ref pq, ref pb, ref pa);
				if (w < 0)
					return false;
				// Compute r, r = u*a + v*b + w*c, from barycentric coordinates (u, v, w)
				double denom = 1.0f / (u + v + w);
				u *= denom;
				v *= denom;
				w *= denom;

				r.x = u * a.x + v * b.x + w * c.x;
				r.y = u * a.y + v * b.y + w * c.y;
				r.z = u * a.z + v * b.z + w * c.z;
			}
			else
			{
				// Test intersection against triangle dac
				pd.x = d.x - p.x;
				pd.y = d.y - p.y;
				pd.z = d.z - p.z;
				double u = DotProduct(ref pd,ref m);     // ScalarTriple(pq, pd, pc);
				if (u < 0)
					return false;
				double w = ScalarTriple(ref pq, ref pa, ref pd);
				if (w < 0)
					return false;
				v = -v;
				// Compute r, r = u*a + v*d + w*c, from barycentric coordinates (u, v, w)
				double denom = 1.0f / (u + v + w);
				u *= denom;
				v *= denom;
				w *= denom;
				r.x = u * a.x + v * d.x + w * c.x;
				r.y = u * a.y + v * d.y + w * c.y;
				r.z = u * a.z + v * d.z + w * c.z;
			}
			return true;
		}

		//--------------------------------------------------------------------------------------
		// Calculate the shortest distance of point from the line defined by vertex1 and vertex 2
		//--------------------------------------------------------------------------------------
		public static double ShortestDistance(ref POINT3D vertex1, ref POINT3D vertex2, ref POINT3D point)
		{
			POINT3D ab = new POINT3D();
			POINT3D ac = new POINT3D();

			ab.x = vertex1.x - vertex2.x;
			ab.y = vertex1.y - vertex2.y;
			ab.z = vertex1.z - vertex2.z;

			ac.x = point.x - vertex1.x;
			ac.y = point.y - vertex1.y;
			ac.z = point.z - vertex1.z;

			POINT3D cross = Cross(ref ab, ref ac);
			double area = Magnitude(cross.x, cross.y, cross.z);
			return area / Magnitude(ab.x, ab.y, ab.z);
		}

	}
}