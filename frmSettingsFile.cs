﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinBridge
{
    public partial class frmSettingsFile : Form
    {
        private class YesNoConverter : BooleanConverter
        {
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
            {
                if ((value is bool) && (destinationType == typeof(string)))
                {
                    return values[(bool)value ? 1 : 0];
                }
                return base.ConvertTo(context, culture, value, destinationType);
            }

            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                string txt = value as string;
                if (values[0] == txt) return false;
                if (values[1] == txt) return true;
                return base.ConvertFrom(context, culture, value);
            }

            private string[] values = new string[] { "No", "Si" };
        }

        private class properties
        {
            [Category("Ellisse"), Description("Larghezza dell'ellisse"), DisplayName("Larghezza")]
            public string EllipseWidth { get; set; }
            [Category("Ellisse"), Description("Altezza dell'ellisse"), DisplayName("Altezza")]
            public string EllipseHeight { get; set; }
            [Category("Ellisse"), Description("Spostamento dell'ellisse dalla base sull'asse verticale"), DisplayName("Spostamento verticale")]
            public string EllipseOffset { get; set; }

            [Category("Varie"), Description("Frequenza di acquisizione dell'unità inerziale in hertz"), DisplayName("Frequenza di acquisizione (Hz)")]
            public string IMU_SampleRate_HZ { get; set; }

            [Category("Varie"), Description("Inversione dell'asse verticale"), DisplayName("Inversione verticale"), TypeConverter(typeof(YesNoConverter))]
            public bool VFlip { get; set; }

            [Category("Posizione"), Description("Rotazione in gradi sull'asse X dell'unità olografica rispetto al centro ottico"), DisplayName("Rotazione X")]
            public string ViewportYaw { get; set; }
            [Category("Posizione"), Description("Rotazione in gradi sull'asse Y dell'unità olografica rispetto al centro ottico"), DisplayName("Rotazione Y")]
            public string ViewportPitch { get; set; }
            [Category("Posizione"), Description("Rotazione in gradi sull'asse Z dell'unità olografica rispetto al centro ottico"), DisplayName("Rotazione Z")]
            public string ViewportRoll { get; set; }

            [Category("Posizione"), Description("Spostamento del centro sull'asse X dell'unità olografica rispetto al centro ottico"), DisplayName("Allineamento X")]
            public string ViewportCenterX { get; set; }
            [Category("Posizione"), Description("Spostamento del centro sull'asse Y dell'unità olografica rispetto al centro ottico"), DisplayName("Allineamento Y")]
            public string ViewportCenterY { get; set; }
            [Category("Posizione"), Description("Spostamento del centro sull'asse Z dell'unità olografica rispetto al centro ottico"), DisplayName("Allineamento Z")]
            public string ViewportCenterZ { get; set; }

            [Category("Posizione"), Description("Spostamento del centro ottico sull'asse X rispetto alla propria posizione GPS"), DisplayName("Spostamento X")]
            public string ViewportOffsetX { get; set; }
            [Category("Posizione"), Description("Spostamento del centro ottico sull'asse Y rispetto alla propria posizione GPS"), DisplayName("Spostamento Y")]
            public string ViewportOffsetY { get; set; }

            [Category("Calibrazione"), Description("Valore di calibrazione per l'asse X del giroscopio"), DisplayName("Offset X giroscopio")]
            public string CalGyroOffsetX { get; set; }
            [Category("Calibrazione"), Description("Valore di calibrazione per l'asse Y del giroscopio"), DisplayName("Offset Y giroscopio")]
            public string CalGyroOffsetY { get; set; }
            [Category("Calibrazione"), Description("Valore di calibrazione per l'asse Z del giroscopio"), DisplayName("Offset Z giroscopio")]
            public string CalGyroOffsetZ { get; set; }
            [Category("Calibrazione"), Description("Valore di calibrazione A-X del magnetometro"), DisplayName("Calibrazione A-X magnetometro")]
            public string CalMagACalX { get; set; }
            [Category("Calibrazione"), Description("Valore di calibrazione A-Y del magnetometro"), DisplayName("Calibrazione A-Y magnetometro")]
            public string CalMagACalY { get; set; }
            [Category("Calibrazione"), Description("Valore di calibrazione A-Z del magnetometro"), DisplayName("Calibrazione A-Z magnetometro")]
            public string CalMagACalZ { get; set; }
            [Category("Calibrazione"), Description("Valore di calibrazione B-X del magnetometro"), DisplayName("Calibrazione B-X magnetometro")]
            public string CalMagBCalX { get; set; }
            [Category("Calibrazione"), Description("Valore di calibrazione B-Y del magnetometro"), DisplayName("Calibrazione B-Y magnetometro")]
            public string CalMagBCalY { get; set; }
            [Category("Calibrazione"), Description("Valore di calibrazione B-Z del magnetometro"), DisplayName("Calibrazione B-Z magnetometro")]
            public string CalMagBCalZ { get; set; }
            [Category("Calibrazione"), Description("Valore iniziale Beta per l'algoritmo"), DisplayName("Beta iniziale")]
            public string InitialBeta { get; set; }
            [Category("Calibrazione"), Description("Valore iniziale Zeta per l'algoritmo"), DisplayName("Zeta iniziale")]
            public string InitialZeta { get; set; }
            [Category("Calibrazione"), Description("Valore normale Beta per l'algoritmo"), DisplayName("Beta")]
            public string Beta { get; set; }
            [Category("Calibrazione"), Description("Valore normale Zeta per l'algoritmo"), DisplayName("Zeta")]
            public string Zeta { get; set; }

            public void Load()
            {
                EllipseWidth = Properties.Settings.Default.EllipseWidth.Replace(',', '.').Trim();
                EllipseHeight = Properties.Settings.Default.EllipseHeight.Replace(',', '.').Trim();
                EllipseOffset = Properties.Settings.Default.EllipseOffset.Replace(',', '.').Trim();
                IMU_SampleRate_HZ = Properties.Settings.Default.IMU_SampleRate_HZ.Replace(',', '.').Trim();

                VFlip = Properties.Settings.Default.VFlip;
                ViewportYaw = Properties.Settings.Default.ViewportYaw.Replace(',', '.').Trim();
                ViewportPitch = Properties.Settings.Default.ViewportPitch.Replace(',', '.').Trim();
                ViewportRoll = Properties.Settings.Default.ViewportRoll.Replace(',', '.').Trim();

                ViewportCenterX = Properties.Settings.Default.ViewportCenterX.Replace(',', '.').Trim();
                ViewportCenterY = Properties.Settings.Default.ViewportCenterY.Replace(',', '.').Trim();
                ViewportCenterZ = Properties.Settings.Default.ViewportCenterZ.Replace(',', '.').Trim();

                ViewportOffsetX = Properties.Settings.Default.ViewportOffsetX.Replace(',', '.').Trim();
                ViewportOffsetY = Properties.Settings.Default.ViewportOffsetY.Replace(',', '.').Trim();

                CalGyroOffsetX = Properties.Settings.Default.CalGyroOffsetX.Replace(',', '.').Trim();
                CalGyroOffsetY = Properties.Settings.Default.CalGyroOffsetY.Replace(',', '.').Trim();
                CalGyroOffsetZ = Properties.Settings.Default.CalGyroOffsetZ.Replace(',', '.').Trim();
                CalMagACalX = Properties.Settings.Default.CalMagACalX.Replace(',', '.').Trim();
                CalMagACalY = Properties.Settings.Default.CalMagACalY.Replace(',', '.').Trim();
                CalMagACalZ = Properties.Settings.Default.CalMagACalZ.Replace(',', '.').Trim();
                CalMagBCalX = Properties.Settings.Default.CalMagBCalX.Replace(',', '.').Trim();
                CalMagBCalY = Properties.Settings.Default.CalMagBCalY.Replace(',', '.').Trim();
                CalMagBCalZ = Properties.Settings.Default.CalMagBCalZ.Replace(',', '.').Trim();
                InitialBeta = Properties.Settings.Default.InitialBeta.Replace(',', '.').Trim();
                InitialZeta = Properties.Settings.Default.InitialZeta.Replace(',', '.').Trim();
                Beta = Properties.Settings.Default.Beta.Replace(',', '.').Trim();
                Zeta = Properties.Settings.Default.Zeta.Replace(',', '.').Trim();
            }

            public void Save()
            {
                Properties.Settings.Default.EllipseWidth = EllipseWidth.Replace(',', '.').Trim();
                Properties.Settings.Default.EllipseHeight = EllipseHeight.Replace(',', '.').Trim();
                Properties.Settings.Default.EllipseOffset = EllipseOffset.Replace(',', '.').Trim();
                Properties.Settings.Default.IMU_SampleRate_HZ = IMU_SampleRate_HZ.Replace(',', '.').Trim();

                Properties.Settings.Default.VFlip = VFlip;
                Properties.Settings.Default.ViewportYaw = ViewportYaw.Replace(',', '.').Trim();
                Properties.Settings.Default.ViewportPitch = ViewportPitch.Replace(',', '.').Trim();
                Properties.Settings.Default.ViewportRoll = ViewportRoll.Replace(',', '.').Trim();

                Properties.Settings.Default.ViewportCenterX = ViewportCenterX.Replace(',', '.').Trim();
                Properties.Settings.Default.ViewportCenterY = ViewportCenterY.Replace(',', '.').Trim();
                Properties.Settings.Default.ViewportCenterZ = ViewportCenterZ.Replace(',', '.').Trim();

                Properties.Settings.Default.ViewportOffsetX = ViewportOffsetX.Replace(',', '.').Trim();
                Properties.Settings.Default.ViewportOffsetY = ViewportOffsetY.Replace(',', '.').Trim();

                Properties.Settings.Default.CalGyroOffsetX = CalGyroOffsetX.Replace(',', '.').Trim();
                Properties.Settings.Default.CalGyroOffsetY = CalGyroOffsetY.Replace(',', '.').Trim();
                Properties.Settings.Default.CalGyroOffsetZ = CalGyroOffsetZ.Replace(',', '.').Trim();
                Properties.Settings.Default.CalMagACalX = CalMagACalX.Replace(',', '.').Trim();
                Properties.Settings.Default.CalMagACalY = CalMagACalY.Replace(',', '.').Trim();
                Properties.Settings.Default.CalMagACalZ = CalMagACalZ.Replace(',', '.').Trim();
                Properties.Settings.Default.CalMagBCalX = CalMagBCalX.Replace(',', '.').Trim();
                Properties.Settings.Default.CalMagBCalY = CalMagBCalY.Replace(',', '.').Trim();
                Properties.Settings.Default.CalMagBCalZ = CalMagBCalZ.Replace(',', '.').Trim();
                Properties.Settings.Default.InitialBeta = InitialBeta.Replace(',', '.').Trim();
                Properties.Settings.Default.InitialZeta = InitialZeta.Replace(',', '.').Trim();
                Properties.Settings.Default.Beta = Beta.Replace(',', '.').Trim();
                Properties.Settings.Default.Zeta = Zeta.Replace(',', '.').Trim();

                Properties.Settings.Default.Save();
            }
        }

        private string xml_path;
        private properties prop = new properties();

        public frmSettingsFile()
        {
            InitializeComponent();

            xml_path = Properties.Settings.Default.XMLpath;
            txtXMLpath.Text = xml_path;

            prop.Load();
            propertyGrid.SelectedObject = prop;
        }

        private void frmSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                try
                {
                    if (Directory.Exists(txtXMLpath.Text) == false)
                        throw new Exception();
                }
                catch
                {
                    MessageBox.Show("Il percorso specificato non è valido", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                    txtXMLpath.Focus();
                    return;
                }

                Properties.Settings.Default.XMLpath = txtXMLpath.Text;
                prop.Save();
                Properties.Settings.Default.Save();
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog()
            {
                EnsurePathExists = true,
                IsFolderPicker = true,
                InitialDirectory = xml_path,
            };

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                xml_path = dialog.FileName;
                txtXMLpath.Text = xml_path;
            }
        }

        private bool validDouble(string txt, out double num)
        {
            return double.TryParse(txt, NumberStyles.Any, CultureInfo.InvariantCulture, out num);
        }

        private void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (e.ChangedItem.Label == "Inversione verticale")
                return;
            string txt = e.ChangedItem.Value.ToString().Trim().Replace(',', '.');

            if (txt.StartsWith("."))
                txt = "0" + txt;
            int count = txt.Split('.').Count();
            double num = double.NaN;

            if ((count > 2) || (validDouble(txt, out num) == false) || double.IsNaN(num))
                MessageBox.Show("Il numero specificato non è valido", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }
}
