﻿namespace WinBridge
{
    partial class frmMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.impostazioniToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.immaginiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.azimuthDistanceCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.calibrazioneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.informazioniSuVisionARMWindowsEngineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.tsTcp = new System.Windows.Forms.ToolStripMenuItem();
            this.tsUdp = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.impostazioniToolStripMenuItem1,
            this.toolsToolStripMenuItem,
            this.tsStatus,
            this.toolStripMenuItem3,
            this.tsFiles,
            this.tsTcp,
            this.tsUdp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.ShowItemToolTips = true;
            this.menuStrip1.Size = new System.Drawing.Size(443, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(94, 22);
            this.exitToolStripMenuItem.Text = "Esci";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // impostazioniToolStripMenuItem1
            // 
            this.impostazioniToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.immaginiToolStripMenuItem});
            this.impostazioniToolStripMenuItem1.Name = "impostazioniToolStripMenuItem1";
            this.impostazioniToolStripMenuItem1.Size = new System.Drawing.Size(87, 20);
            this.impostazioniToolStripMenuItem1.Text = "Impostazioni";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(188, 22);
            this.toolStripMenuItem4.Text = "Generali";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.impostazioniToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(188, 22);
            this.toolStripMenuItem5.Text = "Connessione al server";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.connessioneProviderXMLToolStripMenuItem_Click);
            // 
            // immaginiToolStripMenuItem
            // 
            this.immaginiToolStripMenuItem.Name = "immaginiToolStripMenuItem";
            this.immaginiToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.immaginiToolStripMenuItem.Text = "Immagini";
            this.immaginiToolStripMenuItem.Click += new System.EventHandler(this.immaginiToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.azimuthDistanceCalculatorToolStripMenuItem,
            this.toolStripMenuItem1,
            this.calibrazioneToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.toolsToolStripMenuItem.Text = "Strumenti";
            // 
            // azimuthDistanceCalculatorToolStripMenuItem
            // 
            this.azimuthDistanceCalculatorToolStripMenuItem.Name = "azimuthDistanceCalculatorToolStripMenuItem";
            this.azimuthDistanceCalculatorToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.azimuthDistanceCalculatorToolStripMenuItem.Text = "Calcolatore Azimut/Distanza";
            this.azimuthDistanceCalculatorToolStripMenuItem.Click += new System.EventHandler(this.azimuthDistanceCalculatorToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(221, 6);
            // 
            // calibrazioneToolStripMenuItem
            // 
            this.calibrazioneToolStripMenuItem.Name = "calibrazioneToolStripMenuItem";
            this.calibrazioneToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.calibrazioneToolStripMenuItem.Text = "Calibrazione IMU";
            this.calibrazioneToolStripMenuItem.Click += new System.EventHandler(this.calibrazioneToolStripMenuItem_Click);
            // 
            // tsStatus
            // 
            this.tsStatus.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsStatus.Image = global::WinBridge.Properties.Resources.smallgrayled;
            this.tsStatus.Name = "tsStatus";
            this.tsStatus.ShowShortcutKeys = false;
            this.tsStatus.Size = new System.Drawing.Size(28, 20);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informazioniSuVisionARMWindowsEngineToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(24, 20);
            this.toolStripMenuItem3.Text = "?";
            // 
            // informazioniSuVisionARMWindowsEngineToolStripMenuItem
            // 
            this.informazioniSuVisionARMWindowsEngineToolStripMenuItem.Name = "informazioniSuVisionARMWindowsEngineToolStripMenuItem";
            this.informazioniSuVisionARMWindowsEngineToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.informazioniSuVisionARMWindowsEngineToolStripMenuItem.Text = "Informazioni su VisionAR_M Windows Engine";
            this.informazioniSuVisionARMWindowsEngineToolStripMenuItem.Click += new System.EventHandler(this.informazioniSuVisionARMWindowsEngineToolStripMenuItem_Click);
            // 
            // tsFiles
            // 
            this.tsFiles.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsFiles.Image = global::WinBridge.Properties.Resources.Folder;
            this.tsFiles.Name = "tsFiles";
            this.tsFiles.ShowShortcutKeys = false;
            this.tsFiles.Size = new System.Drawing.Size(28, 20);
            // 
            // tsTcp
            // 
            this.tsTcp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsTcp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsTcp.Image = global::WinBridge.Properties.Resources.ConnectionOff;
            this.tsTcp.Name = "tsTcp";
            this.tsTcp.ShowShortcutKeys = false;
            this.tsTcp.Size = new System.Drawing.Size(28, 20);
            // 
            // tsUdp
            // 
            this.tsUdp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsUdp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsUdp.Image = global::WinBridge.Properties.Resources.ConnectionOff;
            this.tsUdp.Name = "tsUdp";
            this.tsUdp.ShowShortcutKeys = false;
            this.tsUdp.Size = new System.Drawing.Size(28, 20);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(419, 138);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(443, 187);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VisionAR_M Windows Engine";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem azimuthDistanceCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem calibrazioneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsStatus;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem informazioniSuVisionARMWindowsEngineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsFiles;
        private System.Windows.Forms.ToolStripMenuItem tsTcp;
        private System.Windows.Forms.ToolStripMenuItem tsUdp;
        private System.Windows.Forms.ToolStripMenuItem impostazioniToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem immaginiToolStripMenuItem;
    }
}

