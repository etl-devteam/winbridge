﻿namespace WinBridge
{
    partial class frmSettingsServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nudIP0 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.nudIP1 = new System.Windows.Forms.NumericUpDown();
            this.nudIP2 = new System.Windows.Forms.NumericUpDown();
            this.nudIP3 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.cbConnectionActive = new System.Windows.Forms.CheckBox();
            this.rbTCP = new System.Windows.Forms.RadioButton();
            this.rbUDP = new System.Windows.Forms.RadioButton();
            this.cbLog = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudIP0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIP3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(290, 165);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(93, 30);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Annulla";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(191, 165);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(93, 30);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Indirizzo IP:";
            // 
            // nudIP0
            // 
            this.nudIP0.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.nudIP0.Location = new System.Drawing.Point(15, 29);
            this.nudIP0.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudIP0.Name = "nudIP0";
            this.nudIP0.Size = new System.Drawing.Size(86, 23);
            this.nudIP0.TabIndex = 0;
            this.nudIP0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 11;
            this.label2.Text = "Porta:";
            // 
            // nudPort
            // 
            this.nudPort.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.nudPort.Location = new System.Drawing.Point(15, 83);
            this.nudPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(86, 23);
            this.nudPort.TabIndex = 4;
            this.nudPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPort.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // nudIP1
            // 
            this.nudIP1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.nudIP1.Location = new System.Drawing.Point(107, 29);
            this.nudIP1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudIP1.Name = "nudIP1";
            this.nudIP1.Size = new System.Drawing.Size(86, 23);
            this.nudIP1.TabIndex = 1;
            this.nudIP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudIP2
            // 
            this.nudIP2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.nudIP2.Location = new System.Drawing.Point(199, 29);
            this.nudIP2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudIP2.Name = "nudIP2";
            this.nudIP2.Size = new System.Drawing.Size(86, 23);
            this.nudIP2.TabIndex = 2;
            this.nudIP2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudIP3
            // 
            this.nudIP3.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.nudIP3.Location = new System.Drawing.Point(291, 29);
            this.nudIP3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudIP3.Name = "nudIP3";
            this.nudIP3.Size = new System.Drawing.Size(86, 23);
            this.nudIP3.TabIndex = 3;
            this.nudIP3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(196, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 16);
            this.label3.TabIndex = 12;
            this.label3.Text = "Protocollo:";
            // 
            // cbConnectionActive
            // 
            this.cbConnectionActive.AutoSize = true;
            this.cbConnectionActive.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cbConnectionActive.Location = new System.Drawing.Point(15, 127);
            this.cbConnectionActive.Name = "cbConnectionActive";
            this.cbConnectionActive.Size = new System.Drawing.Size(147, 20);
            this.cbConnectionActive.TabIndex = 7;
            this.cbConnectionActive.Text = "Attiva la connessione";
            this.cbConnectionActive.UseVisualStyleBackColor = true;
            // 
            // rbTCP
            // 
            this.rbTCP.AutoSize = true;
            this.rbTCP.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.rbTCP.Location = new System.Drawing.Point(290, 83);
            this.rbTCP.Name = "rbTCP";
            this.rbTCP.Size = new System.Drawing.Size(49, 20);
            this.rbTCP.TabIndex = 6;
            this.rbTCP.Text = "TCP";
            this.rbTCP.UseVisualStyleBackColor = true;
            // 
            // rbUDP
            // 
            this.rbUDP.AutoSize = true;
            this.rbUDP.Checked = true;
            this.rbUDP.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.rbUDP.Location = new System.Drawing.Point(199, 83);
            this.rbUDP.Name = "rbUDP";
            this.rbUDP.Size = new System.Drawing.Size(49, 20);
            this.rbUDP.TabIndex = 5;
            this.rbUDP.TabStop = true;
            this.rbUDP.Text = "UDP";
            this.rbUDP.UseVisualStyleBackColor = true;
            // 
            // cbLog
            // 
            this.cbLog.AutoSize = true;
            this.cbLog.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cbLog.Location = new System.Drawing.Point(15, 153);
            this.cbLog.Name = "cbLog";
            this.cbLog.Size = new System.Drawing.Size(90, 20);
            this.cbLog.TabIndex = 7;
            this.cbLog.Text = "Attiva il log";
            this.cbLog.UseVisualStyleBackColor = true;
            // 
            // frmSettingsServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 207);
            this.Controls.Add(this.rbUDP);
            this.Controls.Add(this.rbTCP);
            this.Controls.Add(this.cbLog);
            this.Controls.Add(this.cbConnectionActive);
            this.Controls.Add(this.nudPort);
            this.Controls.Add(this.nudIP3);
            this.Controls.Add(this.nudIP2);
            this.Controls.Add(this.nudIP1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudIP0);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.MinimumSize = new System.Drawing.Size(320, 200);
            this.Name = "frmSettingsServer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Impostazioni Server XML";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSettings_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nudIP0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIP3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudIP0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudPort;
        private System.Windows.Forms.NumericUpDown nudIP1;
        private System.Windows.Forms.NumericUpDown nudIP2;
        private System.Windows.Forms.NumericUpDown nudIP3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbConnectionActive;
        private System.Windows.Forms.RadioButton rbTCP;
        private System.Windows.Forms.RadioButton rbUDP;
        private System.Windows.Forms.CheckBox cbLog;
    }
}