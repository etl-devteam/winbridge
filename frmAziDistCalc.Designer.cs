﻿namespace WinBridge
{
    partial class frmAziDistCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbLatA = new System.Windows.Forms.TextBox();
            this.tbLongA = new System.Windows.Forms.TextBox();
            this.tbElevA = new System.Windows.Forms.TextBox();
            this.tbLatB = new System.Windows.Forms.TextBox();
            this.tbLongB = new System.Windows.Forms.TextBox();
            this.tbElevB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDist = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblAzimuth = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblAltitude = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbLatA
            // 
            this.tbLatA.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLatA.Location = new System.Drawing.Point(59, 48);
            this.tbLatA.Name = "tbLatA";
            this.tbLatA.Size = new System.Drawing.Size(212, 26);
            this.tbLatA.TabIndex = 0;
            this.tbLatA.Text = "35.177950000004209";
            this.tbLatA.TextChanged += new System.EventHandler(this.value_TextChanged);
            // 
            // tbLongA
            // 
            this.tbLongA.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLongA.Location = new System.Drawing.Point(277, 48);
            this.tbLongA.Name = "tbLongA";
            this.tbLongA.Size = new System.Drawing.Size(212, 26);
            this.tbLongA.TabIndex = 1;
            this.tbLongA.Text = "-79.435990000009511";
            this.tbLongA.TextChanged += new System.EventHandler(this.value_TextChanged);
            // 
            // tbElevA
            // 
            this.tbElevA.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbElevA.Location = new System.Drawing.Point(495, 48);
            this.tbElevA.Name = "tbElevA";
            this.tbElevA.Size = new System.Drawing.Size(212, 26);
            this.tbElevA.TabIndex = 2;
            this.tbElevA.Text = "93.000000000000000";
            this.tbElevA.TextChanged += new System.EventHandler(this.value_TextChanged);
            // 
            // tbLatB
            // 
            this.tbLatB.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLatB.Location = new System.Drawing.Point(59, 80);
            this.tbLatB.Name = "tbLatB";
            this.tbLatB.Size = new System.Drawing.Size(212, 26);
            this.tbLatB.TabIndex = 3;
            this.tbLatB.Text = "35.178624552377450";
            this.tbLatB.TextChanged += new System.EventHandler(this.value_TextChanged);
            // 
            // tbLongB
            // 
            this.tbLongB.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLongB.Location = new System.Drawing.Point(277, 80);
            this.tbLongB.Name = "tbLongB";
            this.tbLongB.Size = new System.Drawing.Size(212, 26);
            this.tbLongB.TabIndex = 4;
            this.tbLongB.Text = "-79.435261918370358";
            this.tbLongB.TextChanged += new System.EventHandler(this.value_TextChanged);
            // 
            // tbElevB
            // 
            this.tbElevB.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbElevB.Location = new System.Drawing.Point(495, 80);
            this.tbElevB.Name = "tbElevB";
            this.tbElevB.Size = new System.Drawing.Size(212, 26);
            this.tbElevB.TabIndex = 5;
            this.tbElevB.Text = "92.209024879194700";
            this.tbElevB.TextChanged += new System.EventHandler(this.value_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Punto A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Punto B";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Latitudine (°N)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(278, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Longitudine (°E)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(496, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Elevazione (metri)";
            // 
            // lblDist
            // 
            this.lblDist.AutoSize = true;
            this.lblDist.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDist.Location = new System.Drawing.Point(196, 136);
            this.lblDist.Name = "lblDist";
            this.lblDist.Size = new System.Drawing.Size(54, 19);
            this.lblDist.TabIndex = 12;
            this.lblDist.Text = "- - -";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(55, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 19);
            this.label7.TabIndex = 11;
            this.label7.Text = "Distanza (Km):";
            // 
            // lblAzimuth
            // 
            this.lblAzimuth.AutoSize = true;
            this.lblAzimuth.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAzimuth.Location = new System.Drawing.Point(196, 165);
            this.lblAzimuth.Name = "lblAzimuth";
            this.lblAzimuth.Size = new System.Drawing.Size(54, 19);
            this.lblAzimuth.TabIndex = 14;
            this.lblAzimuth.Text = "- - -";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(55, 165);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 19);
            this.label8.TabIndex = 13;
            this.label8.Text = "Azimut (°)";
            // 
            // lblAltitude
            // 
            this.lblAltitude.AutoSize = true;
            this.lblAltitude.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltitude.Location = new System.Drawing.Point(196, 194);
            this.lblAltitude.Name = "lblAltitude";
            this.lblAltitude.Size = new System.Drawing.Size(54, 19);
            this.lblAltitude.TabIndex = 16;
            this.lblAltitude.Text = "- - -";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(55, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 19);
            this.label10.TabIndex = 15;
            this.label10.Text = "Altezza (°)";
            // 
            // frmAziDistCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 245);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblAltitude);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblAzimuth);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblDist);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbElevB);
            this.Controls.Add(this.tbElevA);
            this.Controls.Add(this.tbLongB);
            this.Controls.Add(this.tbLongA);
            this.Controls.Add(this.tbLatB);
            this.Controls.Add(this.tbLatA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmAziDistCalc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calcolatore Azimut/Distanza";
            this.Load += new System.EventHandler(this.FrmAziDistCalc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbLatA;
        private System.Windows.Forms.TextBox tbLongA;
        private System.Windows.Forms.TextBox tbElevA;
        private System.Windows.Forms.TextBox tbLatB;
        private System.Windows.Forms.TextBox tbLongB;
        private System.Windows.Forms.TextBox tbElevB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblDist;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblAzimuth;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblAltitude;
        private System.Windows.Forms.Label label10;
    }
}