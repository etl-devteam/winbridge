﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinBridge
{
	struct POINT
	{
		public double ce;
		public double hae;
		public double lat;
		public double le;
		public double lon;
	};

	class Heading
	{
		public double Geodetic;
		public double Magnetic;
	};

	class MarkingLocation
	{
		public double hae;
		public double lat;
		public double lon;
		public uint refined;
	};

	class RelativePosition
	{
		public double Azimuth;
		public double Elevation;
		public double Range;
	};

	class Track
	{
		public double Course;
		public double Speed;
	};

	class MARKER
	{
		public bool deleted = false;
		public DateTime creationDate;

		//---------------------

		public string How;

		public long Stale;
		public long Start;
		public long Timestamp;

		public string Type;
		public string UId;
		public string Version;

		public Heading Heading = new Heading();
		public MarkingLocation MarkingLocation = new MarkingLocation();
		public RelativePosition RelativePosition = new RelativePosition();

		public long FlowTagsDebug;

		public string Callsign;
		public string Remarks;

		public Track Track = new Track();
		public POINT Point = new POINT();
	};

	struct ENTITY
	{
		public string type;
		public POINT3D point;
		public int distance;
		public int azimuth;
		public int altitude;
	};

	class Markers
	{
		private static POINT3D zero = new POINT3D();
		private static List<MARKER> markers = new List<MARKER>();
		private static MARKER loaded_marker = new MARKER();
		private static Stopwatch sw_watcher = new Stopwatch();
		private static DateTime last_watch = DateTime.MinValue;

		private static void SetValue<T>(T value, string dst)
		{
			switch (dst)
			{
				case "How":
					loaded_marker.How = Convert.ToString(value);
					break;

				case "Stale":
					loaded_marker.Stale = Convert.ToInt64(value);
					break;

				case "Start":
					loaded_marker.Start = Convert.ToInt64(value);
					break;

				case "Timestamp":
					loaded_marker.Timestamp = Convert.ToInt64(value);
					break;

				case "Type":
					loaded_marker.Type = Convert.ToString(value);
					break;

				case "UId":
					loaded_marker.UId = Convert.ToString(value);
					break;

				case "Version":
					loaded_marker.Version = Convert.ToString(value);
					break;

				case "FlowTagsDebug":
					loaded_marker.FlowTagsDebug = Convert.ToInt64(value);
					break;

				case "Callsign":
					loaded_marker.Callsign = Convert.ToString(value);
					break;

				case "Remarks":
					loaded_marker.Remarks = Convert.ToString(value);
					break;

				case "Heading.Geodetic":
					loaded_marker.Heading.Geodetic = Convert.ToDouble(value);
					break;

				case "Heading.Magnetic":
					loaded_marker.Heading.Magnetic = Convert.ToDouble(value);
					break;

				case "MarkingLocation.hae":
					loaded_marker.MarkingLocation.hae = Convert.ToDouble(value);
					break;

				case "MarkingLocation.lat":
					loaded_marker.MarkingLocation.lat = Convert.ToDouble(value);
					break;

				case "MarkingLocation.lon":
					loaded_marker.MarkingLocation.lon = Convert.ToDouble(value);
					break;

				case "MarkingLocation.refined":
					loaded_marker.MarkingLocation.refined = Convert.ToUInt32(value);
					break;

				case "RelativePosition.Azimuth":
					loaded_marker.RelativePosition.Azimuth = Convert.ToDouble(value);
					break;

				case "RelativePosition.Elevation":
					loaded_marker.RelativePosition.Elevation = Convert.ToDouble(value);
					break;

				case "RelativePosition.Range":
					loaded_marker.RelativePosition.Range = Convert.ToDouble(value);
					break;

				case "Track.Course":
					loaded_marker.Track.Course = Convert.ToDouble(value);
					break;

				case "Track.Speed":
					loaded_marker.Track.Speed = Convert.ToDouble(value);
					break;

				case "Point.ce":
					loaded_marker.Point.ce = Convert.ToDouble(value);
					break;

				case "Point.hae":
					loaded_marker.Point.hae = Convert.ToDouble(value);
					break;

				case "Point.lat":
					loaded_marker.Point.lat = Convert.ToDouble(value);
					break;

				case "Point.le":
					loaded_marker.Point.le = Convert.ToDouble(value);
					break;

				case "Point.lon":
					loaded_marker.Point.lon = Convert.ToDouble(value);
					break;
			}
		}

		private static void StringConverter(string stringValue, ref byte flags, string dst)
		{
			try
			{
				SetValue(stringValue, dst);

				if ((flags & XML.MAP_FLAG_FOUND) != 0)
					flags |= XML.MAP_FLAG_DUPLICATED;
				flags |= XML.MAP_FLAG_FOUND;
			}
			catch { flags |= XML.MAP_FLAG_ERROR; }
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		private static void DateTimeConverter(string stringValue, ref byte flags, string dst)
		{
			string fmt = "yyyy-MM-dd'T'HH:mm:ss.f'Z'";
			int cnt = stringValue.Length - 21;
			if (cnt > 0)
				fmt = fmt.Replace("f", new string('f', cnt));

			try
			{
				DateTimeOffset dt = DateTimeOffset.ParseExact(stringValue, fmt, null, DateTimeStyles.AssumeUniversal);
				SetValue(dt.ToUnixTimeSeconds(), dst);
				if ((flags & XML.MAP_FLAG_FOUND) != 0)
					flags |= XML.MAP_FLAG_DUPLICATED;
				flags |= XML.MAP_FLAG_FOUND;
			}
			catch { flags |= XML.MAP_FLAG_ERROR; }
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		private static void UIntConverter(string stringValue, ref byte flags, string dst)
		{
			try
			{
				double d = double.Parse(stringValue.Replace(',', '.'), CultureInfo.InvariantCulture);
				SetValue((uint)Math.Round(d), dst);
				if ((flags & XML.MAP_FLAG_FOUND) != 0)
					flags |= XML.MAP_FLAG_DUPLICATED;
				flags |= XML.MAP_FLAG_FOUND;
			}
			catch { flags |= XML.MAP_FLAG_ERROR; }
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		private static void DoubleConverter(string stringValue, ref byte flags, string dst)
		{
			try
			{
				double d = double.Parse(stringValue.Replace(',', '.'), CultureInfo.InvariantCulture);
				SetValue(d, dst);
				if ((flags & XML.MAP_FLAG_FOUND) != 0)
					flags |= XML.MAP_FLAG_DUPLICATED;
				flags |= XML.MAP_FLAG_FOUND;
			}
			catch { flags |= XML.MAP_FLAG_ERROR; }
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		private static XML_PARSE_MAP[] GetMap()
		{
			List<XML_PARSE_MAP> map = new List<XML_PARSE_MAP>();

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/how", flags = XML.MAP_FLAG_MANDATORY, callback = StringConverter, dst = "How" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/stale", flags = XML.MAP_FLAG_MANDATORY, callback = DateTimeConverter, dst = "Stale" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/start", flags = XML.MAP_FLAG_MANDATORY, callback = DateTimeConverter, dst = "Start" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/time", flags = XML.MAP_FLAG_MANDATORY, callback = DateTimeConverter, dst = "Timestamp" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/type", flags = XML.MAP_FLAG_MANDATORY, callback = StringConverter, dst = "Type" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/uid", flags = XML.MAP_FLAG_MANDATORY, callback = StringConverter, dst = "UId" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/version", flags = XML.MAP_FLAG_OPTIONAL, callback = StringConverter, dst = "Version" });

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/Heading/geodetic", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Heading.Geodetic" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/Heading/magnetic", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Heading.Magnetic" });

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/MarkingLocation/hae", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "MarkingLocation.hae" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/MarkingLocation/lat", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "MarkingLocation.lat" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/MarkingLocation/lon", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "MarkingLocation.lon" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/MarkingLocation/refined", flags = XML.MAP_FLAG_MANDATORY, callback = UIntConverter, dst = "MarkingLocation.refined" });

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/RelativePosition/azimuth", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "RelativePosition.Azimuth" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/RelativePosition/elevation", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "RelativePosition.Elevation" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/RelativePosition/range", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "RelativePosition.Range" });

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/_flow-tags_/debug", flags = XML.MAP_FLAG_MANDATORY, callback = DateTimeConverter, dst = "FlowTagsDebug" });

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/contact/callsign", flags = XML.MAP_FLAG_MANDATORY, callback = StringConverter, dst = "Callsign" });

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/remarks", flags = XML.MAP_FLAG_OPTIONAL, callback = StringConverter, dst = "Remarks" });

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/track/course", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Track.Course" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/detail/track/speed", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Track.Speed" });

			map.Add(new XML_PARSE_MAP() { NodePath = "/event/point/ce", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Point.ce" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/point/hae", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Point.hae" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/point/lat", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Point.lat" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/point/le", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Point.le" });
			map.Add(new XML_PARSE_MAP() { NodePath = "/event/point/lon", flags = XML.MAP_FLAG_MANDATORY, callback = DoubleConverter, dst = "Point.lon" });

			return map.ToArray();
		}

		//--------------------------------------------------------------------------------------
		// Load marker from file
		//--------------------------------------------------------------------------------------
		public static void LoadMarker(string path)
		{
			XML_PARSE_MAP[] map = GetMap();
			loaded_marker = new MARKER();
			if (XML.parse_file(path, ref map) == false)
				return;

			int idx = markers.FindIndex(a => a.UId == loaded_marker.UId);
			loaded_marker.deleted = false;
			if (idx >= 0)
			{
				loaded_marker.creationDate = markers[idx].creationDate;
				markers[idx] = loaded_marker;
			}
			else
			{
				loaded_marker.creationDate = DateTime.Now;
				markers.Add(loaded_marker);
			}
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static void ScanForMarkers(string dirName, BackgroundWorker worker)
		{
			if (sw_watcher.IsRunning && sw_watcher.ElapsedMilliseconds < 200)
				return;

			if (Directory.Exists(dirName) == false)
			{
				markers.Clear();
				worker.ReportProgress(-3, "Percorso file XML non valido");
				return;
			}

			foreach (MARKER m in markers)
				m.deleted = true;

			DirectoryInfo dir = new DirectoryInfo(dirName);
			FileInfo[] files = dir.GetFiles("*.xml").Where(f => f.Name.EndsWith(".xml")).ToArray();
			foreach (FileInfo file in files)
				LoadMarker(file.FullName);        // load marker

			markers.RemoveAll(x => x.deleted == true);

			if (markers.Count == 0)
				worker.ReportProgress(-3, "Nessun file XML caricato");
			else if (markers.Count == 1)
				worker.ReportProgress(-4, "Un file XML caricato");
			else
				worker.ReportProgress(-4, markers.Count.ToString() + " file XML caricati");
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static POINT GetWatcherPoint()
		{
			long tm = long.MinValue;
			POINT pt = new POINT();

			pt.ce = 9999999.0;
			pt.hae = 9999999.0;
			pt.lat = 9999999.0;
			pt.le = 9999999.0;
			pt.lon = 9999999.0;

			foreach (MARKER mk in markers)
			{
				if ((mk.Point.ce < 9999999.0) && (mk.Point.hae < 9999999.0) && (mk.Point.lat < 9999999.0) && (mk.Point.le < 9999999.0) && (mk.Point.lon < 9999999.0))
				{
					if (mk.Timestamp > tm)		// find the most recent position
					{
						pt = mk.Point;
						tm = mk.Timestamp;
					}
				}
			}

			return pt;
		}

		//--------------------------------------------------------------------------------------
		//
		//--------------------------------------------------------------------------------------
		public static string GetRemarks()
		{
			long now = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
			MARKER marker = null;
			foreach (MARKER mk in markers)
			{
				if ((string.IsNullOrWhiteSpace(mk.Remarks) == false) && (now >= mk.Start) && (now <= mk.Stale))
				{
					if ((marker == null) || (mk.creationDate > marker.creationDate))
						marker = mk;
				}
			}

			if (marker == null)
				return "";

			return marker.Remarks;
		}

		//--------------------------------------------------------------------------------------
		// Get a list of valid markers. The markers are translated to the observer origin 'o'
		// and rotated by the set of head-tracking angles. So the the xyz coordinate and distance
		// of the entity point is seen from the point 'o' (observer origin)
		//--------------------------------------------------------------------------------------
		public static ENTITY[] GetEntities(POINT o, double roll, double pitch, double yaw)
		{
			List<ENTITY> entities = new List<ENTITY>();
			ROTATION_MATRIX matrix;

			if (Program.Settings.VFlip)
				matrix = Math3D.InitRotationMatrix(roll, pitch, yaw);
			else
				matrix = Math3D.InitRotationMatrix(roll, -pitch, yaw);

			long now = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
			LOCATION ol = new LOCATION();
			LOCATION ml = new LOCATION();

			foreach (MARKER mk in markers)
			{
				if ((mk.MarkingLocation.hae < 9999999.0) && (mk.MarkingLocation.lat < 9999999.0) && (mk.MarkingLocation.lon < 9999999.0))
				{
					if ((now >= mk.Start) && (now <= mk.Stale))
					{
						ol.lat = o.lat;							// origin location
						ol.lon = o.lon;
						ol.elev = o.hae;

						ml.lat = mk.MarkingLocation.lat;		// marker location
						ml.lon = mk.MarkingLocation.lon;
						ml.elev = mk.MarkingLocation.hae;

						ENTITY e = new ENTITY();
						SPHERICAL_COORDINATE sc = Math3D.CalculateSphericalCoordinate(ref ol, ref ml);
						e.point = Math3D.SphericalToCartesian(ref zero, ref sc);
						Math3D.MatrixRotate(ref e.point, ref matrix);
						e.distance = (int)Math.Round(sc.distance);
						e.azimuth = (int)Math.Round(sc.azimuth);
						e.altitude = (int)Math.Round(sc.altitude);
						e.type = mk.Type;
						entities.Add(e);
					}
				}
			}

			return entities.ToArray();
		}
	}
}
