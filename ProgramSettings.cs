﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinBridge
{
    class ProgramSettings
    {
        public double EllipseWidth;
        public double EllipseHeight;
        public double EllipseOffset;
        public double IMU_SampleRate_HZ;
        public bool VFlip;
        public double ViewportYaw;
        public double ViewportPitch;
        public double ViewportRoll;

        public double ViewportCenterX;
        public double ViewportCenterY;
        public double ViewportCenterZ;

        public double ViewportOffsetX;
        public double ViewportOffsetY;

        public double CalGyroOffsetX;
        public double CalGyroOffsetY;
        public double CalGyroOffsetZ;
        public double CalMagACalX;
        public double CalMagACalY;
        public double CalMagACalZ;
        public double CalMagBCalX;
        public double CalMagBCalY;
        public double CalMagBCalZ;
        public double InitialBeta;
        public double InitialZeta;
        public double Beta;
        public double Zeta;

        public bool Changed;

        private double ConvertToDouble(string s)
        {
            double num;
            if (double.TryParse(s.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out num) && (double.IsNaN(num) == false))
                return num;
            return 0;
        }

        public void Load()
        {
            EllipseWidth = ConvertToDouble(Properties.Settings.Default.EllipseWidth);
            EllipseHeight = ConvertToDouble(Properties.Settings.Default.EllipseHeight);
            EllipseOffset = ConvertToDouble(Properties.Settings.Default.EllipseOffset);
            IMU_SampleRate_HZ = ConvertToDouble(Properties.Settings.Default.IMU_SampleRate_HZ);

            VFlip = Properties.Settings.Default.VFlip;
            ViewportYaw = ConvertToDouble(Properties.Settings.Default.ViewportYaw);
            ViewportPitch = ConvertToDouble(Properties.Settings.Default.ViewportPitch);
            ViewportRoll = ConvertToDouble(Properties.Settings.Default.ViewportRoll);

            ViewportCenterX = ConvertToDouble(Properties.Settings.Default.ViewportCenterX);
            ViewportCenterY = ConvertToDouble(Properties.Settings.Default.ViewportCenterY);
            ViewportCenterZ = ConvertToDouble(Properties.Settings.Default.ViewportCenterZ);

            ViewportOffsetX = ConvertToDouble(Properties.Settings.Default.ViewportOffsetX);
            ViewportOffsetY = ConvertToDouble(Properties.Settings.Default.ViewportOffsetY);

            CalGyroOffsetX = ConvertToDouble(Properties.Settings.Default.CalGyroOffsetX);
            CalGyroOffsetY = ConvertToDouble(Properties.Settings.Default.CalGyroOffsetY);
            CalGyroOffsetZ = ConvertToDouble(Properties.Settings.Default.CalGyroOffsetZ);
            CalMagACalX = ConvertToDouble(Properties.Settings.Default.CalMagACalX);
            CalMagACalY = ConvertToDouble(Properties.Settings.Default.CalMagACalY);
            CalMagACalZ = ConvertToDouble(Properties.Settings.Default.CalMagACalZ);
            CalMagBCalX = ConvertToDouble(Properties.Settings.Default.CalMagBCalX);
            CalMagBCalY = ConvertToDouble(Properties.Settings.Default.CalMagBCalY);
            CalMagBCalZ = ConvertToDouble(Properties.Settings.Default.CalMagBCalZ);
            InitialBeta = ConvertToDouble(Properties.Settings.Default.InitialBeta);
            InitialZeta = ConvertToDouble(Properties.Settings.Default.InitialZeta);
            Beta = ConvertToDouble(Properties.Settings.Default.Beta);
            Zeta = ConvertToDouble(Properties.Settings.Default.Zeta);

            Changed = false;
        }
    }
}
